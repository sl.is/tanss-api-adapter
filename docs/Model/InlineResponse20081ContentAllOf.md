# # InlineResponse20081ContentAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**guarantee** | **object** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

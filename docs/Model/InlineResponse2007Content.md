# # InlineResponse2007Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | filename of the uploaded file | [optional]
**size** | **int** | filesize of the uploaded file | [optional]
**mimeType** | **string** | MIME type of the uploaded file | [optional]
**status** | **string** | info representing the upload state | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

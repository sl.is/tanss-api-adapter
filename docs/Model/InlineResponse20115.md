# # InlineResponse20115

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | **object** |  | [optional]
**content** | [**InlineObject16**](InlineObject16.md) | chat message object | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

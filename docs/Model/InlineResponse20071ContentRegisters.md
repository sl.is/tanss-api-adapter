# # InlineResponse20071ContentRegisters

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Panel register id | [optional]
**name** | **string** | Panel register name | [optional]
**tickets** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20071ContentTickets[]**](InlineResponse20071ContentTickets.md) | Array of tickets | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

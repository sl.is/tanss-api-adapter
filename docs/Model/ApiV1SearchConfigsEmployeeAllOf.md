# # ApiV1SearchConfigsEmployeeAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**companyId** | **int** | if only is to be searched in one company | [optional]
**inactive** | **bool** | if false, inactive users won&#39;t be fetched (default &#x3D; true) | [optional]
**categories** | **bool** | if true, categories will be fetches as well. The names are given in the \&quot;linked entities\&quot; - \&quot;employeeCategories\&quot; (default &#x3D; false) | [optional]
**callbacks** | **bool** | if true, expected callbacks will be fetched as well (default &#x3D; false) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

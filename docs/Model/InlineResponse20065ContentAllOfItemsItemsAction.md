# # InlineResponse20065ContentAllOfItemsItemsAction

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **int** | 1 &#x3D; checked / 0 &#x3D; not checked | [optional]
**date** | **int** | date, when the action took place | [optional]
**userId** | **int** | id of the user who checked this field | [optional]
**supportId** | **int** | if a support was created, while this item was check, the id is given here (which contains the sevice text for this item) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

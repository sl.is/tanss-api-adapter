# # InlineResponse20065ContentAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the checklist | [optional]
**name** | **string** | name of the checklist | [optional]
**description** | **string** | describes the checklist, i.e. a short info | [optional]
**type** | **string** | defines the type of a checklist | [optional]
**creatorId** | **int** | id of the employee who created this checklist | [optional]
**predecessorId** | **int** | id of the the previous version of the checklist | [optional]
**active** | **bool** | is checklist active? | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

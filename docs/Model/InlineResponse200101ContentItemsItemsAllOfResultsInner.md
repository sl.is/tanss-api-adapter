# # InlineResponse200101ContentItemsItemsAllOfResultsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**linkType** | **object** |  | [optional]
**linkId** | **int** | retrieved \&quot;id\&quot; of the entity which was found | [optional]
**percent** | **int** | (optional) if not mathcin 100%, will return a certain percentage | [optional]
**name** | **string** | name of the entity which was found | [optional]
**searchSection** | **object** |  | [optional]
**object** | **object** | object of the target entity (containing the actual found entity, i.e. company, cpu type, manufacturer...) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

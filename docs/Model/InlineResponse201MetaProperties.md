# # InlineResponse201MetaProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fields** | **object** |  | [optional]
**extras** | **array<string,object>** |  | [optional]
**editable** | **bool** |  | [optional]
**message** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineResponse20081

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | **object** |  | [optional]
**content** | **mixed** | Describes a periphery with all \&quot;attached\&quot; infos | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

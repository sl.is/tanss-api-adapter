# # InlineResponse20068ContentDepartments

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**panelId** | **int** | Panel id | [optional]
**departmentId** | **int** | Department id | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

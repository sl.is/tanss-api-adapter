# # InlineObject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**staff** | **int[]** | fetch only tickets assigned to these employees | [optional]
**notAssignedToEmployees** | **int[]** | don&#39;t fetch tickets assigned to these employees | [optional]
**companies** | **int[]** | fetch only ticket from these companies | [optional]
**departments** | **int[]** | fetch only ticket assigned to these departments | [optional]
**isRepair** | **bool** | if set, will only fetch repair (or no repair) tickets | [optional]
**remitterId** | **int** | if set, will only fetch tickets of this remitter id | [optional]
**ids** | **int[]** | fetch only ticket with these ids | [optional]
**projectId** | **int** | fetches only tickets of this project | [optional]
**phaseId** | **int** | fetches only tickets of a given phase id | [optional]
**includeDoneTickets** | **bool** | by deafult, only \&quot;open\&quot; tickets will be fetched. if \&quot;done\&quot; tickets shall be fetched as well, this has to be given here | [optional]
**types** | **int[]** | fetch only ticket with these type ids | [optional]
**states** | **int[]** | fetch only ticket with these state ids | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

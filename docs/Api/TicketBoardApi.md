# SLIS\Adapter\Tanss\TicketBoardApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1TicketBoardGet()**](TicketBoardApi.md#apiV1TicketBoardGet) | **GET** /api/v1/ticketBoard | Gets the ticket board with all panels
[**apiV1TicketBoardPanelGet()**](TicketBoardApi.md#apiV1TicketBoardPanelGet) | **GET** /api/v1/ticketBoard/panel | Gets an empty ticket board panel
[**apiV1TicketBoardPanelIdDelete()**](TicketBoardApi.md#apiV1TicketBoardPanelIdDelete) | **DELETE** /api/v1/ticketBoard/panel/{id} | Deletes a ticket board panel
[**apiV1TicketBoardPanelIdGet()**](TicketBoardApi.md#apiV1TicketBoardPanelIdGet) | **GET** /api/v1/ticketBoard/panel/{id} | Gets a ticket board panel
[**apiV1TicketBoardPanelIdRegistersGet()**](TicketBoardApi.md#apiV1TicketBoardPanelIdRegistersGet) | **GET** /api/v1/ticketBoard/panel/{id}/registers | Gets all registers from a ticket board panel
[**apiV1TicketBoardPanelPost()**](TicketBoardApi.md#apiV1TicketBoardPanelPost) | **POST** /api/v1/ticketBoard/panel | Creates a new ticket board panel
[**apiV1TicketBoardPanelPut()**](TicketBoardApi.md#apiV1TicketBoardPanelPut) | **PUT** /api/v1/ticketBoard/panel | Updates a ticket board panel
[**apiV1TicketBoardProjectGlobalPanelsGet()**](TicketBoardApi.md#apiV1TicketBoardProjectGlobalPanelsGet) | **GET** /api/v1/ticketBoard/project/globalPanels | Get global ticket panels
[**apiV1TicketBoardProjectIdGet()**](TicketBoardApi.md#apiV1TicketBoardProjectIdGet) | **GET** /api/v1/ticketBoard/project/{id} | Gets a ticket board from a project
[**apiV1TicketBoardProjectIdRegistersGet()**](TicketBoardApi.md#apiV1TicketBoardProjectIdRegistersGet) | **GET** /api/v1/ticketBoard/project/{id}/registers | Gets all registers from a ticket board project


## `apiV1TicketBoardGet()`

```php
apiV1TicketBoardGet(): \SLIS\Adapter\Tanss\Model\InlineResponse20068
```

Gets the ticket board with all panels

Gets the ticket board with all panels

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketBoardApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiV1TicketBoardGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketBoardApi->apiV1TicketBoardGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20068**](../Model/InlineResponse20068.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketBoardPanelGet()`

```php
apiV1TicketBoardPanelGet($mode): \SLIS\Adapter\Tanss\Model\InlineResponse20069
```

Gets an empty ticket board panel

Gets an empty ticket board panel

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketBoardApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$mode = 'mode_example'; // string | If mode = \"edit\" you get all possible employees, departments, tags, ticket types and tickets status which can be assigned

try {
    $result = $apiInstance->apiV1TicketBoardPanelGet($mode);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketBoardApi->apiV1TicketBoardPanelGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mode** | **string**| If mode &#x3D; \&quot;edit\&quot; you get all possible employees, departments, tags, ticket types and tickets status which can be assigned | [optional]

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20069**](../Model/InlineResponse20069.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketBoardPanelIdDelete()`

```php
apiV1TicketBoardPanelIdDelete($id)
```

Deletes a ticket board panel

Deletes a ticket board panel

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketBoardApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | Panel id

try {
    $apiInstance->apiV1TicketBoardPanelIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling TicketBoardApi->apiV1TicketBoardPanelIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Panel id |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketBoardPanelIdGet()`

```php
apiV1TicketBoardPanelIdGet($id): \SLIS\Adapter\Tanss\Model\InlineResponse2026
```

Gets a ticket board panel

Gets a ticket board panel

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketBoardApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | Panel id

try {
    $result = $apiInstance->apiV1TicketBoardPanelIdGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketBoardApi->apiV1TicketBoardPanelIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Panel id |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2026**](../Model/InlineResponse2026.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketBoardPanelIdRegistersGet()`

```php
apiV1TicketBoardPanelIdRegistersGet($id): \SLIS\Adapter\Tanss\Model\InlineResponse20071
```

Gets all registers from a ticket board panel

Gets all registers from a ticket board panel

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketBoardApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | Panel id

try {
    $result = $apiInstance->apiV1TicketBoardPanelIdRegistersGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketBoardApi->apiV1TicketBoardPanelIdRegistersGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Panel id |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20071**](../Model/InlineResponse20071.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketBoardPanelPost()`

```php
apiV1TicketBoardPanelPost($body): \SLIS\Adapter\Tanss\Model\InlineResponse2026
```

Creates a new ticket board panel

Creates a new ticket board panel

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketBoardApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \stdClass; // object | Ticket board panel

try {
    $result = $apiInstance->apiV1TicketBoardPanelPost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketBoardApi->apiV1TicketBoardPanelPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **object**| Ticket board panel |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2026**](../Model/InlineResponse2026.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketBoardPanelPut()`

```php
apiV1TicketBoardPanelPut($body): \SLIS\Adapter\Tanss\Model\InlineResponse2026
```

Updates a ticket board panel

Updates a ticket board panel

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketBoardApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \stdClass; // object | Ticket board panel

try {
    $result = $apiInstance->apiV1TicketBoardPanelPut($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketBoardApi->apiV1TicketBoardPanelPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **object**| Ticket board panel |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2026**](../Model/InlineResponse2026.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketBoardProjectGlobalPanelsGet()`

```php
apiV1TicketBoardProjectGlobalPanelsGet(): \SLIS\Adapter\Tanss\Model\InlineResponse20074
```

Get global ticket panels

Gets all global ticket board panels

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketBoardApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiV1TicketBoardProjectGlobalPanelsGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketBoardApi->apiV1TicketBoardProjectGlobalPanelsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20074**](../Model/InlineResponse20074.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketBoardProjectIdGet()`

```php
apiV1TicketBoardProjectIdGet($id): \SLIS\Adapter\Tanss\Model\InlineResponse20072
```

Gets a ticket board from a project

Gets a ticket board from a project

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketBoardApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | Project id

try {
    $result = $apiInstance->apiV1TicketBoardProjectIdGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketBoardApi->apiV1TicketBoardProjectIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Project id |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20072**](../Model/InlineResponse20072.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketBoardProjectIdRegistersGet()`

```php
apiV1TicketBoardProjectIdRegistersGet($id): \SLIS\Adapter\Tanss\Model\InlineResponse20073
```

Gets all registers from a ticket board project

Gets all registers from a ticket board project

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketBoardApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | Project id

try {
    $result = $apiInstance->apiV1TicketBoardProjectIdRegistersGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketBoardApi->apiV1TicketBoardProjectIdRegistersGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Project id |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20073**](../Model/InlineResponse20073.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

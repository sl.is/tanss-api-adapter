<?php
/**
 * OfferApiTest
 * PHP version 7.3
 *
 * @category Class
 * @package  SLIS\Adapter\Tanss
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * TANSS API
 *
 * ## Documentation of the TANSS API. Version: 5.8.22.1  ### older versions Older versions of the API documentation can be found here: * [Docs for 5.8.21.6](https://api-doc.tanss.de/5.8.21.6/) * [Docs for 5.8.21.3](https://api-doc.tanss.de/5.8.21.3/) * [Docs for 5.8.21.1](https://api-doc.tanss.de/5.8.21.1/) * [Docs for 5.8.20.5](https://api-doc.tanss.de/5.8.20.5/) * [Docs for 5.8.20.4](https://api-doc.tanss.de/5.8.20.4/) * [Docs for 5.8.20.2](https://api-doc.tanss.de/5.8.20.2/) * [Docs for 5.8.19.3](https://api-doc.tanss.de/5.8.19.3/)
 *
 * The version of the OpenAPI document: 5.8.22.1
 * Contact: support@tanss.de
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 6.0.0-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace SLIS\Adapter\Tanss\Test\Api;

use \SLIS\Adapter\Tanss\Configuration;
use \SLIS\Adapter\Tanss\ApiException;
use \SLIS\Adapter\Tanss\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * OfferApiTest Class Doc Comment
 *
 * @category Class
 * @package  SLIS\Adapter\Tanss
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class OfferApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test case for apiV1OfferOfferIdDelete
     *
     * Deletes an offer.
     *
     */
    public function testApiV1OfferOfferIdDelete()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiV1OfferOfferIdGet
     *
     * Gets an offer.
     *
     */
    public function testApiV1OfferOfferIdGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiV1OfferOfferIdPut
     *
     * Updates an offer.
     *
     */
    public function testApiV1OfferOfferIdPut()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiV1OffersErpSelectionsErpSelectionIdDelete
     *
     * Deletes an erp selection.
     *
     */
    public function testApiV1OffersErpSelectionsErpSelectionIdDelete()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiV1OffersErpSelectionsErpSelectionIdGet
     *
     * Fetches an erp selection.
     *
     */
    public function testApiV1OffersErpSelectionsErpSelectionIdGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiV1OffersErpSelectionsErpSelectionIdPut
     *
     * Updates an erp selection.
     *
     */
    public function testApiV1OffersErpSelectionsErpSelectionIdPut()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiV1OffersErpSelectionsMatPickerErpSelectionIdGet
     *
     * material picker for erp selection.
     *
     */
    public function testApiV1OffersErpSelectionsMatPickerErpSelectionIdGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiV1OffersErpSelectionsMatPickerGet
     *
     * material picker.
     *
     */
    public function testApiV1OffersErpSelectionsMatPickerGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiV1OffersErpSelectionsPost
     *
     * Creates a new erp selection.
     *
     */
    public function testApiV1OffersErpSelectionsPost()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiV1OffersPost
     *
     * Creates an offer.
     *
     */
    public function testApiV1OffersPost()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiV1OffersPut
     *
     * Gets list of offers.
     *
     */
    public function testApiV1OffersPut()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiV1OffersTemplatesGet
     *
     * Gets list of offer templates.
     *
     */
    public function testApiV1OffersTemplatesGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiV1OffersTemplatesPost
     *
     * Creates an offer template.
     *
     */
    public function testApiV1OffersTemplatesPost()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiV1OffersTemplatesTemplateIdDelete
     *
     * Deletes an offer template.
     *
     */
    public function testApiV1OffersTemplatesTemplateIdDelete()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiV1OffersTemplatesTemplateIdGet
     *
     * Gets an offer templates.
     *
     */
    public function testApiV1OffersTemplatesTemplateIdGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiV1OffersTemplatesTemplateIdPut
     *
     * Updates an offer templates.
     *
     */
    public function testApiV1OffersTemplatesTemplateIdPut()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}

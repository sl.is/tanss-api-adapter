# # InlineObject4

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timeframe** | **object** |  | [optional]
**employeeId** | **int** | only shows remote supports of a certain employee | [optional]
**companyId** | **int** | only shows remote supports of this company | [optional]
**typeId** | **int** | If set, only shows remote supports of this type | [optional]
**text** | **string** | text filter for remote supports | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineResponse20061ContentTickets

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | id of the ticket | [optional]
**title** | **string** | title of the ticket | [optional]
**content** | **string** | content of the ticket. This is cut by the number of \&quot;previewContentMaxChars\&quot; as defined in the dearch config | [optional]
**statusId** | **int** | status id, name is given in the \&quot;linked entities\&quot; | [optional]
**companyId** | **int** | id of the company, name is given in the \&quot;linked entities\&quot; | [optional]
**assignedToEmployeeId** | **int** | assigned to employee id, name is given in the \&quot;linked entities\&quot; | [optional]
**assignedToDepartmentId** | **int** | assigned to department id, name is given in the \&quot;linked entities\&quot; | [optional]
**extTicketId** | **string** | the external ticket id (if given) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

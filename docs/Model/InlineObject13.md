# # InlineObject13

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tillDate** | **string** | date in the format YYYY-MM-DD | [optional]
**employeeIds** | **int[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineResponse4031

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**\SLIS\Adapter\Tanss\Model\InlineResponse4031Meta**](InlineResponse4031Meta.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

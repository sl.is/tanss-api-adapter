# # InlineObject27

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timeframe** | **object** |  | [optional]
**supportLocations** | **string[]** | show only supports from these support locations | [optional]
**supportIconTypes** | **string[]** | show only supports with these support icon types | [optional]
**companies** | **int[]** | show only supports from this companies (ids) | [optional]
**employees** | **int[]** | filter for emmployees (technician) ids | [optional]
**internalSupport** | **string** | determines which filter for \&quot;internal support\&quot; shall be used | [optional]
**companyCategories** | **int[]** | show supports from these company categories (ids) | [optional]
**companyCategoriesMatchType** | **string** | Enum representing the logic linking of certain ids (AND / NOT / OR) | [optional]
**excludeOwnCompany** | **bool** | true, if supports from the own company shall be excluded | [optional]
**invoiceNumber** | **string** | filter for a certain invoice (erp) number | [optional]
**excludeExternalEmployees** | **bool** | true if external employees shall be excluded | [optional]
**discountedSupports** | **string** | determines which filter for \&quot;discounted\&quot; supports shall be used | [optional]
**notChargedReasons** | **int[]** | show supports from these \&quot;Not charged reasons\&quot; only | [optional]
**departmentFilter** | **int[]** | show supports from these departments only | [optional]
**ticketTypes** | **int[]** | show supports from these ticket types | [optional]
**tickets** | **int[]** | show supports from these ticket (ids) only | [optional]
**ticketPhases** | **int[]** | show supports from these ticket phases only | [optional]
**linkTypes** | **int[]** | show supports from these link types only | [optional]
**supportTypes** | **int[]** | show supports from these support types only | [optional]
**includeSubTickets** | **bool** | if true, all sub-ticket supports will be included as well (when displaying support from projects) | [optional]
**planningTypes** | **string[]** | show supports from these planning types only | [optional]
**textFilter** | **string** | if filtering for support containing a given text, you can specify the text filter here | [optional]
**useRelations** | **bool** | true if relation supports shall fe fecthed as well (when specifying company filter) | [optional]
**openTickets** | **string** | determines wether supports from open tickets shall be fecthed or not | [optional]
**taskFilter** | **string** | determines wether supports from tasks shall be fetched as well | [optional]
**importTypes** | **object[]** | show supports from these support \&quot;import types\&quot; only | [optional]
**contracts** | **int[]** | show supports from these contracts (ids) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

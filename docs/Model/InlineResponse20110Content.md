# # InlineResponse20110Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**employeeId** | **int** | id of the employee for this day closing | [optional]
**date** | **\DateTime** | day for this day closing (YYYY-mm-dd) | [optional]
**totalTimestampWorkTime** | **int** | Number of total minutes of all WORK timstamp periods for this day | [optional]
**paidAbsences** | **int** | Number of total minutes of paid absences (&#x3D; vacation, illness, overtime and paid absences) | [optional]
**unpaidAbsences** | **int** | Number of total minutes of unpaid absences (&#x3D; unpaid absences) | [optional]
**documentedSupport** | **int** | Number of total minutes of all documented supportd | [optional]
**workingTime** | **int** | The number of minutes, the employee has to work on this day (based on his working time model) | [optional]
**workingTimeCleaned** | **int** | Subtracts the \&quot;workingTime\&quot; with the minutes of \&quot;paid absences\&quot;, thus returning the \&quot;effective\&quot; working time the employee has to work | [optional]
**totalTime** | **int** | Cumulating the \&quot;totalTimestampWorkTime\&quot;, \&quot;paidAbsences\&quot; and \&quot;unpaidAbsences\&quot; | [optional]
**documentedPercentage** | **int** | a ration between the documented supports to the \&quot;WORK\&quot; timestamps | [optional]
**balance** | **int** | returns the effective balance for this account (&#x3D; totalTime - workingTimeCleaned) | [optional]
**completedByEmployeeId** | **int** | the id of the employee who has approved the day | [optional]
**completedOnDate** | **int** | the timestamp, when the employee has approved the day closing | [optional]
**totalBalance** | **int** | the total balance of all previous days accumulated, plus the balance of the current day | [optional]
**totalBalanceBeforeTimeframe** | **int** | the total balance of all previous -not shown- days accumulated | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

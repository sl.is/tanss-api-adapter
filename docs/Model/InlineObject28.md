# # InlineObject28

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**linkType** | **object** |  | [optional]
**linkIds** | **int[]** | ids of the assignment to be filtered | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

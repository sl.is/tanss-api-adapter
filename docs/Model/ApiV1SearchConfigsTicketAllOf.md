# # ApiV1SearchConfigsTicketAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**previewContentMaxChars** | **int** | if defined, overrides the to preview the content (default values is 60) | [optional]
**companyId** | **int** | if only is to be searched in one company | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

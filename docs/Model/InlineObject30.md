# # InlineObject30

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**linkType** | **object** |  | [optional]
**linkId** | **int** | id of the assignment, for which an action shall be triggered (mostly a ticket id) | [optional]
**ticketNotificationOptions** | [**\SLIS\Adapter\Tanss\Model\ApiV1TanssEventsRulesTestActionTicketNotificationOptions**](ApiV1TanssEventsRulesTestActionTicketNotificationOptions.md) |  | [optional]
**action** | [**\SLIS\Adapter\Tanss\Model\ApiV1TanssEventsRulesActions**](ApiV1TanssEventsRulesActions.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

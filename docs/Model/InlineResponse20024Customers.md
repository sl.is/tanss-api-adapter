# # InlineResponse20024Customers

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Id of the company in tanss | [optional]
**customerNumber** | **string** | Represent the Id in the ERP-System | [optional]
**matchcode** | **string** | Matchcode is a additional field with a unique key | [optional]
**name** | **string** | Name of the company | [optional]
**street** | **string** | Street where the company is located | [optional]
**postalCode** | **string** | Postalcode of the location | [optional]
**city** | **string** | City where the company is located | [optional]
**country** | **string** | Country where the company is located | [optional]
**phoneNumber** | **string** | First telephone number of the company | [optional]
**faxNumber** | **string** | Fax number of the company | [optional]
**email** | **string** | General email address of the company | [optional]
**website** | **string** | Web address of the company | [optional]
**headquarters** | **string** | Headquarters of the company | [optional]
**active** | **bool** | Indicates if the company is an active or inactive customer | [optional]
**private** | **bool** | Indicates if the company is a private customer | [optional]
**categories** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20024Categories[]**](InlineResponse20024Categories.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

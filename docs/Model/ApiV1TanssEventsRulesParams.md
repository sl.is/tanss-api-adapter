# # ApiV1TanssEventsRulesParams

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **string** | defines the url a webhook url will be sent to (actionType WEBHOOK) | [optional]
**method** | **string** | defines the http method for a webhook notification (actionType WEBHOOK) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

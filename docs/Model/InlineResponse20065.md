# # InlineResponse20065

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | **object** |  | [optional]
**content** | [**InlineResponse20065ContentAllOf**](InlineResponse20065ContentAllOf.md) | detailled infos for the checklist, with all values (checked items, history, vars atc.) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

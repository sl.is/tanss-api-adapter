# # InlineResponse20083Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the periphery type | [optional]
**name** | **string** | name of the periphery type | [optional]
**image** | **string** | icon used when displaying peripheries of this type | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

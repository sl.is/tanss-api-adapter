# # ApiV1TagsVisibilities

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tagId** | **int** | id of the tag | [optional]
**linkTypeId** | **int** | linkType of the assignment | [optional]
**linkId** | **int** | id of the assignment | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineResponse20067ContentItemsAllOf1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accountingTypeId** | **int** | id of the accountingtype used for this support (i.e. \&quot;Techniker\&quot;) | [optional]
**booked** | **bool** | wether the support was already booked | [optional]
**clearanceStatus** | **string** | determines if a support was already \&quot;cleared\&quot; for billing | [optional]
**companyId** | **int** | id of the company for this support | [optional]
**contractId** | **int** | id of the contract, used for this support | [optional]
**createdEmployeeId** | **int** | id of the employee who has created this support | [optional]
**departmentId** | **int** | if used, id of the department which is assigned to this support | [optional]
**durationApproach** | **int** | duration (in minutes) of the approach (drive) | [optional]
**durationDeparture** | **int** | duration (in minutes) of the departure (drive) | [optional]
**startBreak** | **int** | timestamp when the pause starts (0 if no pause is given) | [optional]
**durationBreak** | **int** | duration of the pause (in minutes) | [optional]
**erpNumber** | **string** | if the support has an erp number entered | [optional]
**extern** | **bool** | if this support was created by a customer login, this value is \&quot;true\&quot; | [optional]
**externalTicketId** | **string** | if the support has an external ticket number entered, this number is given here | [optional]
**hourlyRate** | **double** | the hourly rate for this support (or the rate per working unit) | [optional]
**installationFee** | **bool** | wether the support will be billed as an \&quot;installation fee\&quot; with a fixed price | [optional]
**installationFeeAmount** | **double** | if the support will be billed as an \&quot;installation fee\&quot;, the amount is given here | [optional]
**installationFeeTypeId** | **int** | if the support will be billed as an \&quot;installation fee\&quot;, the id of the installation fee type is given here | [optional]
**internal** | **bool** | if this support is marked as \&quot;internal\&quot; (customers won&#39;t see those) | [optional]
**outlook** | **bool** | true, if this support is a TANSS2Ex appointment | [optional]
**supportIconType** | **string** | determines the icon used to display this support | [optional]
**textIntern** | **string** | internal remarks for this support (customers won&#39;t see this) | [optional]
**voucherId** | **int** | if the support was already booked and assigned to a voucher, the voucher id is given here | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

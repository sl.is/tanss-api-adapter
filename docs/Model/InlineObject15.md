# # InlineObject15

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**employeeId** | **int** | filters chats for a given employee id. All chats will be returned to which this employee has access to | [optional]
**onlyExpectedTimeExpired** | **bool** | if true, only open chats with an expired expected time will be shown (overrides \&quot;status\&quot;) | [optional]
**chatIds** | **int[]** | if a list of specific chat ids shall be used, you can place the here | [optional]
**status** | **string** | Enum representing a the state of the chat | [optional]
**searchString** | **string** | If chats shall be filtered for a given search text, this text goes here | [optional]
**linkTypeId** | **int** | linkType of assignment | [optional]
**linkId** | **int** | id of assignment | [optional]
**linkIds** | **int[]** | if multiple assignment ids schall be fetched, you can use an array of integers here | [optional]
**creatorId** | **int** | filters chats from this given chat creator | [optional]
**showOnlyParticapatedChat** | **bool** | if true, only returns chats which the employee is assigned as creator or participant. Otherwise returns all chats whith access. | [optional]
**loadMessages** | **string** | describes if/which messages shall be loaded in a chat list | [optional]
**fillLinkedEntities** | **bool** | if true, the \&quot;linked entities\&quot; will be filled as well (with information regarding employees, tickets etc.) | [optional]
**minimumCreationDate** | **int** | only returns chats with at least the given creation date | [optional]
**itemsPerPage** | **int** | if pagination is used, you can enter the number of items per page | [optional]
**page** | **int** | if pagination is used, you can enter the page number here | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

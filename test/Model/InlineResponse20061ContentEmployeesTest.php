<?php
/**
 * InlineResponse20061ContentEmployeesTest
 *
 * PHP version 7.3
 *
 * @category Class
 * @package  SLIS\Adapter\Tanss
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * TANSS API
 *
 * ## Documentation of the TANSS API. Version: 5.8.22.1  ### older versions Older versions of the API documentation can be found here: * [Docs for 5.8.21.6](https://api-doc.tanss.de/5.8.21.6/) * [Docs for 5.8.21.3](https://api-doc.tanss.de/5.8.21.3/) * [Docs for 5.8.21.1](https://api-doc.tanss.de/5.8.21.1/) * [Docs for 5.8.20.5](https://api-doc.tanss.de/5.8.20.5/) * [Docs for 5.8.20.4](https://api-doc.tanss.de/5.8.20.4/) * [Docs for 5.8.20.2](https://api-doc.tanss.de/5.8.20.2/) * [Docs for 5.8.19.3](https://api-doc.tanss.de/5.8.19.3/)
 *
 * The version of the OpenAPI document: 5.8.22.1
 * Contact: support@tanss.de
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 6.0.0-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace SLIS\Adapter\Tanss\Test\Model;

use PHPUnit\Framework\TestCase;

/**
 * InlineResponse20061ContentEmployeesTest Class Doc Comment
 *
 * @category    Class
 * @description object containing a found employee with all associated infos
 * @package     SLIS\Adapter\Tanss
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class InlineResponse20061ContentEmployeesTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test "InlineResponse20061ContentEmployees"
     */
    public function testInlineResponse20061ContentEmployees()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "name"
     */
    public function testPropertyName()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "firstName"
     */
    public function testPropertyFirstName()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "lastName"
     */
    public function testPropertyLastName()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "departmentId"
     */
    public function testPropertyDepartmentId()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "role"
     */
    public function testPropertyRole()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "phoneNr"
     */
    public function testPropertyPhoneNr()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "phoneNr2"
     */
    public function testPropertyPhoneNr2()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "email"
     */
    public function testPropertyEmail()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "mobileNr"
     */
    public function testPropertyMobileNr()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "mobileNr2"
     */
    public function testPropertyMobileNr2()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "privateNr"
     */
    public function testPropertyPrivateNr()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "companies"
     */
    public function testPropertyCompanies()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "active"
     */
    public function testPropertyActive()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "anticipatedCallbacks"
     */
    public function testPropertyAnticipatedCallbacks()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "categories"
     */
    public function testPropertyCategories()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}

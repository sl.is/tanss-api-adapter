# # InlineResponse20045ContentCloseRequests

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**description** | **string** | if a general chat (without assignment) is created, a description is needed | [optional]
**linkTypeId** | **int** | linkType of assignment | [optional]
**linkId** | **int** | id of assignment | [optional]
**status** | **string** | Enum representing a the state of the chat | [optional]
**closedByEmployeeId** | **int** | id of employee who has closed this chat | [optional]
**expectedResponseTime** | **int** | timestamp of an expected response (if chat has an expected response) | [optional]
**createdByEmployeeId** | **int** | id of employee who has created this chat | [optional]
**creationDate** | **int** | timestamp of an chat creation | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

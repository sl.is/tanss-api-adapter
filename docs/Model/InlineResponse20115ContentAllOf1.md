# # InlineResponse20115ContentAllOf1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the chat message | [optional]
**employeeId** | **int** | id of the employee who has sent this message | [optional]
**creationDate** | **int** | date when the message was sent | [optional]
**expectedResponse** | **int** | if the message has an expected response time, this is given here (in minutes) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

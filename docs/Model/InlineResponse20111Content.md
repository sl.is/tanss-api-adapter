# # InlineResponse20111Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdDayClosings** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20110Content[]**](InlineResponse20110Content.md) | a list of created day closing objects | [optional]
**errors** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20111ContentErrors[]**](InlineResponse20111ContentErrors.md) | a list of errors that occured while creating the day closings | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

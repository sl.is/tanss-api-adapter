# SLIS\Adapter\Tanss\OperatingSystemsApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1OsGet()**](OperatingSystemsApi.md#apiV1OsGet) | **GET** /api/v1/os | Get a list of all os
[**apiV1OsIdDelete()**](OperatingSystemsApi.md#apiV1OsIdDelete) | **DELETE** /api/v1/os/{id} | Deletes a specific os
[**apiV1OsIdGet()**](OperatingSystemsApi.md#apiV1OsIdGet) | **GET** /api/v1/os/{id} | Get a specific os
[**apiV1OsIdPut()**](OperatingSystemsApi.md#apiV1OsIdPut) | **PUT** /api/v1/os/{id} | Updates a os
[**apiV1OsPost()**](OperatingSystemsApi.md#apiV1OsPost) | **POST** /api/v1/os | Creates a new os


## `apiV1OsGet()`

```php
apiV1OsGet(): \SLIS\Adapter\Tanss\Model\InlineResponse20091
```

Get a list of all os

Gets a list of all operating systems

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\OperatingSystemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiV1OsGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperatingSystemsApi->apiV1OsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20091**](../Model/InlineResponse20091.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1OsIdDelete()`

```php
apiV1OsIdDelete($id)
```

Deletes a specific os

Deletes a specific operating system

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\OperatingSystemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | Id of the operating system

try {
    $apiInstance->apiV1OsIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling OperatingSystemsApi->apiV1OsIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Id of the operating system |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1OsIdGet()`

```php
apiV1OsIdGet($id): \SLIS\Adapter\Tanss\Model\InlineResponse20149
```

Get a specific os

Gets a specific operating system

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\OperatingSystemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | Id of the operating system

try {
    $result = $apiInstance->apiV1OsIdGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperatingSystemsApi->apiV1OsIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Id of the operating system |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20149**](../Model/InlineResponse20149.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1OsIdPut()`

```php
apiV1OsIdPut($id, $body): \SLIS\Adapter\Tanss\Model\InlineResponse20149
```

Updates a os

Updates an existing operating system

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\OperatingSystemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | Id of the operating system
$body = new \stdClass; // object | operating system to be updated

try {
    $result = $apiInstance->apiV1OsIdPut($id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperatingSystemsApi->apiV1OsIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Id of the operating system |
 **body** | **object**| operating system to be updated |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20149**](../Model/InlineResponse20149.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1OsPost()`

```php
apiV1OsPost($inlineObject32): \SLIS\Adapter\Tanss\Model\InlineResponse20149
```

Creates a new os

Creates a new operating system

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\OperatingSystemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject32 = new \SLIS\Adapter\Tanss\Model\InlineObject32(); // \SLIS\Adapter\Tanss\Model\InlineObject32

try {
    $result = $apiInstance->apiV1OsPost($inlineObject32);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperatingSystemsApi->apiV1OsPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject32** | [**\SLIS\Adapter\Tanss\Model\InlineObject32**](../Model/InlineObject32.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20149**](../Model/InlineResponse20149.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

# # InlineResponse20051ContentItemsAllOfWorkflowsItemsAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of this workflow | [optional]
**companyId** | **int** | id of the company for this workflow | [optional]
**date** | **int** | date, when the workflow was started | [optional]
**state** | **string** | describes the state of a workflow | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # ApiCallsV1PhoneParticipants

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**phoneCallId** | **int** | Id of the phone call | [optional]
**idString** | **string** | Identifier string the phone system uses to represent the participant (&#x3D;technician). This is used to assign a phone call to an employee. | [optional]
**employeeId** | **int** | Id of the employee, the call is assigned to. The \&quot;idString\&quot; can be assigned to certain employees. If this could be \&quot;translated\&quot;, the actual id will be given here. If storing calls into the database and the TANSS employee id is not known, then only the \&quot;idString\&quot; should be given, and upon creating the call, the system will try to \&quot;translate\&quot; the \&quot;idString\&quot; into an employee id | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineResponse20068ContentTags

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**panelId** | **int** | Panel id | [optional]
**tagId** | **int** | Tag id | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

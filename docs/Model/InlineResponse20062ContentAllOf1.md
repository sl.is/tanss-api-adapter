# # InlineResponse20062ContentAllOf1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assignmentInfos** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20062ContentAllOfAssignmentInfos**](InlineResponse20062ContentAllOfAssignmentInfos.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineObject9

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groupName** | **string** | name of the monitoring group. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

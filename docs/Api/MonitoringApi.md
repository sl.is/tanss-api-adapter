# SLIS\Adapter\Tanss\MonitoringApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiMonitoringV1AssignGroupDelete()**](MonitoringApi.md#apiMonitoringV1AssignGroupDelete) | **DELETE** /api/monitoring/v1/assignGroup | Delete a group assignment
[**apiMonitoringV1AssignGroupGet()**](MonitoringApi.md#apiMonitoringV1AssignGroupGet) | **GET** /api/monitoring/v1/assignGroup | Gets all group assignments
[**apiMonitoringV1AssignGroupPost()**](MonitoringApi.md#apiMonitoringV1AssignGroupPost) | **POST** /api/monitoring/v1/assignGroup | Assigns a groupName to a company or device
[**apiMonitoringV1TicketFromGroupGet()**](MonitoringApi.md#apiMonitoringV1TicketFromGroupGet) | **GET** /api/monitoring/v1/ticketFromGroup | Gets ticket(s), based on a given group
[**apiMonitoringV1TicketPost()**](MonitoringApi.md#apiMonitoringV1TicketPost) | **POST** /api/monitoring/v1/ticket | Creates a ticket, using the monitoring api
[**apiMonitoringV1TicketTicketIdGet()**](MonitoringApi.md#apiMonitoringV1TicketTicketIdGet) | **GET** /api/monitoring/v1/ticket/{ticketId} | Gets a ticket (created by the monitoring api) by id
[**apiMonitoringV1TicketTicketIdPut()**](MonitoringApi.md#apiMonitoringV1TicketTicketIdPut) | **PUT** /api/monitoring/v1/ticket/{ticketId} | Updates a ticket (created by the monitoring api) by id


## `apiMonitoringV1AssignGroupDelete()`

```php
apiMonitoringV1AssignGroupDelete($inlineObject9)
```

Delete a group assignment

Monitoring systems send \"groups\" to determine for which device a ticket shall be created. A \"translation table\" can be filled with assignments of goup names to specific devices.  This call deletes such an assignment.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\MonitoringApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject9 = new \SLIS\Adapter\Tanss\Model\InlineObject9(); // \SLIS\Adapter\Tanss\Model\InlineObject9

try {
    $apiInstance->apiMonitoringV1AssignGroupDelete($inlineObject9);
} catch (Exception $e) {
    echo 'Exception when calling MonitoringApi->apiMonitoringV1AssignGroupDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject9** | [**\SLIS\Adapter\Tanss\Model\InlineObject9**](../Model/InlineObject9.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiMonitoringV1AssignGroupGet()`

```php
apiMonitoringV1AssignGroupGet(): \SLIS\Adapter\Tanss\Model\InlineResponse20018
```

Gets all group assignments

Monitoring systems send \"groups\" to determine, for which device a ticket shall be created. A \"translation table\" can be filled with assignments of goup names to specific devices.  This call lists all existing group assignments.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\MonitoringApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiMonitoringV1AssignGroupGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MonitoringApi->apiMonitoringV1AssignGroupGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20018**](../Model/InlineResponse20018.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiMonitoringV1AssignGroupPost()`

```php
apiMonitoringV1AssignGroupPost($inlineObject8): \SLIS\Adapter\Tanss\Model\InlineResponse2018
```

Assigns a groupName to a company or device

Monitoring systems send \"groups\" to determine, for which device a ticket shall be created. A \"translation table\" can be filled with assignments of goup names to specific devices - the following api call does this.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\MonitoringApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject8 = new \SLIS\Adapter\Tanss\Model\InlineObject8(); // \SLIS\Adapter\Tanss\Model\InlineObject8

try {
    $result = $apiInstance->apiMonitoringV1AssignGroupPost($inlineObject8);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MonitoringApi->apiMonitoringV1AssignGroupPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject8** | [**\SLIS\Adapter\Tanss\Model\InlineObject8**](../Model/InlineObject8.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2018**](../Model/InlineResponse2018.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiMonitoringV1TicketFromGroupGet()`

```php
apiMonitoringV1TicketFromGroupGet($groupName, $closedTickets): \SLIS\Adapter\Tanss\Model\InlineResponse20019
```

Gets ticket(s), based on a given group

If a monitoring system wants to check if a ticket does exist based on a given group name, this can be checked here

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\MonitoringApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$groupName = 'groupName_example'; // string | Name of the group, which the ticket shall be retrieved for
$closedTickets = True; // bool | The default behaviour ist that only \"open\" tickets will be returned. Set this to \"true\" if you want to retrieve closed tickets as well

try {
    $result = $apiInstance->apiMonitoringV1TicketFromGroupGet($groupName, $closedTickets);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MonitoringApi->apiMonitoringV1TicketFromGroupGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupName** | **string**| Name of the group, which the ticket shall be retrieved for |
 **closedTickets** | **bool**| The default behaviour ist that only \&quot;open\&quot; tickets will be returned. Set this to \&quot;true\&quot; if you want to retrieve closed tickets as well | [optional]

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20019**](../Model/InlineResponse20019.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiMonitoringV1TicketPost()`

```php
apiMonitoringV1TicketPost($uNKNOWNBASETYPE): \SLIS\Adapter\Tanss\Model\InlineResponse2017
```

Creates a ticket, using the monitoring api

A monitoring system can create tickets. This is mainly the same process as creating a ticket via the API (`POST /api/v1/tickets`), but has the following special behaviours:  * You don't have to know the `companyId` or assignment (`linkTypeId` / `linkId`), as the api can retrieve them from the given `groupName` * The `groupName` can be assigned to companies or assignment - this is done via the route `/api/monitoring/v1/assignGroup`

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\MonitoringApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$uNKNOWNBASETYPE = new \SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE(); // \SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE

try {
    $result = $apiInstance->apiMonitoringV1TicketPost($uNKNOWNBASETYPE);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MonitoringApi->apiMonitoringV1TicketPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uNKNOWNBASETYPE** | [**\SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2017**](../Model/InlineResponse2017.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiMonitoringV1TicketTicketIdGet()`

```php
apiMonitoringV1TicketTicketIdGet($ticketId): \SLIS\Adapter\Tanss\Model\InlineResponse202
```

Gets a ticket (created by the monitoring api) by id

Fetches a ticket by id and returns all aspects of the ticket. Only tickets which were created by the monitoring api routes can be fetched.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\MonitoringApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ticketId = 56; // int | Id of the ticket that should be fetched

try {
    $result = $apiInstance->apiMonitoringV1TicketTicketIdGet($ticketId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MonitoringApi->apiMonitoringV1TicketTicketIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketId** | **int**| Id of the ticket that should be fetched |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse202**](../Model/InlineResponse202.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiMonitoringV1TicketTicketIdPut()`

```php
apiMonitoringV1TicketTicketIdPut($ticketId): \SLIS\Adapter\Tanss\Model\InlineResponse202
```

Updates a ticket (created by the monitoring api) by id

Updates a ticket by id. Only tickets which were created by the monitoring api routes can be editied.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\MonitoringApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ticketId = 56; // int | Id of the ticket that should be updated

try {
    $result = $apiInstance->apiMonitoringV1TicketTicketIdPut($ticketId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MonitoringApi->apiMonitoringV1TicketTicketIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketId** | **int**| Id of the ticket that should be updated |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse202**](../Model/InlineResponse202.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

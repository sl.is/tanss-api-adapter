# # ApiCallsV1FromPhoneNrInfosItems

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **object** |  | [optional]
**id** | **int** | id of the company or employee which was found (based on the \&quot;type\&quot;) | [optional]
**name** | **string** | a string containing the name of the company or employee | [optional]
**active** | **bool** | describes if the company or employee is still active | [optional]
**charsLeftOut** | **int** | Describes how many chars were left out while doing the search. This could have been the case when searching for companies. In this case, some chars will be left out so that number extensions can be searched as well. Example: * A company has the phone number 06154/6006-0 * You search for a number 06154/6006-123 * The system will try to replace the last 3 chars of the company, so that the number could be found as well,   because 06154/6006-123 is an extension to 06154/6006-0 | [optional]
**companyId** | **int** | When searching for employees, the \&quot;main\&quot; company id of the employee will be returned as well | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

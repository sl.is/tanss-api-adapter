# # InlineResponse20079ContentAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**serviceIcons** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20079ContentAllOfServiceIconsInner[]**](InlineResponse20079ContentAllOfServiceIconsInner.md) |  | [optional]
**components** | **object[]** | infos about components which are built into this pc | [optional]
**devices** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20079ContentAllOfDevicesInner[]**](InlineResponse20079ContentAllOfDevicesInner.md) | infos about devices which are linked to this pc | [optional]
**softwarelicenses** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20079ContentAllOfSoftwarelicensesInner[]**](InlineResponse20079ContentAllOfSoftwarelicensesInner.md) | infos about software licenses which are used by this pc | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineResponse20040ContentItemsAllOfHistoryInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the history log entry | [optional]
**timestampId** | **int** | id of the timestamp which this history entry is for | [optional]
**date** | **int** | timestamp when the change occured | [optional]
**changeByUserId** | **int** | id of the user which did this change | [optional]
**fromState** | **string** | Determines the state for the timestamp (i.e. on, off) | [optional]
**fromType** | **string** | Determines the type for the timestamp (i.e. work) | [optional]
**fromDate** | **int** |  | [optional]
**toState** | **object** |  | [optional]
**toType** | **object** |  | [optional]
**toDate** | **int** | new timestamp (is 0 if the entry was deleted) | [optional]
**historyType** | **string** | describes the \&quot;type\&quot; of a history entry | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

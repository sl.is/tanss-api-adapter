# # InlineResponse20053Availability

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | **int** | date when this state was set | [optional]
**employeeId** | **int** | id of employee | [optional]
**typeId** | **int** | id of availability type | [optional]
**host** | **string** | hostname which set this state | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineResponse200101ContentItemsItemsAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**linkType** | **object** |  | [optional]
**identifier** | **string** | identifier (or name) of the entity. This will be used to search the entity id. | [optional]
**searchSections** | **string[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

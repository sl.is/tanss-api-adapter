# SLIS\Adapter\Tanss\PeripheryApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1PeripheriesPeripheryIdBuildInLinkTypeIdLinkIdDelete()**](PeripheryApi.md#apiV1PeripheriesPeripheryIdBuildInLinkTypeIdLinkIdDelete) | **DELETE** /api/v1/peripheries/{peripheryId}/buildIn/{linkTypeId}/{linkId} | Delete periphery assignment
[**apiV1PeripheriesPeripheryIdBuildInLinkTypeIdLinkIdPost()**](PeripheryApi.md#apiV1PeripheriesPeripheryIdBuildInLinkTypeIdLinkIdPost) | **POST** /api/v1/peripheries/{peripheryId}/buildIn/{linkTypeId}/{linkId} | Assign periphery
[**apiV1PeripheriesPeripheryIdDelete()**](PeripheryApi.md#apiV1PeripheriesPeripheryIdDelete) | **DELETE** /api/v1/peripheries/{peripheryId} | Deletes a periphery
[**apiV1PeripheriesPeripheryIdGet()**](PeripheryApi.md#apiV1PeripheriesPeripheryIdGet) | **GET** /api/v1/peripheries/{peripheryId} | Gets a periphery by id
[**apiV1PeripheriesPeripheryIdPut()**](PeripheryApi.md#apiV1PeripheriesPeripheryIdPut) | **PUT** /api/v1/peripheries/{peripheryId} | Updates a periphery
[**apiV1PeripheriesPost()**](PeripheryApi.md#apiV1PeripheriesPost) | **POST** /api/v1/peripheries | Creates a periphery
[**apiV1PeripheriesPut()**](PeripheryApi.md#apiV1PeripheriesPut) | **PUT** /api/v1/peripheries | Gets a list of peripheries
[**apiV1PeripheriesTypesGet()**](PeripheryApi.md#apiV1PeripheriesTypesGet) | **GET** /api/v1/peripheries/types | Get periphery types
[**apiV1PeripheriesTypesPost()**](PeripheryApi.md#apiV1PeripheriesTypesPost) | **POST** /api/v1/peripheries/types | Create periphery type
[**apiV1PeripheriesTypesTypeIdDelete()**](PeripheryApi.md#apiV1PeripheriesTypesTypeIdDelete) | **DELETE** /api/v1/peripheries/types/{typeId} | Delete periphery type
[**apiV1PeripheriesTypesTypeIdPut()**](PeripheryApi.md#apiV1PeripheriesTypesTypeIdPut) | **PUT** /api/v1/peripheries/types/{typeId} | Update periphery type


## `apiV1PeripheriesPeripheryIdBuildInLinkTypeIdLinkIdDelete()`

```php
apiV1PeripheriesPeripheryIdBuildInLinkTypeIdLinkIdDelete($peripheryId, $linkTypeId, $linkId)
```

Delete periphery assignment

Deletes a periphery assignment to another pc or periphery

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\PeripheryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$peripheryId = 56; // int | id of the periphery to be removed
$linkTypeId = 56; // int | link type of the \"target\" assignment (i.e. 1 = pc)
$linkId = 56; // int | link id of the \"target\" assignment (i.e. id of the pc)

try {
    $apiInstance->apiV1PeripheriesPeripheryIdBuildInLinkTypeIdLinkIdDelete($peripheryId, $linkTypeId, $linkId);
} catch (Exception $e) {
    echo 'Exception when calling PeripheryApi->apiV1PeripheriesPeripheryIdBuildInLinkTypeIdLinkIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **peripheryId** | **int**| id of the periphery to be removed |
 **linkTypeId** | **int**| link type of the \&quot;target\&quot; assignment (i.e. 1 &#x3D; pc) |
 **linkId** | **int**| link id of the \&quot;target\&quot; assignment (i.e. id of the pc) |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1PeripheriesPeripheryIdBuildInLinkTypeIdLinkIdPost()`

```php
apiV1PeripheriesPeripheryIdBuildInLinkTypeIdLinkIdPost($peripheryId, $linkTypeId, $linkId): \SLIS\Adapter\Tanss\Model\InlineResponse20063
```

Assign periphery

Assigns a periphery to another pc or periphery

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\PeripheryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$peripheryId = 56; // int | id of the periphery to be assigned
$linkTypeId = 56; // int | link type of the \"target\" assignment (i.e. 1 = pc)
$linkId = 56; // int | link id of the \"target\" assignment (i.e. id of the pc)

try {
    $result = $apiInstance->apiV1PeripheriesPeripheryIdBuildInLinkTypeIdLinkIdPost($peripheryId, $linkTypeId, $linkId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PeripheryApi->apiV1PeripheriesPeripheryIdBuildInLinkTypeIdLinkIdPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **peripheryId** | **int**| id of the periphery to be assigned |
 **linkTypeId** | **int**| link type of the \&quot;target\&quot; assignment (i.e. 1 &#x3D; pc) |
 **linkId** | **int**| link id of the \&quot;target\&quot; assignment (i.e. id of the pc) |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20063**](../Model/InlineResponse20063.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1PeripheriesPeripheryIdDelete()`

```php
apiV1PeripheriesPeripheryIdDelete($peripheryId)
```

Deletes a periphery

Deletes a periphery

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\PeripheryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$peripheryId = 56; // int

try {
    $apiInstance->apiV1PeripheriesPeripheryIdDelete($peripheryId);
} catch (Exception $e) {
    echo 'Exception when calling PeripheryApi->apiV1PeripheriesPeripheryIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **peripheryId** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1PeripheriesPeripheryIdGet()`

```php
apiV1PeripheriesPeripheryIdGet($peripheryId): \SLIS\Adapter\Tanss\Model\InlineResponse20081
```

Gets a periphery by id

Fetches a periphery by a given id

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\PeripheryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$peripheryId = 56; // int

try {
    $result = $apiInstance->apiV1PeripheriesPeripheryIdGet($peripheryId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PeripheryApi->apiV1PeripheriesPeripheryIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **peripheryId** | **int**|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20081**](../Model/InlineResponse20081.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1PeripheriesPeripheryIdPut()`

```php
apiV1PeripheriesPeripheryIdPut($peripheryId, $body): \SLIS\Adapter\Tanss\Model\InlineResponse20134
```

Updates a periphery

Updates a periphery  Will also update attached ip address information or guarantee.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\PeripheryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$peripheryId = 56; // int
$body = new \stdClass; // object | periphery object to be saved

try {
    $result = $apiInstance->apiV1PeripheriesPeripheryIdPut($peripheryId, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PeripheryApi->apiV1PeripheriesPeripheryIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **peripheryId** | **int**|  |
 **body** | **object**| periphery object to be saved |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20134**](../Model/InlineResponse20134.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1PeripheriesPost()`

```php
apiV1PeripheriesPost($body): \SLIS\Adapter\Tanss\Model\InlineResponse20134
```

Creates a periphery

Creates a periphery  Will also store attached ip address information or guarantee.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\PeripheryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \stdClass; // object | periphery object to be saved

try {
    $result = $apiInstance->apiV1PeripheriesPost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PeripheryApi->apiV1PeripheriesPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **object**| periphery object to be saved |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20134**](../Model/InlineResponse20134.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1PeripheriesPut()`

```php
apiV1PeripheriesPut($uNKNOWNBASETYPE): \SLIS\Adapter\Tanss\Model\InlineResponse20082
```

Gets a list of peripheries

Gets a list of peripheries

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\PeripheryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$uNKNOWNBASETYPE = new \SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE(); // \SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE | query parameters for the request

try {
    $result = $apiInstance->apiV1PeripheriesPut($uNKNOWNBASETYPE);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PeripheryApi->apiV1PeripheriesPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uNKNOWNBASETYPE** | [**\SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)| query parameters for the request |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20082**](../Model/InlineResponse20082.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1PeripheriesTypesGet()`

```php
apiV1PeripheriesTypesGet(): \SLIS\Adapter\Tanss\Model\InlineResponse20083
```

Get periphery types

Gets a list of all periphery types

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\PeripheryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiV1PeripheriesTypesGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PeripheryApi->apiV1PeripheriesTypesGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20083**](../Model/InlineResponse20083.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1PeripheriesTypesPost()`

```php
apiV1PeripheriesTypesPost($body): \SLIS\Adapter\Tanss\Model\InlineResponse20136
```

Create periphery type

Creates a new periphery types

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\PeripheryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \stdClass; // object | periphery type to be saved

try {
    $result = $apiInstance->apiV1PeripheriesTypesPost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PeripheryApi->apiV1PeripheriesTypesPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **object**| periphery type to be saved |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20136**](../Model/InlineResponse20136.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1PeripheriesTypesTypeIdDelete()`

```php
apiV1PeripheriesTypesTypeIdDelete($typeId)
```

Delete periphery type

Deletes a periphery type

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\PeripheryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$typeId = 56; // int | type id to be deleted

try {
    $apiInstance->apiV1PeripheriesTypesTypeIdDelete($typeId);
} catch (Exception $e) {
    echo 'Exception when calling PeripheryApi->apiV1PeripheriesTypesTypeIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **typeId** | **int**| type id to be deleted |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1PeripheriesTypesTypeIdPut()`

```php
apiV1PeripheriesTypesTypeIdPut($typeId, $body): \SLIS\Adapter\Tanss\Model\InlineResponse20136
```

Update periphery type

Updates a periphery type

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\PeripheryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$typeId = 56; // int | type id to be updated
$body = new \stdClass; // object | periphery type to be updated

try {
    $result = $apiInstance->apiV1PeripheriesTypesTypeIdPut($typeId, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PeripheryApi->apiV1PeripheriesTypesTypeIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **typeId** | **int**| type id to be updated |
 **body** | **object**| periphery type to be updated |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20136**](../Model/InlineResponse20136.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

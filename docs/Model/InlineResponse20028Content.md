# # InlineResponse20028Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**name** | **string** | name of the ticket type | [optional]
**useReactionHours** | **bool** | determines wether reaction hours shall be used for this ticket type | [optional]
**active** | **bool** | determines if the ticket type is active | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

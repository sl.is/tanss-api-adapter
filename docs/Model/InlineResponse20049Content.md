# # InlineResponse20049Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of this material | [optional]
**articleNumber** | **string** | article number of this material | [optional]
**label** | **string** | laben / name of this material | [optional]
**serialNumber** | **string** | serial number | [optional]
**price** | **float** | price for this material | [optional]
**remark** | **string** | addition text / remark for this material | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

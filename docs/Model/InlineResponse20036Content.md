# # InlineResponse20036Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**employeeId** | **int** | id of the employee | [optional]
**date** | **int** | date | [optional]
**state** | **string** | Determines the state for the timestamp (i.e. on, off) | [optional]
**type** | **string** | Determines the type for the timestamp (i.e. work) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineResponse20027Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the ticket state | [optional]
**name** | **string** | name of the ticket state | [optional]
**image** | **string** | image name to be used for this ticket state (when displaying the ticket) | [optional]
**waitState** | **bool** | if true, this state counts as a \&quot;waiting state\&quot; | [optional]
**rank** | **int** | current rank in the order of ticket states | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

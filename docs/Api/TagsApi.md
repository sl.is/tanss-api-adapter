# SLIS\Adapter\Tanss\TagsApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1TagsAssignmentDelete()**](TagsApi.md#apiV1TagsAssignmentDelete) | **DELETE** /api/v1/tags/assignment | Removes a tag
[**apiV1TagsAssignmentGet()**](TagsApi.md#apiV1TagsAssignmentGet) | **GET** /api/v1/tags/assignment | List of tags to an assignment
[**apiV1TagsAssignmentLogGet()**](TagsApi.md#apiV1TagsAssignmentLogGet) | **GET** /api/v1/tags/assignment/log | List of tags logs to an assignment
[**apiV1TagsAssignmentPost()**](TagsApi.md#apiV1TagsAssignmentPost) | **POST** /api/v1/tags/assignment | Assigns a tag
[**apiV1TagsAssignmentPut()**](TagsApi.md#apiV1TagsAssignmentPut) | **PUT** /api/v1/tags/assignment | Assigns multiple tags
[**apiV1TagsGet()**](TagsApi.md#apiV1TagsGet) | **GET** /api/v1/tags | Get all tags
[**apiV1TagsIdDelete()**](TagsApi.md#apiV1TagsIdDelete) | **DELETE** /api/v1/tags/{id} | Deletes a tag
[**apiV1TagsIdGet()**](TagsApi.md#apiV1TagsIdGet) | **GET** /api/v1/tags/{id} | Gets a tag
[**apiV1TagsIdPut()**](TagsApi.md#apiV1TagsIdPut) | **PUT** /api/v1/tags/{id} | Edits a tag
[**apiV1TagsPost()**](TagsApi.md#apiV1TagsPost) | **POST** /api/v1/tags | Creates a new tag


## `apiV1TagsAssignmentDelete()`

```php
apiV1TagsAssignmentDelete($tagId, $linkTypeId, $linkId)
```

Removes a tag

Removes a tag from an assignment.  Must be technician/freelancer and have access to the assignment!

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TagsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$tagId = 56; // int | id of the tag
$linkTypeId = 56; // int | linkType of the assignment
$linkId = 56; // int | id of the assignment

try {
    $apiInstance->apiV1TagsAssignmentDelete($tagId, $linkTypeId, $linkId);
} catch (Exception $e) {
    echo 'Exception when calling TagsApi->apiV1TagsAssignmentDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tagId** | **int**| id of the tag |
 **linkTypeId** | **int**| linkType of the assignment |
 **linkId** | **int**| id of the assignment |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TagsAssignmentGet()`

```php
apiV1TagsAssignmentGet($linkTypeId, $linkId): \SLIS\Adapter\Tanss\Model\InlineResponse20055
```

List of tags to an assignment

Gets alist of all tags who are assigned to an entity (for example ticket).  Must be technician/freelancer and have access to the assignment!

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TagsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$linkTypeId = 56; // int | linkType of the assignment
$linkId = 56; // int | id of the assignment

try {
    $result = $apiInstance->apiV1TagsAssignmentGet($linkTypeId, $linkId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TagsApi->apiV1TagsAssignmentGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **linkTypeId** | **int**| linkType of the assignment |
 **linkId** | **int**| id of the assignment |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20055**](../Model/InlineResponse20055.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TagsAssignmentLogGet()`

```php
apiV1TagsAssignmentLogGet($linkTypeId, $linkId): \SLIS\Adapter\Tanss\Model\InlineResponse20058
```

List of tags logs to an assignment

Gets alist of all tag logs to an entity (for example ticket). Here you can see when tags habe been assigned or removed.  Must be technician/freelancer and have access to the assignment!

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TagsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$linkTypeId = 56; // int | linkType of the assignment
$linkId = 56; // int | id of the assignment

try {
    $result = $apiInstance->apiV1TagsAssignmentLogGet($linkTypeId, $linkId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TagsApi->apiV1TagsAssignmentLogGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **linkTypeId** | **int**| linkType of the assignment |
 **linkId** | **int**| id of the assignment |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20058**](../Model/InlineResponse20058.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TagsAssignmentPost()`

```php
apiV1TagsAssignmentPost($inlineObject22): \SLIS\Adapter\Tanss\Model\InlineResponse20127
```

Assigns a tag

Assigns a tag to an entity.  Must be technician/freelancer and have access to the assignment!

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TagsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject22 = new \SLIS\Adapter\Tanss\Model\InlineObject22(); // \SLIS\Adapter\Tanss\Model\InlineObject22

try {
    $result = $apiInstance->apiV1TagsAssignmentPost($inlineObject22);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TagsApi->apiV1TagsAssignmentPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject22** | [**\SLIS\Adapter\Tanss\Model\InlineObject22**](../Model/InlineObject22.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20127**](../Model/InlineResponse20127.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TagsAssignmentPut()`

```php
apiV1TagsAssignmentPut($linkTypeId, $linkId, $requestBody): \SLIS\Adapter\Tanss\Model\InlineResponse20055
```

Assigns multiple tags

Assigns multiplke tags at one for a specific device/assignment. All tag ids are given.  The system automatically determines wbhich of those tags shall be created or removed.  Must be technician/freelancer and have access to the assignment!

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TagsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$linkTypeId = 56; // int | linkType of the assignment
$linkId = 56; // int | id of the assignment
$requestBody = array(56); // int[] | tag assignment object

try {
    $result = $apiInstance->apiV1TagsAssignmentPut($linkTypeId, $linkId, $requestBody);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TagsApi->apiV1TagsAssignmentPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **linkTypeId** | **int**| linkType of the assignment |
 **linkId** | **int**| id of the assignment |
 **requestBody** | [**int[]**](../Model/int.md)| tag assignment object |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20055**](../Model/InlineResponse20055.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TagsGet()`

```php
apiV1TagsGet(): \SLIS\Adapter\Tanss\Model\InlineResponse20055
```

Get all tags

Gets a list of all tags  Must be technician or freelancer in order to use this route!

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TagsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiV1TagsGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TagsApi->apiV1TagsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20055**](../Model/InlineResponse20055.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TagsIdDelete()`

```php
apiV1TagsIdDelete($id)
```

Deletes a tag

Deletes an existing tag.  Must have the permission to manage tags in the system in order to use this route!

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TagsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | id of the tag to be deleted

try {
    $apiInstance->apiV1TagsIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling TagsApi->apiV1TagsIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id of the tag to be deleted |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TagsIdGet()`

```php
apiV1TagsIdGet($id): \SLIS\Adapter\Tanss\Model\InlineResponse20125
```

Gets a tag

Gets an existing tag.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TagsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | id of the tag

try {
    $result = $apiInstance->apiV1TagsIdGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TagsApi->apiV1TagsIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id of the tag |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20125**](../Model/InlineResponse20125.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TagsIdPut()`

```php
apiV1TagsIdPut($id, $body): \SLIS\Adapter\Tanss\Model\InlineResponse20125
```

Edits a tag

Edits an existing tag.  Must have the permission to manage tags in the system in order to use this route!

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TagsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | id of the tag
$body = new \stdClass; // object | tag object

try {
    $result = $apiInstance->apiV1TagsIdPut($id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TagsApi->apiV1TagsIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id of the tag |
 **body** | **object**| tag object |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20125**](../Model/InlineResponse20125.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TagsPost()`

```php
apiV1TagsPost($inlineObject21): \SLIS\Adapter\Tanss\Model\InlineResponse20125
```

Creates a new tag

Creates a new tag.  Must have the permission to manage tags in the system in order to use this route!

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TagsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject21 = new \SLIS\Adapter\Tanss\Model\InlineObject21(); // \SLIS\Adapter\Tanss\Model\InlineObject21

try {
    $result = $apiInstance->apiV1TagsPost($inlineObject21);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TagsApi->apiV1TagsPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject21** | [**\SLIS\Adapter\Tanss\Model\InlineObject21**](../Model/InlineObject21.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20125**](../Model/InlineResponse20125.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

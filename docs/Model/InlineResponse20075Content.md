# # InlineResponse20075Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**employeeId** | **int** | id of the employee of this timer | [optional]
**companyId** | **int** | id of the company (otional) | [optional]
**title** | **string** | a title for this timer | [optional]
**duration** | **int** | total duration (in seconds) for this timer | [optional]
**callbackId** | **int** | (optional) if the timer was created for a callback, the id is given here | [optional]
**ticketId** | **int** | (optional) if the timer was created for a ticket, the id is given here | [optional]
**linkTypeId** | **int** | (optional) if the timer was created for a misc. assignment, the linkTypeId goes here | [optional]
**linkId** | **int** | (optional) if the timer was created for a misc. assignment, the linkId goes here | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

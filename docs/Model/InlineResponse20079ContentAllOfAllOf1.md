# # InlineResponse20079ContentAllOfAllOf1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ips** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20079ContentAllOfAllOfIpsInner[]**](InlineResponse20079ContentAllOfAllOfIpsInner.md) |  | [optional]
**guarantee** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20079ContentAllOfAllOfGuarantee**](InlineResponse20079ContentAllOfAllOfGuarantee.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineObject19

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**companyId** | **int** | if only offers from one company shall be fetched, give the company id here | [optional]
**onlyValid** | **bool** | if true, only offers with a valid \&quot;validTill\&quot; shall be given | [optional]
**templateId** | **int** | only returns offers from this template | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineResponse2003Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the document | [optional]
**ticketId** | **int** | id of the assigned ticket | [optional]
**fileName** | **string** | original filename of the document | [optional]
**mimeType** | **string** | mime type (if given) | [optional]
**employeeId** | **int** | id of the employee who uploaded this document | [optional]
**date** | **int** | date when the document was uploaded | [optional]
**internal** | **bool** | true if the document is internal (customers won&#39;t see it) | [optional]
**description** | **string** | (optional) description of the document | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

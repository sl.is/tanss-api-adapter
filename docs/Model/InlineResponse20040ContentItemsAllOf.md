# # InlineResponse20040ContentItemsAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**employeeId** | **int** | id of the employee | [optional]
**date** | **\DateTime** | day in the format YYYY-mm-dd | [optional]
**weekDay** | **object** |  | [optional]
**workingTimeModelId** | **int** | id of the working time model. Infos are stored in the \&quot;list properties\&quot; - \&quot;workingTimeModels\&quot; - (ID) - \&quot;extras\&quot; - \&quot;model\&quot; | [optional]
**timestamps** | **object[]** | A list of all timestamps of the given day | [optional]
**types** | [**array<string,\SLIS\Adapter\Tanss\Model\InlineResponse20039ContentItemsAllOfTypesAddlProps>**](InlineResponse20039ContentItemsAllOfTypesAddlProps.md) | a list of timestamp periods, grouped by the timestamp type. The key is the TimestampType, the value is a list of TimestampPeriods | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

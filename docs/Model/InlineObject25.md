# # InlineObject25

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**areas** | **string[]** | describes in which areas to search | [optional]
**query** | **string** | the query string | [optional]
**configs** | [**\SLIS\Adapter\Tanss\Model\ApiV1SearchConfigs**](ApiV1SearchConfigs.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

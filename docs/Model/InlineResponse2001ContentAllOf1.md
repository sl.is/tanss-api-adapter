# # InlineResponse2001ContentAllOf1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**modifiedBy** | **int** | id of the employee who last modified this ticket | [optional] [readonly]
**modifiedTime** | **int** | timestamp, when the ticket was last modified | [optional] [readonly]
**modifiedText** | **string** | infos about the last change for this ticket | [optional] [readonly]
**numberOfDocuments** | **int** | number of documents for this ticket | [optional] [readonly]
**chats** | [**\SLIS\Adapter\Tanss\Model\InlineResponse2001ContentAllOfChatsInner[]**](InlineResponse2001ContentAllOfChatsInner.md) | list of chat ids for this ticket | [optional] [readonly]
**nextSupport** | [**\SLIS\Adapter\Tanss\Model\InlineResponse2001ContentAllOfNextSupport**](InlineResponse2001ContentAllOfNextSupport.md) |  | [optional]
**remitterDepartmentName** | **string** | If the remitter is assigned to a department, returns the name of the department | [optional] [readonly]
**remitterDepartmentId** | **int** | If the remitter is assigned to a department, returns the id of the department | [optional] [readonly]
**internalContent** | **string** | returns the internal content if the user has right to | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineResponse20059ContentItemsAllOf1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stateLog** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20059ContentItemsAllOfStateLogInner[]**](InlineResponse20059ContentItemsAllOfStateLogInner.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

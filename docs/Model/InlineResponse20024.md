# # InlineResponse20024

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customers** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20024Customers[]**](InlineResponse20024Customers.md) |  | [optional]
**employees** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20024Employees[]**](InlineResponse20024Employees.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

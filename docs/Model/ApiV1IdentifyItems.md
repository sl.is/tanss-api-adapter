# # ApiV1IdentifyItems

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**linkType** | **string** | Enum representing the \&quot;link type\&quot; of an entity to be identifed | [optional]
**identifier** | **string** | identifier (or name) of the entity. This will be used to search the entity id. | [optional]
**searchSections** | **string[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

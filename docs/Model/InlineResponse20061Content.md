# # InlineResponse20061Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**companies** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20061ContentCompanies[]**](InlineResponse20061ContentCompanies.md) |  | [optional]
**employees** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20061ContentEmployees[]**](InlineResponse20061ContentEmployees.md) |  | [optional]
**tickets** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20061ContentTickets[]**](InlineResponse20061ContentTickets.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineResponse20027

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20027Meta**](InlineResponse20027Meta.md) |  | [optional]
**content** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20027Content[]**](InlineResponse20027Content.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

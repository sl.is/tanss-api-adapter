# # InlineObject21

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**name** | **string** | name of the tag | [optional]
**backgroundColor** | **string** | background color of the tag (in hex, for example FF0000 - max. 6 chars!) | [optional]
**fontColor** | **string** | font color of the tag (in hex, for example FF0000 - max. 6 chars!) | [optional]
**description** | **string** | description of the tag (optional) | [optional]
**image** | **string** | name of image (optional) | [optional]
**groupTagId** | **int** | id of the group tag | [optional]
**groupTag** | [**\SLIS\Adapter\Tanss\Model\ApiV1TagsGroupTag**](ApiV1TagsGroupTag.md) |  | [optional]
**groupTagInheritance** | **bool** | inherit visibilities from group tag | [optional]
**isGroupParent** | **bool** | is the tag a parent for other tags | [optional]
**visibilities** | [**\SLIS\Adapter\Tanss\Model\ApiV1TagsVisibilities[]**](ApiV1TagsVisibilities.md) | visibility assignments (departments, employees, company categories, ticket types) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

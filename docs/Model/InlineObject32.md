# # InlineObject32

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the os | [optional]
**name** | **string** | name of the os | [optional]
**serverOperatingSystem** | **bool** | define if this os is only to be used for servers | [optional]
**active** | **bool** | define if this os is still active/used | [optional]
**articleNumber** | **string** | (optional) article number used in erp | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

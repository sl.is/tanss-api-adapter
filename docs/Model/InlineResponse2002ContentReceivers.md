# # InlineResponse2002ContentReceivers

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**emailAddress** | **string** | e-Mail address of the receiver | [optional]
**name** | **string** | name of the receiver | [optional]
**status** | **string** | Status of the mail sending | [optional]
**sentDate** | **int** | date, when the mail was sent | [optional]
**method** | **string** | Method of the mail receiver (TO / CC / BCC) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# SLIS\Adapter\Tanss\PcApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1PcsPcIdDelete()**](PcApi.md#apiV1PcsPcIdDelete) | **DELETE** /api/v1/pcs/{pcId} | Deletes a pc
[**apiV1PcsPcIdGet()**](PcApi.md#apiV1PcsPcIdGet) | **GET** /api/v1/pcs/{pcId} | Gets a pc by id
[**apiV1PcsPcIdPut()**](PcApi.md#apiV1PcsPcIdPut) | **PUT** /api/v1/pcs/{pcId} | Updates a pc
[**apiV1PcsPost()**](PcApi.md#apiV1PcsPost) | **POST** /api/v1/pcs | Creates a pc
[**apiV1PcsPut()**](PcApi.md#apiV1PcsPut) | **PUT** /api/v1/pcs | Gets a list of pcs


## `apiV1PcsPcIdDelete()`

```php
apiV1PcsPcIdDelete($pcId)
```

Deletes a pc

Deletes a pc or server.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\PcApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$pcId = 56; // int

try {
    $apiInstance->apiV1PcsPcIdDelete($pcId);
} catch (Exception $e) {
    echo 'Exception when calling PcApi->apiV1PcsPcIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pcId** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1PcsPcIdGet()`

```php
apiV1PcsPcIdGet($pcId): \SLIS\Adapter\Tanss\Model\InlineResponse20079
```

Gets a pc by id

Fetches a pc or server by a given id

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\PcApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$pcId = 56; // int

try {
    $result = $apiInstance->apiV1PcsPcIdGet($pcId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PcApi->apiV1PcsPcIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pcId** | **int**|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20079**](../Model/InlineResponse20079.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1PcsPcIdPut()`

```php
apiV1PcsPcIdPut($pcId, $uNKNOWNBASETYPE): \SLIS\Adapter\Tanss\Model\InlineResponse20132
```

Updates a pc

Updates a pc or server.  Will also update attached ip address information or guarantee.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\PcApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$pcId = 56; // int
$uNKNOWNBASETYPE = new \SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE(); // \SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE | pc object to be saved

try {
    $result = $apiInstance->apiV1PcsPcIdPut($pcId, $uNKNOWNBASETYPE);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PcApi->apiV1PcsPcIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pcId** | **int**|  |
 **uNKNOWNBASETYPE** | [**\SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)| pc object to be saved |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20132**](../Model/InlineResponse20132.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1PcsPost()`

```php
apiV1PcsPost($body): \SLIS\Adapter\Tanss\Model\InlineResponse20132
```

Creates a pc

Creates a pc or server.  Will also store attached ip address information or guarantee.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\PcApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \stdClass; // object | pc object to be saved

try {
    $result = $apiInstance->apiV1PcsPost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PcApi->apiV1PcsPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **object**| pc object to be saved |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20132**](../Model/InlineResponse20132.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1PcsPut()`

```php
apiV1PcsPut($uNKNOWNBASETYPE): \SLIS\Adapter\Tanss\Model\InlineResponse20080
```

Gets a list of pcs

Gets a list of pcs or server.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\PcApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$uNKNOWNBASETYPE = new \SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE(); // \SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE | query parameters for the request

try {
    $result = $apiInstance->apiV1PcsPut($uNKNOWNBASETYPE);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PcApi->apiV1PcsPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uNKNOWNBASETYPE** | [**\SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)| query parameters for the request |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20080**](../Model/InlineResponse20080.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

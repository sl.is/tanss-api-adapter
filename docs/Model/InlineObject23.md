# # InlineObject23

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timeframe** | **object** |  | [optional]
**state** | **string** | Describes a current \&quot;state\&quot; of a callback | [optional]
**states** | **object[]** | if multiple states have to be filtered, give an array here | [optional]
**fromEmployeeId** | **int** | filters only callbacks which were created by this employee (technician) | [optional]
**toEmployeeId** | **int** | filters only callbacks to this employee (techncian) | [optional]
**toAllEmployeesWithAccessTo** | **bool** | if \&quot;toEmployeeId\&quot; is given, also retrieves all callbacks which the user has access to | [optional]
**toDepartmentIds** | **int** | filters only callbacks for these departments | [optional]
**companyIds** | **int** | filters only callbacks for these (customer) companies (ids given here) | [optional]
**employeeIds** | **int** | filters only callbacks for these (customer) employees (ids given here) | [optional]
**loadLinkedEntites** | **bool** | if true, \&quot;linked entities\&quot; will be loaded as well (given in the responses \&quot;meta\&quot; section) | [optional]
**withLog** | **bool** | if true, will also load all state logs of the retrieved callbacks | [optional]
**itemsPerPage** | **int** | if pagination shall be used, give here the items per page | [optional]
**page** | **int** | if pagination shall be used, the page number is given here | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

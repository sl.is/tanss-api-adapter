# # InlineResponse20146Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**employeeId** | **int** | id of the employee who this event is for | [optional]
**linkType** | **object** |  | [optional]
**linkId** | **int** | id of the assignment, which this event refers to | [optional]
**triggerType** | **string** | describes the \&quot;trigger type\&quot; of an event | [optional]
**content** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20146ContentContent**](InlineResponse20146ContentContent.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

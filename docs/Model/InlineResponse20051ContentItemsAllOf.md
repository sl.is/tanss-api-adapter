# # InlineResponse20051ContentItemsAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the offer | [optional]
**name** | **string** | name of the offer | [optional]
**date** | **int** | creation date of this offer | [optional]
**createdByEmployeeId** | **int** | id othe employee who has created this offer | [optional]
**linkTypeId** | **int** | link type of the offer assignment | [optional]
**linkId** | **int** | id of the offer assignment | [optional]
**validTill** | **int** | date which the offer expires | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# SLIS\Adapter\Tanss\SearchApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1SearchPut()**](SearchApi.md#apiV1SearchPut) | **PUT** /api/v1/search | global search


## `apiV1SearchPut()`

```php
apiV1SearchPut($inlineObject25): \SLIS\Adapter\Tanss\Model\InlineResponse20061
```

global search

Executes a global search, defined via the configuration object in the request body

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\SearchApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject25 = new \SLIS\Adapter\Tanss\Model\InlineObject25(); // \SLIS\Adapter\Tanss\Model\InlineObject25

try {
    $result = $apiInstance->apiV1SearchPut($inlineObject25);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SearchApi->apiV1SearchPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject25** | [**\SLIS\Adapter\Tanss\Model\InlineObject25**](../Model/InlineObject25.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20061**](../Model/InlineResponse20061.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

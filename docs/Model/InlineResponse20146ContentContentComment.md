# # InlineResponse20146ContentContentComment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the comment | [optional]
**employeeId** | **int** | id of the author / employee | [optional]
**employeeName** | **string** | name of the author / employee | [optional]
**title** | **string** | title of the comment | [optional]
**content** | **string** | content of the comment | [optional]
**internal** | **bool** | true if it&#39;s an internal comment | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

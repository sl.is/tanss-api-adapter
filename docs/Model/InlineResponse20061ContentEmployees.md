# # InlineResponse20061ContentEmployees

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | id of the employee | [optional]
**name** | **string** | full (displayed) name | [optional]
**firstName** | **string** | first name | [optional]
**lastName** | **string** | last name | [optional]
**departmentId** | **string** | department id of the employee, the name is given in the \&quot;linked entities\&quot; | [optional]
**role** | **string** | role of the employee | [optional]
**phoneNr** | **string** | phone number | [optional]
**phoneNr2** | **string** | phone number | [optional]
**email** | **string** | email | [optional]
**mobileNr** | **string** | mobile phone number | [optional]
**mobileNr2** | **string** | mobile phone number | [optional]
**privateNr** | **string** | private phone number | [optional]
**companies** | **int[]** | shows assigned companies of this user, the name is given in the \&quot;linked entities\&quot; | [optional]
**active** | **bool** | determines wether an employee is active or not | [optional]
**anticipatedCallbacks** | **object[]** |  | [optional]
**categories** | **int[]** | ids of all employee categories (if defined in the employee search configuration). The names are given in the \&quot;linked entities\&quot; - \&quot;employeeCategories\&quot; | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineResponse200101Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**InlineResponse200101ContentItemsItemsAllOf[]**](InlineResponse200101ContentItemsItemsAllOf.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

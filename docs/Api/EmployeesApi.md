# SLIS\Adapter\Tanss\EmployeesApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1EmployeesPost()**](EmployeesApi.md#apiV1EmployeesPost) | **POST** /api/v1/employees | creates an employee
[**apiV1EmployeesTechniciansGet()**](EmployeesApi.md#apiV1EmployeesTechniciansGet) | **GET** /api/v1/employees/technicians | Gets all technicians


## `apiV1EmployeesPost()`

```php
apiV1EmployeesPost($uNKNOWNBASETYPE): \SLIS\Adapter\Tanss\Model\InlineResponse20123
```

creates an employee

Creates an employee

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\EmployeesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$uNKNOWNBASETYPE = new \SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE(); // \SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE | employee object

try {
    $result = $apiInstance->apiV1EmployeesPost($uNKNOWNBASETYPE);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EmployeesApi->apiV1EmployeesPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uNKNOWNBASETYPE** | [**\SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)| employee object |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20123**](../Model/InlineResponse20123.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1EmployeesTechniciansGet()`

```php
apiV1EmployeesTechniciansGet($freelancerCompanyId): \SLIS\Adapter\Tanss\Model\InlineResponse20032
```

Gets all technicians

Gets all technicians of this system.  Needs to be a technician or freelancer in order to view this list

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\EmployeesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$freelancerCompanyId = 56; // int | If this parameter is given, also fetches the freelancers of this company (0 if all freelancers shall be retrieved)

try {
    $result = $apiInstance->apiV1EmployeesTechniciansGet($freelancerCompanyId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EmployeesApi->apiV1EmployeesTechniciansGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **freelancerCompanyId** | **int**| If this parameter is given, also fetches the freelancers of this company (0 if all freelancers shall be retrieved) | [optional]

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20032**](../Model/InlineResponse20032.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

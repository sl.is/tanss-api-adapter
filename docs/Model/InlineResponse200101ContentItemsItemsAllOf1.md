# # InlineResponse200101ContentItemsItemsAllOf1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**\SLIS\Adapter\Tanss\Model\InlineResponse200101ContentItemsItemsAllOfResultsInner[]**](InlineResponse200101ContentItemsItemsAllOfResultsInner.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineResponse20079ContentAllOfDevicesItemsFieldsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**additionalFieldId** | **int** | id of the additional field | [optional]
**title** | **string** | title name for the field | [optional]
**value** | **string** | value for this additional field | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

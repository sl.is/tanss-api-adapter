# SLIS\Adapter\Tanss\SecurityApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1UserLoginGet()**](SecurityApi.md#apiV1UserLoginGet) | **GET** /api/v1/user/login | logs in the user and generates an api token for further authentication


## `apiV1UserLoginGet()`

```php
apiV1UserLoginGet($user, $password, $logintoken)
```

logs in the user and generates an api token for further authentication

A successful login contains two tokens,  the apiToken (apiKey) and the refeshToken (refresh). The apiToken will expire after 4 hours and the refreshToken is valid for 5 days.  Every request to the API must use one of these tokens in the http-header (for example (\"apiToken\" -> \"Bearer ...\";), only with the apiToken will you receive the requested resource.    If the refreshToken is used, you will get a new apiToken and refreshToken. You can use every route apart from /user/login to get a new pair of tokens.The response to this request looks similar to a usual login.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\SecurityApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$user = 'user_example'; // string | username for login in TANSS
$password = 'password_example'; // string | password for login in TANSS
$logintoken = 'logintoken_example'; // string | If the user needs an login token, this field must be set as well!

try {
    $apiInstance->apiV1UserLoginGet($user, $password, $logintoken);
} catch (Exception $e) {
    echo 'Exception when calling SecurityApi->apiV1UserLoginGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **string**| username for login in TANSS |
 **password** | **string**| password for login in TANSS |
 **logintoken** | **string**| If the user needs an login token, this field must be set as well! | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

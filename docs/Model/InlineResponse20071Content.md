# # InlineResponse20071Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**registers** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20071ContentRegisters[]**](InlineResponse20071ContentRegisters.md) | Array of panel registers | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineResponse20068ContentPanels

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Panel id | [optional]
**name** | **string** | Panel name | [optional]
**projectId** | **int** | Panel project id | [optional]
**employeeId** | **int** | Panel employee id | [optional]
**panelType** | **string** | Panel type | [optional]
**registerType** | **string** | Register type | [optional]
**includeProjects** | **bool** | Include project tickets | [optional]
**includeSubTickets** | **bool** | Include sub tickets | [optional]
**includeWaitingStates** | **bool** | Include tickets with waiting status | [optional]
**visibility** | **string** | Panel visibility | [optional]
**companies** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20068ContentCompanies[]**](InlineResponse20068ContentCompanies.md) | Array of panel companies | [optional]
**employees** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20068ContentEmployees[]**](InlineResponse20068ContentEmployees.md) | Array of panel employees | [optional]
**departments** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20068ContentDepartments[]**](InlineResponse20068ContentDepartments.md) | Array of panel departments | [optional]
**tags** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20068ContentTags[]**](InlineResponse20068ContentTags.md) | Array of panel tags | [optional]
**ticketTypes** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20068ContentTicketTypes[]**](InlineResponse20068ContentTicketTypes.md) | Array of panel ticket types | [optional]
**ticketStatus** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20068ContentTicketStatus[]**](InlineResponse20068ContentTicketStatus.md) | Array of panel ticket status | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

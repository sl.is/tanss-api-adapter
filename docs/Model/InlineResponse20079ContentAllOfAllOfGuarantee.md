# # InlineResponse20079ContentAllOfAllOfGuarantee

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**linkTypeId** | **int** | link type id of the assignment | [optional]
**linkId** | **int** | link id of the assignment | [optional]
**purchaseDate** | **int** | date when this device was purchased | [optional]
**guaranteeMonth** | **int** | number of months (device guarantee) | [optional]
**guaranteeExpire** | **int** | date when the guarantee expires | [optional]
**warrantyMonth** | **int** | number of months (device warranty) | [optional]
**warrantyExpire** | **int** | date when the warranty expires | [optional]
**remark** | **string** | Misc. remarks regarding the guarantee / warranty | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineResponse2002Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**comments** | **object[]** | contains all comments of this ticket | [optional]
**supports** | **object[]** | contains all supports of this ticket | [optional]
**mails** | [**\SLIS\Adapter\Tanss\Model\InlineResponse2002ContentMails[]**](InlineResponse2002ContentMails.md) | contains all mails of this ticket | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

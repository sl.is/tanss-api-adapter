# SLIS\Adapter\Tanss\SupportsApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1SupportsListPut()**](SupportsApi.md#apiV1SupportsListPut) | **PUT** /api/v1/supports/list | Get a support list


## `apiV1SupportsListPut()`

```php
apiV1SupportsListPut($inlineObject27): \SLIS\Adapter\Tanss\Model\InlineResponse20067
```

Get a support list

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\SupportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject27 = new \SLIS\Adapter\Tanss\Model\InlineObject27(); // \SLIS\Adapter\Tanss\Model\InlineObject27

try {
    $result = $apiInstance->apiV1SupportsListPut($inlineObject27);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupportsApi->apiV1SupportsListPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject27** | [**\SLIS\Adapter\Tanss\Model\InlineObject27**](../Model/InlineObject27.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20067**](../Model/InlineResponse20067.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

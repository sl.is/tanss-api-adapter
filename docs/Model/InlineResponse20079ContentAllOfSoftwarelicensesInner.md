# # InlineResponse20079ContentAllOfSoftwarelicensesInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the software license | [optional]
**companyId** | **int** | company id of the software license | [optional]
**remark** | **string** | misc. infos of for this software license | [optional]
**internalRemark** | **string** | internal remarks of the license (can contain passwords), which only is seen by technicians or freelancers with the given permission | [optional]
**serialNumber** | **string** | serial number | [optional]
**inventoryNumber** | **string** | inventory number | [optional]
**softwarelicenseTypeId** | **int** | id of the sofware license type | [optional]
**expirationDate** | **int** | date when the software license will expire | [optional]
**renew** | **bool** | if true, the license must be renewed upon expiring and therefore needs further \&quot;attention\&quot; | [optional]
**date** | **int** | starting date for this software license | [optional]
**maxNumberOfInstall** | **int** | maximum number of installations | [optional]
**numberOfVolumes** | **int** | number of volumes (i.e. cds) | [optional]
**active** | **bool** | true if the software license is still active | [optional]
**employeeId** | **int** | id of employee which uses this software license | [optional]
**articleNumber** | **string** | article number of the component | [optional]
**contractPrice** | **float** | price which is used in maintenace contracts | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

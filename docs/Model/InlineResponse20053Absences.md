# # InlineResponse20053Absences

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of this support | [optional]
**ticketId** | **int** | id of the connected ticket | [optional]
**date** | **int** | beginning of the support | [optional]
**duration** | **int** | duration of the service (in minutes) | [optional]
**location** | **string** | Defines, where a support takes place | [optional]
**linkTypeId** | **int** | link type of the assignment | [optional]
**linkId** | **int** | id of the assignment | [optional]
**typeId** | **int** | id of the support type | [optional]
**text** | **string** | description / text for this support entry | [optional]
**durationNotCharged** | **int** | duration which is not charged (in minutes) | [optional]
**employeeId** | **int** | id of the employee who has done this support | [optional]
**planningType** | **object** |  | [optional]
**costCenterId** | **int** | id of the cost center | [optional]
**reasonNotChargedText** | **string** | if support was not chatged, the reason / text goes here | [optional]
**reasonNotChargedId** | **int** | id of the \&quot;not charged reason\&quot; (if support was completely not charged) | [optional]
**reasonPartialNotChargedId** | **int** | id of the \&quot;not charged reason\&quot; (if support was partially not charged) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

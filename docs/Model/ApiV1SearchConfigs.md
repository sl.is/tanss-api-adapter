# # ApiV1SearchConfigs

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**company** | [**ApiV1SearchConfigsCompanyAllOf**](ApiV1SearchConfigsCompanyAllOf.md) |  | [optional]
**employee** | **object** |  | [optional]
**ticket** | **object** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

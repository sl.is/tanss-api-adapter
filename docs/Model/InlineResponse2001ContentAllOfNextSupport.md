# # InlineResponse2001ContentAllOfNextSupport

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**date** | **int** | date of the support | [optional]
**planningType** | **object** |  | [optional]
**employeeId** | **int** |  | [optional]
**location** | **string** | Defines, where a support takes place | [optional]
**duration** | **int** |  | [optional]
**durationApproach** | **int** |  | [optional]
**durationDeparture** | **int** |  | [optional]
**text** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

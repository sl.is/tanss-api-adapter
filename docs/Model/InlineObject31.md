# # InlineObject31

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the service | [optional]
**text** | **string** | name of the service | [optional]
**symbol** | **string** | filename of the image representing the service | [optional]
**command** | **string** | command which shall be executed (via batch file)  %1% is the placeholder which contains the ip address | [optional]
**active** | **bool** | is service active or not | [optional]
**callType** | **string** | defines how a service will be executed | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

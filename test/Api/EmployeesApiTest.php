<?php
/**
 * EmployeesApiTest
 * PHP version 7.3
 *
 * @category Class
 * @package  SLIS\Adapter\Tanss
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * TANSS API
 *
 * ## Documentation of the TANSS API. Version: 5.8.22.1  ### older versions Older versions of the API documentation can be found here: * [Docs for 5.8.21.6](https://api-doc.tanss.de/5.8.21.6/) * [Docs for 5.8.21.3](https://api-doc.tanss.de/5.8.21.3/) * [Docs for 5.8.21.1](https://api-doc.tanss.de/5.8.21.1/) * [Docs for 5.8.20.5](https://api-doc.tanss.de/5.8.20.5/) * [Docs for 5.8.20.4](https://api-doc.tanss.de/5.8.20.4/) * [Docs for 5.8.20.2](https://api-doc.tanss.de/5.8.20.2/) * [Docs for 5.8.19.3](https://api-doc.tanss.de/5.8.19.3/)
 *
 * The version of the OpenAPI document: 5.8.22.1
 * Contact: support@tanss.de
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 6.0.0-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace SLIS\Adapter\Tanss\Test\Api;

use \SLIS\Adapter\Tanss\Configuration;
use \SLIS\Adapter\Tanss\ApiException;
use \SLIS\Adapter\Tanss\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * EmployeesApiTest Class Doc Comment
 *
 * @category Class
 * @package  SLIS\Adapter\Tanss
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class EmployeesApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test case for apiV1EmployeesPost
     *
     * creates an employee.
     *
     */
    public function testApiV1EmployeesPost()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiV1EmployeesTechniciansGet
     *
     * Gets all technicians.
     *
     */
    public function testApiV1EmployeesTechniciansGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}

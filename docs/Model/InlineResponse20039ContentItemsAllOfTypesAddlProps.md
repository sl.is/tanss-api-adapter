# # InlineResponse20039ContentItemsAllOfTypesAddlProps

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**begin** | **int** | begin of period | [optional]
**end** | **int** | end of period | [optional]
**seconds** | **int** | duration of period (in seconds) | [optional]
**state** | **string** | A state representing the Timstamp period | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

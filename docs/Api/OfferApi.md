# SLIS\Adapter\Tanss\OfferApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1OfferOfferIdDelete()**](OfferApi.md#apiV1OfferOfferIdDelete) | **DELETE** /api/v1/offer/{offerId} | Deletes an offer
[**apiV1OfferOfferIdGet()**](OfferApi.md#apiV1OfferOfferIdGet) | **GET** /api/v1/offer/{offerId} | Gets an offer
[**apiV1OfferOfferIdPut()**](OfferApi.md#apiV1OfferOfferIdPut) | **PUT** /api/v1/offer/{offerId} | Updates an offer
[**apiV1OffersErpSelectionsErpSelectionIdDelete()**](OfferApi.md#apiV1OffersErpSelectionsErpSelectionIdDelete) | **DELETE** /api/v1/offers/erpSelections/{erpSelectionId} | Deletes an erp selection
[**apiV1OffersErpSelectionsErpSelectionIdGet()**](OfferApi.md#apiV1OffersErpSelectionsErpSelectionIdGet) | **GET** /api/v1/offers/erpSelections/{erpSelectionId} | Fetches an erp selection
[**apiV1OffersErpSelectionsErpSelectionIdPut()**](OfferApi.md#apiV1OffersErpSelectionsErpSelectionIdPut) | **PUT** /api/v1/offers/erpSelections/{erpSelectionId} | Updates an erp selection
[**apiV1OffersErpSelectionsMatPickerErpSelectionIdGet()**](OfferApi.md#apiV1OffersErpSelectionsMatPickerErpSelectionIdGet) | **GET** /api/v1/offers/erpSelections/matPicker/{erpSelectionId} | material picker for erp selection
[**apiV1OffersErpSelectionsMatPickerGet()**](OfferApi.md#apiV1OffersErpSelectionsMatPickerGet) | **GET** /api/v1/offers/erpSelections/matPicker | material picker
[**apiV1OffersErpSelectionsPost()**](OfferApi.md#apiV1OffersErpSelectionsPost) | **POST** /api/v1/offers/erpSelections | Creates a new erp selection
[**apiV1OffersPost()**](OfferApi.md#apiV1OffersPost) | **POST** /api/v1/offers | Creates an offer
[**apiV1OffersPut()**](OfferApi.md#apiV1OffersPut) | **PUT** /api/v1/offers | Gets list of offers
[**apiV1OffersTemplatesGet()**](OfferApi.md#apiV1OffersTemplatesGet) | **GET** /api/v1/offers/templates | Gets list of offer templates
[**apiV1OffersTemplatesPost()**](OfferApi.md#apiV1OffersTemplatesPost) | **POST** /api/v1/offers/templates | Creates an offer template
[**apiV1OffersTemplatesTemplateIdDelete()**](OfferApi.md#apiV1OffersTemplatesTemplateIdDelete) | **DELETE** /api/v1/offers/templates/{templateId} | Deletes an offer template
[**apiV1OffersTemplatesTemplateIdGet()**](OfferApi.md#apiV1OffersTemplatesTemplateIdGet) | **GET** /api/v1/offers/templates/{templateId} | Gets an offer templates
[**apiV1OffersTemplatesTemplateIdPut()**](OfferApi.md#apiV1OffersTemplatesTemplateIdPut) | **PUT** /api/v1/offers/templates/{templateId} | Updates an offer templates


## `apiV1OfferOfferIdDelete()`

```php
apiV1OfferOfferIdDelete($offerId)
```

Deletes an offer

Deletes an offer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\OfferApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$offerId = 56; // int | Id of the offer to be deleted

try {
    $apiInstance->apiV1OfferOfferIdDelete($offerId);
} catch (Exception $e) {
    echo 'Exception when calling OfferApi->apiV1OfferOfferIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offerId** | **int**| Id of the offer to be deleted |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1OfferOfferIdGet()`

```php
apiV1OfferOfferIdGet($offerId): \SLIS\Adapter\Tanss\Model\InlineResponse20122
```

Gets an offer

This route gets a detailled view of an offer (including vars and material)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\OfferApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$offerId = 56; // int | Id of the offer

try {
    $result = $apiInstance->apiV1OfferOfferIdGet($offerId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OfferApi->apiV1OfferOfferIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offerId** | **int**| Id of the offer |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20122**](../Model/InlineResponse20122.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1OfferOfferIdPut()`

```php
apiV1OfferOfferIdPut($offerId, $body): \SLIS\Adapter\Tanss\Model\InlineResponse20122
```

Updates an offer

This route updates an offer (including vars and material)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\OfferApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$offerId = 56; // int | Id of the offer
$body = new \stdClass; // object | offer object

try {
    $result = $apiInstance->apiV1OfferOfferIdPut($offerId, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OfferApi->apiV1OfferOfferIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offerId** | **int**| Id of the offer |
 **body** | **object**| offer object |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20122**](../Model/InlineResponse20122.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1OffersErpSelectionsErpSelectionIdDelete()`

```php
apiV1OffersErpSelectionsErpSelectionIdDelete($erpSelectionId)
```

Deletes an erp selection

Deletes an erp selection

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\OfferApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$erpSelectionId = 56; // int | Id of the erp selection to be deleted

try {
    $apiInstance->apiV1OffersErpSelectionsErpSelectionIdDelete($erpSelectionId);
} catch (Exception $e) {
    echo 'Exception when calling OfferApi->apiV1OffersErpSelectionsErpSelectionIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **erpSelectionId** | **int**| Id of the erp selection to be deleted |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1OffersErpSelectionsErpSelectionIdGet()`

```php
apiV1OffersErpSelectionsErpSelectionIdGet($erpSelectionId): \SLIS\Adapter\Tanss\Model\InlineResponse20120
```

Fetches an erp selection

Fetches information regarding a specific erp selection (including material)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\OfferApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$erpSelectionId = 56; // int | Id of the erp selection to be fetched

try {
    $result = $apiInstance->apiV1OffersErpSelectionsErpSelectionIdGet($erpSelectionId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OfferApi->apiV1OffersErpSelectionsErpSelectionIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **erpSelectionId** | **int**| Id of the erp selection to be fetched |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20120**](../Model/InlineResponse20120.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1OffersErpSelectionsErpSelectionIdPut()`

```php
apiV1OffersErpSelectionsErpSelectionIdPut($erpSelectionId, $body): \SLIS\Adapter\Tanss\Model\InlineResponse20120
```

Updates an erp selection

Updates an erp selection and saves all attached materials as well

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\OfferApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$erpSelectionId = 56; // int | Id of the erp selection to be updated
$body = new \stdClass; // object | erp selection to be saved (including materials)

try {
    $result = $apiInstance->apiV1OffersErpSelectionsErpSelectionIdPut($erpSelectionId, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OfferApi->apiV1OffersErpSelectionsErpSelectionIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **erpSelectionId** | **int**| Id of the erp selection to be updated |
 **body** | **object**| erp selection to be saved (including materials) |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20120**](../Model/InlineResponse20120.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1OffersErpSelectionsMatPickerErpSelectionIdGet()`

```php
apiV1OffersErpSelectionsMatPickerErpSelectionIdGet($erpSelectionId): \SLIS\Adapter\Tanss\Model\InlineResponse20050
```

material picker for erp selection

This route gets a list of materials for the \"material picker\" which is used to fill material in an \"erp selection\"

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\OfferApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$erpSelectionId = 56; // int | Id of the erp selection

try {
    $result = $apiInstance->apiV1OffersErpSelectionsMatPickerErpSelectionIdGet($erpSelectionId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OfferApi->apiV1OffersErpSelectionsMatPickerErpSelectionIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **erpSelectionId** | **int**| Id of the erp selection |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20050**](../Model/InlineResponse20050.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1OffersErpSelectionsMatPickerGet()`

```php
apiV1OffersErpSelectionsMatPickerGet($source, $searchText): \SLIS\Adapter\Tanss\Model\InlineResponse20049
```

material picker

This route gets a list of materials for the \"material picker\" which is used to fill material in an \"erp selection\"

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\OfferApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$source = 'source_example'; // string | determines the source for this picker
$searchText = 'searchText_example'; // string | if result should be filtered, the search text goes here

try {
    $result = $apiInstance->apiV1OffersErpSelectionsMatPickerGet($source, $searchText);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OfferApi->apiV1OffersErpSelectionsMatPickerGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **source** | **string**| determines the source for this picker |
 **searchText** | **string**| if result should be filtered, the search text goes here | [optional]

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20049**](../Model/InlineResponse20049.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1OffersErpSelectionsPost()`

```php
apiV1OffersErpSelectionsPost($inlineObject18): \SLIS\Adapter\Tanss\Model\InlineResponse20120
```

Creates a new erp selection

Creates a new erp selection which is used in offer templates. This erp selection contains all materials to be saved as well

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\OfferApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject18 = new \SLIS\Adapter\Tanss\Model\InlineObject18(); // \SLIS\Adapter\Tanss\Model\InlineObject18

try {
    $result = $apiInstance->apiV1OffersErpSelectionsPost($inlineObject18);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OfferApi->apiV1OffersErpSelectionsPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject18** | [**\SLIS\Adapter\Tanss\Model\InlineObject18**](../Model/InlineObject18.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20120**](../Model/InlineResponse20120.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1OffersPost()`

```php
apiV1OffersPost($uNKNOWNBASETYPE): \SLIS\Adapter\Tanss\Model\InlineResponse20122
```

Creates an offer

This route creates a new offer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\OfferApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$uNKNOWNBASETYPE = new \SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE(); // \SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE | offer object

try {
    $result = $apiInstance->apiV1OffersPost($uNKNOWNBASETYPE);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OfferApi->apiV1OffersPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uNKNOWNBASETYPE** | [**\SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)| offer object |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20122**](../Model/InlineResponse20122.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1OffersPut()`

```php
apiV1OffersPut($inlineObject19): \SLIS\Adapter\Tanss\Model\InlineResponse20051
```

Gets list of offers

This route gets a list of offers (by a given filter, trasmitted in the request body)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\OfferApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject19 = new \SLIS\Adapter\Tanss\Model\InlineObject19(); // \SLIS\Adapter\Tanss\Model\InlineObject19

try {
    $result = $apiInstance->apiV1OffersPut($inlineObject19);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OfferApi->apiV1OffersPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject19** | [**\SLIS\Adapter\Tanss\Model\InlineObject19**](../Model/InlineObject19.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20051**](../Model/InlineResponse20051.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1OffersTemplatesGet()`

```php
apiV1OffersTemplatesGet(): \SLIS\Adapter\Tanss\Model\InlineResponse20047
```

Gets list of offer templates

This route gets a list of all offer termplates

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\OfferApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiV1OffersTemplatesGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OfferApi->apiV1OffersTemplatesGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20047**](../Model/InlineResponse20047.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1OffersTemplatesPost()`

```php
apiV1OffersTemplatesPost($body): \SLIS\Adapter\Tanss\Model\InlineResponse20121
```

Creates an offer template

Creates a new offer template

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\OfferApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \stdClass; // object | offer templates

try {
    $result = $apiInstance->apiV1OffersTemplatesPost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OfferApi->apiV1OffersTemplatesPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **object**| offer templates |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20121**](../Model/InlineResponse20121.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1OffersTemplatesTemplateIdDelete()`

```php
apiV1OffersTemplatesTemplateIdDelete($templateId)
```

Deletes an offer template

Deletes an offer template

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\OfferApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$templateId = 56; // int | Id of the offer template to be deleted

try {
    $apiInstance->apiV1OffersTemplatesTemplateIdDelete($templateId);
} catch (Exception $e) {
    echo 'Exception when calling OfferApi->apiV1OffersTemplatesTemplateIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **templateId** | **int**| Id of the offer template to be deleted |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1OffersTemplatesTemplateIdGet()`

```php
apiV1OffersTemplatesTemplateIdGet($templateId): \SLIS\Adapter\Tanss\Model\InlineResponse20048
```

Gets an offer templates

This route gets a detailled view of an offer template

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\OfferApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$templateId = 56; // int | Id of the offer template

try {
    $result = $apiInstance->apiV1OffersTemplatesTemplateIdGet($templateId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OfferApi->apiV1OffersTemplatesTemplateIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **templateId** | **int**| Id of the offer template |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20048**](../Model/InlineResponse20048.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1OffersTemplatesTemplateIdPut()`

```php
apiV1OffersTemplatesTemplateIdPut($templateId, $body): \SLIS\Adapter\Tanss\Model\InlineResponse2023
```

Updates an offer templates

This route updates an offer template

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\OfferApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$templateId = 56; // int | Id of the offer template
$body = new \stdClass; // object | offer template

try {
    $result = $apiInstance->apiV1OffersTemplatesTemplateIdPut($templateId, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OfferApi->apiV1OffersTemplatesTemplateIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **templateId** | **int**| Id of the offer template |
 **body** | **object**| offer template |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2023**](../Model/InlineResponse2023.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

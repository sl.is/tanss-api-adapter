# # InlineResponse20024Employees

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**name** | **string** |  | [optional]
**salutation** | **string** |  | [optional]
**title** | **string** |  | [optional]
**firstName** | **string** |  | [optional]
**lastName** | **string** |  | [optional]
**email** | **string** |  | [optional]
**phoneNumber1** | **string** |  | [optional]
**phoneNumber2** | **string** |  | [optional]
**mobileNumber1** | **string** |  | [optional]
**mobileNumber2** | **string** |  | [optional]
**faxNumber** | **string** |  | [optional]
**room** | **string** |  | [optional]
**initials** | **string** |  | [optional]
**function** | **string** |  | [optional]
**active** | **bool** |  | [optional]
**externalId** | **string** |  | [optional]
**assignedToCustomers** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20024AssignedToCustomers[]**](InlineResponse20024AssignedToCustomers.md) |  | [optional]
**preferredCustomer** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20024PreferredCustomer**](InlineResponse20024PreferredCustomer.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# SLIS\Adapter\Tanss\ChecklistsApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1ChecklistsAssignmentLinkTypeIdLinkIdChecklistIdDelete()**](ChecklistsApi.md#apiV1ChecklistsAssignmentLinkTypeIdLinkIdChecklistIdDelete) | **DELETE** /api/v1/checklists/assignment/{linkTypeId}/{linkId}/{checklistId} | Removes a checklist from a ticket
[**apiV1ChecklistsAssignmentLinkTypeIdLinkIdChecklistIdPost()**](ChecklistsApi.md#apiV1ChecklistsAssignmentLinkTypeIdLinkIdChecklistIdPost) | **POST** /api/v1/checklists/assignment/{linkTypeId}/{linkId}/{checklistId} | Assigns a checklist to a ticket
[**apiV1ChecklistsAssignmentLinkTypeIdLinkIdGet()**](ChecklistsApi.md#apiV1ChecklistsAssignmentLinkTypeIdLinkIdGet) | **GET** /api/v1/checklists/assignment/{linkTypeId}/{linkId} | Gets checklists for a ticket
[**apiV1ChecklistsCheckPut()**](ChecklistsApi.md#apiV1ChecklistsCheckPut) | **PUT** /api/v1/checklists/check | Check an item
[**apiV1ChecklistsChecklistIdProcessGet()**](ChecklistsApi.md#apiV1ChecklistsChecklistIdProcessGet) | **GET** /api/v1/checklists/{checklistId}/process | Gets checklist for ticket


## `apiV1ChecklistsAssignmentLinkTypeIdLinkIdChecklistIdDelete()`

```php
apiV1ChecklistsAssignmentLinkTypeIdLinkIdChecklistIdDelete($linkTypeId, $linkId, $checklistId): \SLIS\Adapter\Tanss\Model\InlineResponse20063
```

Removes a checklist from a ticket

This route will remove a checklist checklist assignment from a ticket

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ChecklistsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$linkTypeId = 56; // int | linkTypeId (11 = ticket)
$linkId = 56; // int | id of the ticket
$checklistId = 56; // int | id of the checklist

try {
    $result = $apiInstance->apiV1ChecklistsAssignmentLinkTypeIdLinkIdChecklistIdDelete($linkTypeId, $linkId, $checklistId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChecklistsApi->apiV1ChecklistsAssignmentLinkTypeIdLinkIdChecklistIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **linkTypeId** | **int**| linkTypeId (11 &#x3D; ticket) |
 **linkId** | **int**| id of the ticket |
 **checklistId** | **int**| id of the checklist |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20063**](../Model/InlineResponse20063.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ChecklistsAssignmentLinkTypeIdLinkIdChecklistIdPost()`

```php
apiV1ChecklistsAssignmentLinkTypeIdLinkIdChecklistIdPost($linkTypeId, $linkId, $checklistId): \SLIS\Adapter\Tanss\Model\InlineResponse20062
```

Assigns a checklist to a ticket

This route will assign a checklist to a ticket

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ChecklistsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$linkTypeId = 56; // int | linkTypeId (11 = ticket)
$linkId = 56; // int | id of the ticket
$checklistId = 56; // int | id of the checklist

try {
    $result = $apiInstance->apiV1ChecklistsAssignmentLinkTypeIdLinkIdChecklistIdPost($linkTypeId, $linkId, $checklistId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChecklistsApi->apiV1ChecklistsAssignmentLinkTypeIdLinkIdChecklistIdPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **linkTypeId** | **int**| linkTypeId (11 &#x3D; ticket) |
 **linkId** | **int**| id of the ticket |
 **checklistId** | **int**| id of the checklist |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20062**](../Model/InlineResponse20062.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ChecklistsAssignmentLinkTypeIdLinkIdGet()`

```php
apiV1ChecklistsAssignmentLinkTypeIdLinkIdGet($linkTypeId, $linkId): \SLIS\Adapter\Tanss\Model\InlineResponse20064
```

Gets checklists for a ticket

This route will get all checklists for a specific assignment (i.e. ticket)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ChecklistsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$linkTypeId = 56; // int | linkTypeId (11 = ticket)
$linkId = 56; // int | id of the ticket

try {
    $result = $apiInstance->apiV1ChecklistsAssignmentLinkTypeIdLinkIdGet($linkTypeId, $linkId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChecklistsApi->apiV1ChecklistsAssignmentLinkTypeIdLinkIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **linkTypeId** | **int**| linkTypeId (11 &#x3D; ticket) |
 **linkId** | **int**| id of the ticket |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20064**](../Model/InlineResponse20064.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ChecklistsCheckPut()`

```php
apiV1ChecklistsCheckPut($inlineObject26): \SLIS\Adapter\Tanss\Model\InlineResponse20066
```

Check an item

This route will check (or uncheck) an item in a checklist

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ChecklistsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject26 = new \SLIS\Adapter\Tanss\Model\InlineObject26(); // \SLIS\Adapter\Tanss\Model\InlineObject26

try {
    $result = $apiInstance->apiV1ChecklistsCheckPut($inlineObject26);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChecklistsApi->apiV1ChecklistsCheckPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject26** | [**\SLIS\Adapter\Tanss\Model\InlineObject26**](../Model/InlineObject26.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20066**](../Model/InlineResponse20066.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ChecklistsChecklistIdProcessGet()`

```php
apiV1ChecklistsChecklistIdProcessGet($checklistId, $linkTypeId, $linkId): \SLIS\Adapter\Tanss\Model\InlineResponse20065
```

Gets checklist for ticket

This route will be used for processing a checklist.  It will fetch the checklist, along with all the values for the current assignment (i.e. ticket).  If you want to process checklist, this is the main part for fetching all neccessary infos.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ChecklistsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$checklistId = 56; // int | id of the checklist
$linkTypeId = 56; // int | linkTypeId (11 = ticket)
$linkId = 56; // int | id of the ticket

try {
    $result = $apiInstance->apiV1ChecklistsChecklistIdProcessGet($checklistId, $linkTypeId, $linkId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChecklistsApi->apiV1ChecklistsChecklistIdProcessGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **checklistId** | **int**| id of the checklist |
 **linkTypeId** | **int**| linkTypeId (11 &#x3D; ticket) |
 **linkId** | **int**| id of the ticket |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20065**](../Model/InlineResponse20065.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

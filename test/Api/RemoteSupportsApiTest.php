<?php
/**
 * RemoteSupportsApiTest
 * PHP version 7.3
 *
 * @category Class
 * @package  SLIS\Adapter\Tanss
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * TANSS API
 *
 * ## Documentation of the TANSS API. Version: 5.8.22.1  ### older versions Older versions of the API documentation can be found here: * [Docs for 5.8.21.6](https://api-doc.tanss.de/5.8.21.6/) * [Docs for 5.8.21.3](https://api-doc.tanss.de/5.8.21.3/) * [Docs for 5.8.21.1](https://api-doc.tanss.de/5.8.21.1/) * [Docs for 5.8.20.5](https://api-doc.tanss.de/5.8.20.5/) * [Docs for 5.8.20.4](https://api-doc.tanss.de/5.8.20.4/) * [Docs for 5.8.20.2](https://api-doc.tanss.de/5.8.20.2/) * [Docs for 5.8.19.3](https://api-doc.tanss.de/5.8.19.3/)
 *
 * The version of the OpenAPI document: 5.8.22.1
 * Contact: support@tanss.de
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 6.0.0-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace SLIS\Adapter\Tanss\Test\Api;

use \SLIS\Adapter\Tanss\Configuration;
use \SLIS\Adapter\Tanss\ApiException;
use \SLIS\Adapter\Tanss\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * RemoteSupportsApiTest Class Doc Comment
 *
 * @category Class
 * @package  SLIS\Adapter\Tanss
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class RemoteSupportsApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test case for apiRemoteSupportsV1AssignDeviceDelete
     *
     * Deletes a device assignment.
     *
     */
    public function testApiRemoteSupportsV1AssignDeviceDelete()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiRemoteSupportsV1AssignDeviceGet
     *
     * Gets all device assignments.
     *
     */
    public function testApiRemoteSupportsV1AssignDeviceGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiRemoteSupportsV1AssignDevicePost
     *
     * Creates a device assignment.
     *
     */
    public function testApiRemoteSupportsV1AssignDevicePost()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiRemoteSupportsV1AssignEmployeeDelete
     *
     * Delets a technician assignment.
     *
     */
    public function testApiRemoteSupportsV1AssignEmployeeDelete()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiRemoteSupportsV1AssignEmployeeGet
     *
     * Gets all technician assignments.
     *
     */
    public function testApiRemoteSupportsV1AssignEmployeeGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiRemoteSupportsV1AssignEmployeePost
     *
     * Creates a technician assignment.
     *
     */
    public function testApiRemoteSupportsV1AssignEmployeePost()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiRemoteSupportsV1Post
     *
     * Creates/imports a remote support into the database.
     *
     */
    public function testApiRemoteSupportsV1Post()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiRemoteSupportsV1Put
     *
     * Get list of remote supports.
     *
     */
    public function testApiRemoteSupportsV1Put()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiRemoteSupportsV1RemoteSupportIdDelete
     *
     * Delete remote support.
     *
     */
    public function testApiRemoteSupportsV1RemoteSupportIdDelete()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiRemoteSupportsV1RemoteSupportIdGet
     *
     * Get remote support by id.
     *
     */
    public function testApiRemoteSupportsV1RemoteSupportIdGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for apiRemoteSupportsV1RemoteSupportIdPut
     *
     * Updates a remote support.
     *
     */
    public function testApiRemoteSupportsV1RemoteSupportIdPut()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}

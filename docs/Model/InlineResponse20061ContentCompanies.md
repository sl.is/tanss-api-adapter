# # InlineResponse20061ContentCompanies

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id fo the company | [optional]
**name** | **string** | name of the company | [optional]
**street** | **string** | street of the company | [optional]
**postCode** | **string** | postal code of the company | [optional]
**city** | **string** | city of the company | [optional]
**country** | **string** | country of the company | [optional]
**displayId** | **string** | display if (customer nr.) | [optional]
**phoneNumber** | **string** | phone number | [optional]
**faxNumber** | **string** | fax number | [optional]
**email** | **string** | e-Mail of the company | [optional]
**website** | **string** | website of the company | [optional]
**inactive** | **bool** | if the company is incative, info is given here | [optional]
**lockout** | **bool** | defines, if the company is locked (n service may be entered) | [optional]
**centralType** | **string** | defines, if a company is a central, or a branch (or none of it) | [optional]
**personalCustomer** | **bool** | defines if the company is a \&quot;private customer\&quot; | [optional]
**mobileNumber** | **string** | mobile phone number (only if its a private customer) | [optional]
**privateNumber** | **string** | private phone number (only if its a private customer) | [optional]
**types** | **object[]** |  | [optional]
**anticipatedCallbacks** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20061ContentAnticipatedCallbacks[]**](InlineResponse20061ContentAnticipatedCallbacks.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineResponse20040ContentItemsAllOf1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**history** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20040ContentItemsAllOfHistoryInner[]**](InlineResponse20040ContentItemsAllOfHistoryInner.md) | history of all timestamp changes of this employee / day | [optional] [readonly]
**dayClosing** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20110Content**](InlineResponse20110Content.md) |  | [optional]
**editMode** | **string** | describes how a timestamp was (or may be) edited * NONE &#x3D; may not be edited * MAY_REQUEST &#x3D; user may only request changes * DIRECT_ACCESS &#x3D; timestamps may be modified directyl | [optional]
**changesRequested** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20040ContentItemsAllOfChangesRequestedInner[]**](InlineResponse20040ContentItemsAllOfChangesRequestedInner.md) | if any change requests are present for this user / day, they are given here | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

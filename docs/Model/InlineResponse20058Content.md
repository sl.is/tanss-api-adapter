# # InlineResponse20058Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**date** | **int** | date of the log entry | [optional]
**employeeId** | **int** | employee who has done the log entry | [optional]
**logType** | **string** |  | [optional]
**linkTypeId** | **int** | linkType of the assignment | [optional]
**linkId** | **int** | id of the assignment | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

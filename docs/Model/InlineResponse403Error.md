# # InlineResponse403Error

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**text** | **string** | unique type to identify the type of the exception | [optional]
**localizedText** | **string** | localized error message (currently only in english) | [optional]
**thrownExceptionMessage** | **string** | detailled message of a general java exception | [optional]
**type** | **string** | name of the TANSS exception | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

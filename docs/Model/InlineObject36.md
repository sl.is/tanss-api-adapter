# # InlineObject36

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**\SLIS\Adapter\Tanss\Model\ApiV1IdentifyItems[]**](ApiV1IdentifyItems.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

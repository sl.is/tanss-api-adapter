# # InlineResponse20040ContentItemsAllOfChangesRequestedInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**employeeId** | **int** | id of the employee | [optional]
**date** | **int** | date | [optional]
**state** | **object** |  | [optional]
**type** | **object** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

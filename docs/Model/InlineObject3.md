# # InlineObject3

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the phone call (in the TANSS database) | [optional]
**callId** | **string** | (Optional) Describes the external id of the phone call, which is used by the remote phone system | [optional]
**telephoneSystemId** | **int** | (Optional) If more than one remote phone systems is used, represents the \&quot;number\&quot; of the phone system | [optional]
**date** | **int** | Unix timestamp representing the begin of the phone call | [optional]
**fromPhoneNumber** | **string** | Phone number of the caller (&#x3D; \&quot;from\&quot; number) | [optional]
**fromPhoneNrInfos** | [**\SLIS\Adapter\Tanss\Model\ApiCallsV1FromPhoneNrInfos**](ApiCallsV1FromPhoneNrInfos.md) |  | [optional]
**toPhoneNumber** | **string** | Phone number of the called (&#x3D; \&quot;to\&quot; number) | [optional]
**toPhoneNrInfos** | **object** |  | [optional]
**direction** | **string** | Defines the \&quot;direction\&quot; of a phone call | [optional]
**connectionEstablished** | **bool** | True if a connection was established, otherwise will count as a \&quot;try\&quot; | [optional]
**durationTotal** | **int** | Total duration of the call (plus ring time) in seconds | [optional]
**durationCall** | **int** | Duration of the call (only time of actual connection) in seconds | [optional]
**group** | **string** | (Optional) A group can be assigned to the call (as a string). This is an optional feature. | [optional]
**phoneParticipants** | [**\SLIS\Adapter\Tanss\Model\ApiCallsV1PhoneParticipants[]**](ApiCallsV1PhoneParticipants.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineResponse20065ContentAllOfItemsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the item | [optional]
**checklistId** | **int** | id of the checklist, which the item is contained in | [optional]
**type** | **string** | defines the type of a checklist item | [optional]
**heading** | **string** | headinh / title of the checklist item | [optional]
**description** | **string** | description / text of the checklist item | [optional]
**entryPoint** | **bool** | true if this is the first item of a checklist | [optional]
**nextItemId** | **int** | id of the \&quot;next\&quot; field in the process chain | [optional]
**serviceText** | **string** | text which is used when creating a support | [optional]
**hidden** | **bool** | true if thes field is hidden | [optional]
**action** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20065ContentAllOfItemsItemsAction**](InlineResponse20065ContentAllOfItemsItemsAction.md) |  | [optional]
**vars** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20048ContentItemsAllOfVarsInner[]**](InlineResponse20048ContentItemsAllOfVarsInner.md) | if vars were used in teh text, these must be given in the \&quot;check\&quot; body to fill in the correct values / infos for checking the item | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

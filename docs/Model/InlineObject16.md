# # InlineObject16

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chatId** | **int** | id of the corresponding chat id | [optional]
**content** | **string** | actual content / message | [optional]
**expectedResponse** | **int** | (optional) number of minutes in which a response is expected | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineResponse20045Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**closeRequests** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20045ContentCloseRequests[]**](InlineResponse20045ContentCloseRequests.md) |  | [optional]
**chatsWithUnreadMessages** | **object[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

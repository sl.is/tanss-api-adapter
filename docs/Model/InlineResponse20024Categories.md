# # InlineResponse20024Categories

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**name** | **string** |  | [optional]
**section** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20024Section**](InlineResponse20024Section.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

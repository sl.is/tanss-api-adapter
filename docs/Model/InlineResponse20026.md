# # InlineResponse20026

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20026Meta**](InlineResponse20026Meta.md) |  | [optional]
**content** | **object** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

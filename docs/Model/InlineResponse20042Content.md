# # InlineResponse20042Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**fromMinues** | **int** | Describes the amount of minutes needed to require a minimum pause (defined in \&quot;minimumPause\&quot;) | [optional]
**minimumPause** | **int** | A working period must contain at least a pause of x minutes | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

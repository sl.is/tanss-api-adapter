# # InlineResponse20078Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timerId** | **int** | id of the timer id for this fragment | [optional]
**startTime** | **int** | starting time of the fragment | [optional]
**stopTime** | **int** | end time of the fragment (if 0 means that the timer is still running) | [optional]
**note** | **string** | a note for this fragment. You could for example make notes of things which are done in this fragment which should help generating the support for this timer | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

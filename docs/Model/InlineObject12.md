# # InlineObject12

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**employeeId** | **int** | id of the employee for this day closing | [optional]
**date** | **\DateTime** | day for this day closing (YYYY-mm-dd) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

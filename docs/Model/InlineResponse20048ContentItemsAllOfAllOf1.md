# # InlineResponse20048ContentItemsAllOfAllOf1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**erpSelections** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20048ContentItemsAllOfAllOfErpSelectionsInner[]**](InlineResponse20048ContentItemsAllOfAllOfErpSelectionsInner.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

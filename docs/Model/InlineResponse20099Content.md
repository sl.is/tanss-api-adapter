# # InlineResponse20099Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the company type | [optional]
**name** | **string** | name of the company type | [optional]
**categoryId** | **int** | id of the category for this type | [optional]
**categoryName** | **string** | name of the category for this type | [optional]
**icon** | **string** | if this company type has an icon, here the name is given | [optional]
**hidden** | **bool** | if true, the company type is hidden | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

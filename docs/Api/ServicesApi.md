# SLIS\Adapter\Tanss\ServicesApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1ServicesGet()**](ServicesApi.md#apiV1ServicesGet) | **GET** /api/v1/services | Gets a list of all services
[**apiV1ServicesIdDelete()**](ServicesApi.md#apiV1ServicesIdDelete) | **DELETE** /api/v1/services/{id} | Deletes a service
[**apiV1ServicesIdGet()**](ServicesApi.md#apiV1ServicesIdGet) | **GET** /api/v1/services/{id} | Gets a service by id
[**apiV1ServicesIdPut()**](ServicesApi.md#apiV1ServicesIdPut) | **PUT** /api/v1/services/{id} | Updates a service
[**apiV1ServicesPost()**](ServicesApi.md#apiV1ServicesPost) | **POST** /api/v1/services | Creates a service


## `apiV1ServicesGet()`

```php
apiV1ServicesGet(): \SLIS\Adapter\Tanss\Model\InlineResponse20089
```

Gets a list of all services

Gets a list of all defined services

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiV1ServicesGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServicesApi->apiV1ServicesGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20089**](../Model/InlineResponse20089.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ServicesIdDelete()`

```php
apiV1ServicesIdDelete($id)
```

Deletes a service

Deletes a service.  Will only succeed if this service isn't assigned to any ip addresses

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int

try {
    $apiInstance->apiV1ServicesIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling ServicesApi->apiV1ServicesIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ServicesIdGet()`

```php
apiV1ServicesIdGet($id): \SLIS\Adapter\Tanss\Model\InlineResponse20147
```

Gets a service by id

Fetches a service given id

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int

try {
    $result = $apiInstance->apiV1ServicesIdGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServicesApi->apiV1ServicesIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20147**](../Model/InlineResponse20147.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ServicesIdPut()`

```php
apiV1ServicesIdPut($id, $body): \SLIS\Adapter\Tanss\Model\InlineResponse20147
```

Updates a service

Updates a services.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int
$body = new \stdClass; // object | service object to be saved

try {
    $result = $apiInstance->apiV1ServicesIdPut($id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServicesApi->apiV1ServicesIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **body** | **object**| service object to be saved |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20147**](../Model/InlineResponse20147.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ServicesPost()`

```php
apiV1ServicesPost($inlineObject31): \SLIS\Adapter\Tanss\Model\InlineResponse20147
```

Creates a service

Creates a service  Services are used in pcs or periphery (on ip addresses) to launch an external software

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ServicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject31 = new \SLIS\Adapter\Tanss\Model\InlineObject31(); // \SLIS\Adapter\Tanss\Model\InlineObject31

try {
    $result = $apiInstance->apiV1ServicesPost($inlineObject31);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServicesApi->apiV1ServicesPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject31** | [**\SLIS\Adapter\Tanss\Model\InlineObject31**](../Model/InlineObject31.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20147**](../Model/InlineResponse20147.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

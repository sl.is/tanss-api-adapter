# # InlineResponse20079ContentAllOfServiceIconsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | Defines the \&quot;type\&quot; of the service icon | [optional]
**name** | **string** | name of the service | [optional]
**address** | **string** | address which is used to establish the connection (mostly the ip address) | [optional]
**symbol** | **string** | if the service has an \&quot;own\&quot; icon, here the name of the image is given | [optional]
**command** | **string** | if the service has a special \&quot;call\&quot; (command line or url), the command is given | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineObject10

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**voucherId** | **int** | Id of the voucher | [optional]
**invoiceNumber** | **string** | Number of the invoice | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

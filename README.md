# TanssAdapter

## Documentation of the TANSS API. Version: 5.8.22.1

### older versions
Older versions of the API documentation can be found here:
* [Docs for 5.8.21.6](https://api-doc.tanss.de/5.8.21.6/)
* [Docs for 5.8.21.3](https://api-doc.tanss.de/5.8.21.3/)
* [Docs for 5.8.21.1](https://api-doc.tanss.de/5.8.21.1/)
* [Docs for 5.8.20.5](https://api-doc.tanss.de/5.8.20.5/)
* [Docs for 5.8.20.4](https://api-doc.tanss.de/5.8.20.4/)
* [Docs for 5.8.20.2](https://api-doc.tanss.de/5.8.20.2/)
* [Docs for 5.8.19.3](https://api-doc.tanss.de/5.8.19.3/)



## Installation & Usage

### Requirements

PHP 7.3 and later.
Should also work with PHP 8.0 or 8.1 but has not been tested.

### Composer

To install the bindings via [Composer](https://getcomposer.org/), add the following to `composer.json`:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://github.com/slis/tanss-php-adapter.git"
    }
  ],
  "require": {
    "slis/tanss-php-adapter": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
<?php
require_once('/path/to/TanssAdapter/vendor/autoload.php');
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');




$apiInstance = new SLIS\Adapter\Tanss\Api\AvailabilityApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$employeeIds = 'employeeIds_example'; // string | Ids of the employees (comma separated)

try {
    $result = $apiInstance->apiV1AvailabilityPost($employeeIds);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AvailabilityApi->apiV1AvailabilityPost: ', $e->getMessage(), PHP_EOL;
}

```

## API Endpoints

All URIs are relative to *http://localhost*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AvailabilityApi* | [**apiV1AvailabilityPost**](docs/Api/AvailabilityApi.md#apiv1availabilitypost) | **POST** /api/v1/availability | Fetches availability infos
*CallbackApi* | [**apiV1CallbacksCallbackIdGet**](docs/Api/CallbackApi.md#apiv1callbackscallbackidget) | **GET** /api/v1/callbacks/{callbackId} | Gets a callback
*CallbackApi* | [**apiV1CallbacksCallbackIdPut**](docs/Api/CallbackApi.md#apiv1callbackscallbackidput) | **PUT** /api/v1/callbacks/{callbackId} | Updates a callback
*CallbackApi* | [**apiV1CallbacksPost**](docs/Api/CallbackApi.md#apiv1callbackspost) | **POST** /api/v1/callbacks | Creates a callback
*CallbackApi* | [**apiV1CallbacksPut**](docs/Api/CallbackApi.md#apiv1callbacksput) | **PUT** /api/v1/callbacks | Get a list of callbacks
*CallsApi* | [**apiCallsV1EmployeeAssignmentDelete**](docs/Api/CallsApi.md#apicallsv1employeeassignmentdelete) | **DELETE** /api/calls/v1/employeeAssignment | Deletes an employee assignment
*CallsApi* | [**apiCallsV1EmployeeAssignmentGet**](docs/Api/CallsApi.md#apicallsv1employeeassignmentget) | **GET** /api/calls/v1/employeeAssignment | Get all employee assignments
*CallsApi* | [**apiCallsV1EmployeeAssignmentPost**](docs/Api/CallsApi.md#apicallsv1employeeassignmentpost) | **POST** /api/calls/v1/employeeAssignment | Creates a new employee assignment
*CallsApi* | [**apiCallsV1IdGet**](docs/Api/CallsApi.md#apicallsv1idget) | **GET** /api/calls/v1/{id} | Get phone call by id
*CallsApi* | [**apiCallsV1IdPut**](docs/Api/CallsApi.md#apicallsv1idput) | **PUT** /api/calls/v1/{id} | Update phone call
*CallsApi* | [**apiCallsV1NotificationPost**](docs/Api/CallsApi.md#apicallsv1notificationpost) | **POST** /api/calls/v1/notification | Creates a call notification
*CallsApi* | [**apiCallsV1Post**](docs/Api/CallsApi.md#apicallsv1post) | **POST** /api/calls/v1 | Creates/imports a phone call into the database
*CallsApi* | [**apiCallsV1Put**](docs/Api/CallsApi.md#apicallsv1put) | **PUT** /api/calls/v1 | Get a list of phone calls
*ChatsApi* | [**apiV1ChatsChatIdGet**](docs/Api/ChatsApi.md#apiv1chatschatidget) | **GET** /api/v1/chats/{chatId} | Gets a chat
*ChatsApi* | [**apiV1ChatsCloseChatIdPost**](docs/Api/ChatsApi.md#apiv1chatsclosechatidpost) | **POST** /api/v1/chats/close/{chatId} | Closes a chat
*ChatsApi* | [**apiV1ChatsCloseChatIdPut**](docs/Api/ChatsApi.md#apiv1chatsclosechatidput) | **PUT** /api/v1/chats/close/{chatId} | Accept/decline close request
*ChatsApi* | [**apiV1ChatsCloseRequestsGet**](docs/Api/ChatsApi.md#apiv1chatscloserequestsget) | **GET** /api/v1/chats/closeRequests | Gets chat close requests
*ChatsApi* | [**apiV1ChatsMessagesPost**](docs/Api/ChatsApi.md#apiv1chatsmessagespost) | **POST** /api/v1/chats/messages | Creates a new chat message
*ChatsApi* | [**apiV1ChatsParticipantsDelete**](docs/Api/ChatsApi.md#apiv1chatsparticipantsdelete) | **DELETE** /api/v1/chats/participants | Deletes a participant
*ChatsApi* | [**apiV1ChatsParticipantsPost**](docs/Api/ChatsApi.md#apiv1chatsparticipantspost) | **POST** /api/v1/chats/participants | Adds a participant
*ChatsApi* | [**apiV1ChatsPost**](docs/Api/ChatsApi.md#apiv1chatspost) | **POST** /api/v1/chats | Creates a new chat
*ChatsApi* | [**apiV1ChatsPut**](docs/Api/ChatsApi.md#apiv1chatsput) | **PUT** /api/v1/chats | Get a list of chats
*ChatsApi* | [**apiV1ChatsReOpenChatIdPost**](docs/Api/ChatsApi.md#apiv1chatsreopenchatidpost) | **POST** /api/v1/chats/reOpen/{chatId} | re-opens a chat
*ChecklistsApi* | [**apiV1ChecklistsAssignmentLinkTypeIdLinkIdChecklistIdDelete**](docs/Api/ChecklistsApi.md#apiv1checklistsassignmentlinktypeidlinkidchecklistiddelete) | **DELETE** /api/v1/checklists/assignment/{linkTypeId}/{linkId}/{checklistId} | Removes a checklist from a ticket
*ChecklistsApi* | [**apiV1ChecklistsAssignmentLinkTypeIdLinkIdChecklistIdPost**](docs/Api/ChecklistsApi.md#apiv1checklistsassignmentlinktypeidlinkidchecklistidpost) | **POST** /api/v1/checklists/assignment/{linkTypeId}/{linkId}/{checklistId} | Assigns a checklist to a ticket
*ChecklistsApi* | [**apiV1ChecklistsAssignmentLinkTypeIdLinkIdGet**](docs/Api/ChecklistsApi.md#apiv1checklistsassignmentlinktypeidlinkidget) | **GET** /api/v1/checklists/assignment/{linkTypeId}/{linkId} | Gets checklists for a ticket
*ChecklistsApi* | [**apiV1ChecklistsCheckPut**](docs/Api/ChecklistsApi.md#apiv1checklistscheckput) | **PUT** /api/v1/checklists/check | Check an item
*ChecklistsApi* | [**apiV1ChecklistsChecklistIdProcessGet**](docs/Api/ChecklistsApi.md#apiv1checklistschecklistidprocessget) | **GET** /api/v1/checklists/{checklistId}/process | Gets checklist for ticket
*CompanyApi* | [**apiV1CompaniesPost**](docs/Api/CompanyApi.md#apiv1companiespost) | **POST** /api/v1/companies | Creates a new company
*CompanyCategoryApi* | [**apiV1CompanyCategoriesGet**](docs/Api/CompanyCategoryApi.md#apiv1companycategoriesget) | **GET** /api/v1/companyCategories | list of categories
*CompanyCategoryApi* | [**apiV1CompanyCategoriesIdDelete**](docs/Api/CompanyCategoryApi.md#apiv1companycategoriesiddelete) | **DELETE** /api/v1/companyCategories/{id} | Deletes a company category
*CompanyCategoryApi* | [**apiV1CompanyCategoriesIdGet**](docs/Api/CompanyCategoryApi.md#apiv1companycategoriesidget) | **GET** /api/v1/companyCategories/{id} | gets a category
*CompanyCategoryApi* | [**apiV1CompanyCategoriesIdPut**](docs/Api/CompanyCategoryApi.md#apiv1companycategoriesidput) | **PUT** /api/v1/companyCategories/{id} | updates a category
*CompanyCategoryApi* | [**apiV1CompanyCategoriesPost**](docs/Api/CompanyCategoryApi.md#apiv1companycategoriespost) | **POST** /api/v1/companyCategories | Creates a new company category
*CompanyCategoryApi* | [**apiV1CompanyCategoriesTypesGet**](docs/Api/CompanyCategoryApi.md#apiv1companycategoriestypesget) | **GET** /api/v1/companyCategories/types | list of company types
*CompanyCategoryApi* | [**apiV1CompanyCategoriesTypesIdDelete**](docs/Api/CompanyCategoryApi.md#apiv1companycategoriestypesiddelete) | **DELETE** /api/v1/companyCategories/types/{id} | Deletes a company type
*CompanyCategoryApi* | [**apiV1CompanyCategoriesTypesIdGet**](docs/Api/CompanyCategoryApi.md#apiv1companycategoriestypesidget) | **GET** /api/v1/companyCategories/types/{id} | gets a company type
*CompanyCategoryApi* | [**apiV1CompanyCategoriesTypesIdPut**](docs/Api/CompanyCategoryApi.md#apiv1companycategoriestypesidput) | **PUT** /api/v1/companyCategories/types/{id} | updates a company type
*CompanyCategoryApi* | [**apiV1CompanyCategoriesTypesPost**](docs/Api/CompanyCategoryApi.md#apiv1companycategoriestypespost) | **POST** /api/v1/companyCategories/types | Creates a new company type
*ComponentsApi* | [**apiV1ComponentsComponentIdDelete**](docs/Api/ComponentsApi.md#apiv1componentscomponentiddelete) | **DELETE** /api/v1/components/{componentId} | Deletes a component
*ComponentsApi* | [**apiV1ComponentsComponentIdGet**](docs/Api/ComponentsApi.md#apiv1componentscomponentidget) | **GET** /api/v1/components/{componentId} | Gets a component by id
*ComponentsApi* | [**apiV1ComponentsComponentIdPut**](docs/Api/ComponentsApi.md#apiv1componentscomponentidput) | **PUT** /api/v1/components/{componentId} | Updates a component
*ComponentsApi* | [**apiV1ComponentsPost**](docs/Api/ComponentsApi.md#apiv1componentspost) | **POST** /api/v1/components | Creates a component
*ComponentsApi* | [**apiV1ComponentsPut**](docs/Api/ComponentsApi.md#apiv1componentsput) | **PUT** /api/v1/components | Gets a list of components
*ComponentsApi* | [**apiV1ComponentsTypesGet**](docs/Api/ComponentsApi.md#apiv1componentstypesget) | **GET** /api/v1/components/types | Gets a list of component types
*ComponentsApi* | [**apiV1ComponentsTypesPost**](docs/Api/ComponentsApi.md#apiv1componentstypespost) | **POST** /api/v1/components/types | Create component type
*ComponentsApi* | [**apiV1ComponentsTypesTypeIdDelete**](docs/Api/ComponentsApi.md#apiv1componentstypestypeiddelete) | **DELETE** /api/v1/components/types/{typeId} | Delete component type
*ComponentsApi* | [**apiV1ComponentsTypesTypeIdPut**](docs/Api/ComponentsApi.md#apiv1componentstypestypeidput) | **PUT** /api/v1/components/types/{typeId} | Update component type
*CpusApi* | [**apiV1CpusGet**](docs/Api/CpusApi.md#apiv1cpusget) | **GET** /api/v1/cpus | Get a list of all cpus
*CpusApi* | [**apiV1CpusIdGet**](docs/Api/CpusApi.md#apiv1cpusidget) | **GET** /api/v1/cpus/{id} | Get a cpu
*CpusApi* | [**apiV1CpusIdPut**](docs/Api/CpusApi.md#apiv1cpusidput) | **PUT** /api/v1/cpus/{id} | Updates a cpu
*CpusApi* | [**apiV1CpusPost**](docs/Api/CpusApi.md#apiv1cpuspost) | **POST** /api/v1/cpus | Creates a new cpu
*EmployeesApi* | [**apiV1EmployeesPost**](docs/Api/EmployeesApi.md#apiv1employeespost) | **POST** /api/v1/employees | creates an employee
*EmployeesApi* | [**apiV1EmployeesTechniciansGet**](docs/Api/EmployeesApi.md#apiv1employeestechniciansget) | **GET** /api/v1/employees/technicians | Gets all technicians
*ErpApi* | [**apiErpV1CompaniesDepartmentsGet**](docs/Api/ErpApi.md#apierpv1companiesdepartmentsget) | **GET** /api/erp/v1/companies/departments | gets all departments
*ErpApi* | [**apiErpV1CompaniesEmployeesDepartmentsGet**](docs/Api/ErpApi.md#apierpv1companiesemployeesdepartmentsget) | **GET** /api/erp/v1/companies/employees/departments/ | gets all users with the associated departments
*ErpApi* | [**apiErpV1CompaniesEmployeesGet**](docs/Api/ErpApi.md#apierpv1companiesemployeesget) | **GET** /api/erp/v1/companies/employees | get all employees from the own company
*ErpApi* | [**apiErpV1CompaniesSearchIdDisplayIdGet**](docs/Api/ErpApi.md#apierpv1companiessearchiddisplayidget) | **GET** /api/erp/v1/companies/searchId/{displayId} | search for a company
*ErpApi* | [**apiErpV1CompanyCategoriesGet**](docs/Api/ErpApi.md#apierpv1companycategoriesget) | **GET** /api/erp/v1/companyCategories | list of company categories
*ErpApi* | [**apiErpV1CustomersGet**](docs/Api/ErpApi.md#apierpv1customersget) | **GET** /api/erp/v1/customers | Get a list of customers and employees
*ErpApi* | [**apiErpV1CustomersPost**](docs/Api/ErpApi.md#apierpv1customerspost) | **POST** /api/erp/v1/customers | Insert new customers
*ErpApi* | [**apiErpV1DepartmentsDepartmentIdEmployeesGet**](docs/Api/ErpApi.md#apierpv1departmentsdepartmentidemployeesget) | **GET** /api/erp/v1/departments/{departmentId}/employees | gets all employees of a department
*ErpApi* | [**apiErpV1EmployeesEmployeeIdDepartmentsGet**](docs/Api/ErpApi.md#apierpv1employeesemployeeiddepartmentsget) | **GET** /api/erp/v1/employees/{employeeId}/departments | gets all departments of a employee
*ErpApi* | [**apiErpV1InvoicesGet**](docs/Api/ErpApi.md#apierpv1invoicesget) | **GET** /api/erp/v1/invoices | Gets a list of billable supports
*ErpApi* | [**apiErpV1InvoicesPost**](docs/Api/ErpApi.md#apierpv1invoicespost) | **POST** /api/erp/v1/invoices | Insert new invoices
*ErpApi* | [**apiErpV1TicketsPost**](docs/Api/ErpApi.md#apierpv1ticketspost) | **POST** /api/erp/v1/tickets | create a new ticket
*ErpApi* | [**apiErpV1TicketsStatusGet**](docs/Api/ErpApi.md#apierpv1ticketsstatusget) | **GET** /api/erp/v1/tickets/status | gets a list of tickets states
*ErpApi* | [**apiErpV1TicketsTypesGet**](docs/Api/ErpApi.md#apierpv1ticketstypesget) | **GET** /api/erp/v1/tickets/types | gets a list of tickets types
*IdentifyApi* | [**apiV1IdentifyPost**](docs/Api/IdentifyApi.md#apiv1identifypost) | **POST** /api/v1/identify | identifies items
*MailsApi* | [**apiV1MailsTestSmtpPost**](docs/Api/MailsApi.md#apiv1mailstestsmtppost) | **POST** /api/v1/mails/test/smtp | Test email smtp settings
*ManufacturerApi* | [**apiV1CpusIdDelete**](docs/Api/ManufacturerApi.md#apiv1cpusiddelete) | **DELETE** /api/v1/cpus/{id} | Deletes a cpu
*ManufacturerApi* | [**apiV1ManufacturersGet**](docs/Api/ManufacturerApi.md#apiv1manufacturersget) | **GET** /api/v1/manufacturers | Get a list of all manufacturers
*ManufacturerApi* | [**apiV1ManufacturersIdDelete**](docs/Api/ManufacturerApi.md#apiv1manufacturersiddelete) | **DELETE** /api/v1/manufacturers/{id} | Deletes a manufacturer
*ManufacturerApi* | [**apiV1ManufacturersIdGet**](docs/Api/ManufacturerApi.md#apiv1manufacturersidget) | **GET** /api/v1/manufacturers/{id} | Get a manufacturer
*ManufacturerApi* | [**apiV1ManufacturersIdPut**](docs/Api/ManufacturerApi.md#apiv1manufacturersidput) | **PUT** /api/v1/manufacturers/{id} | Updates a manufacturer
*ManufacturerApi* | [**apiV1ManufacturersPost**](docs/Api/ManufacturerApi.md#apiv1manufacturerspost) | **POST** /api/v1/manufacturers | Creates a new manufacturer
*MonitoringApi* | [**apiMonitoringV1AssignGroupDelete**](docs/Api/MonitoringApi.md#apimonitoringv1assigngroupdelete) | **DELETE** /api/monitoring/v1/assignGroup | Delete a group assignment
*MonitoringApi* | [**apiMonitoringV1AssignGroupGet**](docs/Api/MonitoringApi.md#apimonitoringv1assigngroupget) | **GET** /api/monitoring/v1/assignGroup | Gets all group assignments
*MonitoringApi* | [**apiMonitoringV1AssignGroupPost**](docs/Api/MonitoringApi.md#apimonitoringv1assigngrouppost) | **POST** /api/monitoring/v1/assignGroup | Assigns a groupName to a company or device
*MonitoringApi* | [**apiMonitoringV1TicketFromGroupGet**](docs/Api/MonitoringApi.md#apimonitoringv1ticketfromgroupget) | **GET** /api/monitoring/v1/ticketFromGroup | Gets ticket(s), based on a given group
*MonitoringApi* | [**apiMonitoringV1TicketPost**](docs/Api/MonitoringApi.md#apimonitoringv1ticketpost) | **POST** /api/monitoring/v1/ticket | Creates a ticket, using the monitoring api
*MonitoringApi* | [**apiMonitoringV1TicketTicketIdGet**](docs/Api/MonitoringApi.md#apimonitoringv1ticketticketidget) | **GET** /api/monitoring/v1/ticket/{ticketId} | Gets a ticket (created by the monitoring api) by id
*MonitoringApi* | [**apiMonitoringV1TicketTicketIdPut**](docs/Api/MonitoringApi.md#apimonitoringv1ticketticketidput) | **PUT** /api/monitoring/v1/ticket/{ticketId} | Updates a ticket (created by the monitoring api) by id
*OfferApi* | [**apiV1OfferOfferIdDelete**](docs/Api/OfferApi.md#apiv1offerofferiddelete) | **DELETE** /api/v1/offer/{offerId} | Deletes an offer
*OfferApi* | [**apiV1OfferOfferIdGet**](docs/Api/OfferApi.md#apiv1offerofferidget) | **GET** /api/v1/offer/{offerId} | Gets an offer
*OfferApi* | [**apiV1OfferOfferIdPut**](docs/Api/OfferApi.md#apiv1offerofferidput) | **PUT** /api/v1/offer/{offerId} | Updates an offer
*OfferApi* | [**apiV1OffersErpSelectionsErpSelectionIdDelete**](docs/Api/OfferApi.md#apiv1offerserpselectionserpselectioniddelete) | **DELETE** /api/v1/offers/erpSelections/{erpSelectionId} | Deletes an erp selection
*OfferApi* | [**apiV1OffersErpSelectionsErpSelectionIdGet**](docs/Api/OfferApi.md#apiv1offerserpselectionserpselectionidget) | **GET** /api/v1/offers/erpSelections/{erpSelectionId} | Fetches an erp selection
*OfferApi* | [**apiV1OffersErpSelectionsErpSelectionIdPut**](docs/Api/OfferApi.md#apiv1offerserpselectionserpselectionidput) | **PUT** /api/v1/offers/erpSelections/{erpSelectionId} | Updates an erp selection
*OfferApi* | [**apiV1OffersErpSelectionsMatPickerErpSelectionIdGet**](docs/Api/OfferApi.md#apiv1offerserpselectionsmatpickererpselectionidget) | **GET** /api/v1/offers/erpSelections/matPicker/{erpSelectionId} | material picker for erp selection
*OfferApi* | [**apiV1OffersErpSelectionsMatPickerGet**](docs/Api/OfferApi.md#apiv1offerserpselectionsmatpickerget) | **GET** /api/v1/offers/erpSelections/matPicker | material picker
*OfferApi* | [**apiV1OffersErpSelectionsPost**](docs/Api/OfferApi.md#apiv1offerserpselectionspost) | **POST** /api/v1/offers/erpSelections | Creates a new erp selection
*OfferApi* | [**apiV1OffersPost**](docs/Api/OfferApi.md#apiv1offerspost) | **POST** /api/v1/offers | Creates an offer
*OfferApi* | [**apiV1OffersPut**](docs/Api/OfferApi.md#apiv1offersput) | **PUT** /api/v1/offers | Gets list of offers
*OfferApi* | [**apiV1OffersTemplatesGet**](docs/Api/OfferApi.md#apiv1offerstemplatesget) | **GET** /api/v1/offers/templates | Gets list of offer templates
*OfferApi* | [**apiV1OffersTemplatesPost**](docs/Api/OfferApi.md#apiv1offerstemplatespost) | **POST** /api/v1/offers/templates | Creates an offer template
*OfferApi* | [**apiV1OffersTemplatesTemplateIdDelete**](docs/Api/OfferApi.md#apiv1offerstemplatestemplateiddelete) | **DELETE** /api/v1/offers/templates/{templateId} | Deletes an offer template
*OfferApi* | [**apiV1OffersTemplatesTemplateIdGet**](docs/Api/OfferApi.md#apiv1offerstemplatestemplateidget) | **GET** /api/v1/offers/templates/{templateId} | Gets an offer templates
*OfferApi* | [**apiV1OffersTemplatesTemplateIdPut**](docs/Api/OfferApi.md#apiv1offerstemplatestemplateidput) | **PUT** /api/v1/offers/templates/{templateId} | Updates an offer templates
*OperatingSystemsApi* | [**apiV1OsGet**](docs/Api/OperatingSystemsApi.md#apiv1osget) | **GET** /api/v1/os | Get a list of all os
*OperatingSystemsApi* | [**apiV1OsIdDelete**](docs/Api/OperatingSystemsApi.md#apiv1osiddelete) | **DELETE** /api/v1/os/{id} | Deletes a specific os
*OperatingSystemsApi* | [**apiV1OsIdGet**](docs/Api/OperatingSystemsApi.md#apiv1osidget) | **GET** /api/v1/os/{id} | Get a specific os
*OperatingSystemsApi* | [**apiV1OsIdPut**](docs/Api/OperatingSystemsApi.md#apiv1osidput) | **PUT** /api/v1/os/{id} | Updates a os
*OperatingSystemsApi* | [**apiV1OsPost**](docs/Api/OperatingSystemsApi.md#apiv1ospost) | **POST** /api/v1/os | Creates a new os
*PcApi* | [**apiV1PcsPcIdDelete**](docs/Api/PcApi.md#apiv1pcspciddelete) | **DELETE** /api/v1/pcs/{pcId} | Deletes a pc
*PcApi* | [**apiV1PcsPcIdGet**](docs/Api/PcApi.md#apiv1pcspcidget) | **GET** /api/v1/pcs/{pcId} | Gets a pc by id
*PcApi* | [**apiV1PcsPcIdPut**](docs/Api/PcApi.md#apiv1pcspcidput) | **PUT** /api/v1/pcs/{pcId} | Updates a pc
*PcApi* | [**apiV1PcsPost**](docs/Api/PcApi.md#apiv1pcspost) | **POST** /api/v1/pcs | Creates a pc
*PcApi* | [**apiV1PcsPut**](docs/Api/PcApi.md#apiv1pcsput) | **PUT** /api/v1/pcs | Gets a list of pcs
*PeripheryApi* | [**apiV1PeripheriesPeripheryIdBuildInLinkTypeIdLinkIdDelete**](docs/Api/PeripheryApi.md#apiv1peripheriesperipheryidbuildinlinktypeidlinkiddelete) | **DELETE** /api/v1/peripheries/{peripheryId}/buildIn/{linkTypeId}/{linkId} | Delete periphery assignment
*PeripheryApi* | [**apiV1PeripheriesPeripheryIdBuildInLinkTypeIdLinkIdPost**](docs/Api/PeripheryApi.md#apiv1peripheriesperipheryidbuildinlinktypeidlinkidpost) | **POST** /api/v1/peripheries/{peripheryId}/buildIn/{linkTypeId}/{linkId} | Assign periphery
*PeripheryApi* | [**apiV1PeripheriesPeripheryIdDelete**](docs/Api/PeripheryApi.md#apiv1peripheriesperipheryiddelete) | **DELETE** /api/v1/peripheries/{peripheryId} | Deletes a periphery
*PeripheryApi* | [**apiV1PeripheriesPeripheryIdGet**](docs/Api/PeripheryApi.md#apiv1peripheriesperipheryidget) | **GET** /api/v1/peripheries/{peripheryId} | Gets a periphery by id
*PeripheryApi* | [**apiV1PeripheriesPeripheryIdPut**](docs/Api/PeripheryApi.md#apiv1peripheriesperipheryidput) | **PUT** /api/v1/peripheries/{peripheryId} | Updates a periphery
*PeripheryApi* | [**apiV1PeripheriesPost**](docs/Api/PeripheryApi.md#apiv1peripheriespost) | **POST** /api/v1/peripheries | Creates a periphery
*PeripheryApi* | [**apiV1PeripheriesPut**](docs/Api/PeripheryApi.md#apiv1peripheriesput) | **PUT** /api/v1/peripheries | Gets a list of peripheries
*PeripheryApi* | [**apiV1PeripheriesTypesGet**](docs/Api/PeripheryApi.md#apiv1peripheriestypesget) | **GET** /api/v1/peripheries/types | Get periphery types
*PeripheryApi* | [**apiV1PeripheriesTypesPost**](docs/Api/PeripheryApi.md#apiv1peripheriestypespost) | **POST** /api/v1/peripheries/types | Create periphery type
*PeripheryApi* | [**apiV1PeripheriesTypesTypeIdDelete**](docs/Api/PeripheryApi.md#apiv1peripheriestypestypeiddelete) | **DELETE** /api/v1/peripheries/types/{typeId} | Delete periphery type
*PeripheryApi* | [**apiV1PeripheriesTypesTypeIdPut**](docs/Api/PeripheryApi.md#apiv1peripheriestypestypeidput) | **PUT** /api/v1/peripheries/types/{typeId} | Update periphery type
*RemoteSupportsApi* | [**apiRemoteSupportsV1AssignDeviceDelete**](docs/Api/RemoteSupportsApi.md#apiremotesupportsv1assigndevicedelete) | **DELETE** /api/remoteSupports/v1/assignDevice | Deletes a device assignment
*RemoteSupportsApi* | [**apiRemoteSupportsV1AssignDeviceGet**](docs/Api/RemoteSupportsApi.md#apiremotesupportsv1assigndeviceget) | **GET** /api/remoteSupports/v1/assignDevice | Gets all device assignments
*RemoteSupportsApi* | [**apiRemoteSupportsV1AssignDevicePost**](docs/Api/RemoteSupportsApi.md#apiremotesupportsv1assigndevicepost) | **POST** /api/remoteSupports/v1/assignDevice | Creates a device assignment
*RemoteSupportsApi* | [**apiRemoteSupportsV1AssignEmployeeDelete**](docs/Api/RemoteSupportsApi.md#apiremotesupportsv1assignemployeedelete) | **DELETE** /api/remoteSupports/v1/assignEmployee | Delets a technician assignment
*RemoteSupportsApi* | [**apiRemoteSupportsV1AssignEmployeeGet**](docs/Api/RemoteSupportsApi.md#apiremotesupportsv1assignemployeeget) | **GET** /api/remoteSupports/v1/assignEmployee | Gets all technician assignments
*RemoteSupportsApi* | [**apiRemoteSupportsV1AssignEmployeePost**](docs/Api/RemoteSupportsApi.md#apiremotesupportsv1assignemployeepost) | **POST** /api/remoteSupports/v1/assignEmployee | Creates a technician assignment
*RemoteSupportsApi* | [**apiRemoteSupportsV1Post**](docs/Api/RemoteSupportsApi.md#apiremotesupportsv1post) | **POST** /api/remoteSupports/v1 | Creates/imports a remote support into the database
*RemoteSupportsApi* | [**apiRemoteSupportsV1Put**](docs/Api/RemoteSupportsApi.md#apiremotesupportsv1put) | **PUT** /api/remoteSupports/v1 | Get list of remote supports
*RemoteSupportsApi* | [**apiRemoteSupportsV1RemoteSupportIdDelete**](docs/Api/RemoteSupportsApi.md#apiremotesupportsv1remotesupportiddelete) | **DELETE** /api/remoteSupports/v1/{remoteSupportId} | Delete remote support
*RemoteSupportsApi* | [**apiRemoteSupportsV1RemoteSupportIdGet**](docs/Api/RemoteSupportsApi.md#apiremotesupportsv1remotesupportidget) | **GET** /api/remoteSupports/v1/{remoteSupportId} | Get remote support by id
*RemoteSupportsApi* | [**apiRemoteSupportsV1RemoteSupportIdPut**](docs/Api/RemoteSupportsApi.md#apiremotesupportsv1remotesupportidput) | **PUT** /api/remoteSupports/v1/{remoteSupportId} | Updates a remote support
*SearchApi* | [**apiV1SearchPut**](docs/Api/SearchApi.md#apiv1searchput) | **PUT** /api/v1/search | global search
*SecurityApi* | [**apiV1UserLoginGet**](docs/Api/SecurityApi.md#apiv1userloginget) | **GET** /api/v1/user/login | logs in the user and generates an api token for further authentication
*ServicesApi* | [**apiV1ServicesGet**](docs/Api/ServicesApi.md#apiv1servicesget) | **GET** /api/v1/services | Gets a list of all services
*ServicesApi* | [**apiV1ServicesIdDelete**](docs/Api/ServicesApi.md#apiv1servicesiddelete) | **DELETE** /api/v1/services/{id} | Deletes a service
*ServicesApi* | [**apiV1ServicesIdGet**](docs/Api/ServicesApi.md#apiv1servicesidget) | **GET** /api/v1/services/{id} | Gets a service by id
*ServicesApi* | [**apiV1ServicesIdPut**](docs/Api/ServicesApi.md#apiv1servicesidput) | **PUT** /api/v1/services/{id} | Updates a service
*ServicesApi* | [**apiV1ServicesPost**](docs/Api/ServicesApi.md#apiv1servicespost) | **POST** /api/v1/services | Creates a service
*SupportsApi* | [**apiV1SupportsListPut**](docs/Api/SupportsApi.md#apiv1supportslistput) | **PUT** /api/v1/supports/list | Get a support list
*TagsApi* | [**apiV1TagsAssignmentDelete**](docs/Api/TagsApi.md#apiv1tagsassignmentdelete) | **DELETE** /api/v1/tags/assignment | Removes a tag
*TagsApi* | [**apiV1TagsAssignmentGet**](docs/Api/TagsApi.md#apiv1tagsassignmentget) | **GET** /api/v1/tags/assignment | List of tags to an assignment
*TagsApi* | [**apiV1TagsAssignmentLogGet**](docs/Api/TagsApi.md#apiv1tagsassignmentlogget) | **GET** /api/v1/tags/assignment/log | List of tags logs to an assignment
*TagsApi* | [**apiV1TagsAssignmentPost**](docs/Api/TagsApi.md#apiv1tagsassignmentpost) | **POST** /api/v1/tags/assignment | Assigns a tag
*TagsApi* | [**apiV1TagsAssignmentPut**](docs/Api/TagsApi.md#apiv1tagsassignmentput) | **PUT** /api/v1/tags/assignment | Assigns multiple tags
*TagsApi* | [**apiV1TagsGet**](docs/Api/TagsApi.md#apiv1tagsget) | **GET** /api/v1/tags | Get all tags
*TagsApi* | [**apiV1TagsIdDelete**](docs/Api/TagsApi.md#apiv1tagsiddelete) | **DELETE** /api/v1/tags/{id} | Deletes a tag
*TagsApi* | [**apiV1TagsIdGet**](docs/Api/TagsApi.md#apiv1tagsidget) | **GET** /api/v1/tags/{id} | Gets a tag
*TagsApi* | [**apiV1TagsIdPut**](docs/Api/TagsApi.md#apiv1tagsidput) | **PUT** /api/v1/tags/{id} | Edits a tag
*TagsApi* | [**apiV1TagsPost**](docs/Api/TagsApi.md#apiv1tagspost) | **POST** /api/v1/tags | Creates a new tag
*TicketBoardApi* | [**apiV1TicketBoardGet**](docs/Api/TicketBoardApi.md#apiv1ticketboardget) | **GET** /api/v1/ticketBoard | Gets the ticket board with all panels
*TicketBoardApi* | [**apiV1TicketBoardPanelGet**](docs/Api/TicketBoardApi.md#apiv1ticketboardpanelget) | **GET** /api/v1/ticketBoard/panel | Gets an empty ticket board panel
*TicketBoardApi* | [**apiV1TicketBoardPanelIdDelete**](docs/Api/TicketBoardApi.md#apiv1ticketboardpaneliddelete) | **DELETE** /api/v1/ticketBoard/panel/{id} | Deletes a ticket board panel
*TicketBoardApi* | [**apiV1TicketBoardPanelIdGet**](docs/Api/TicketBoardApi.md#apiv1ticketboardpanelidget) | **GET** /api/v1/ticketBoard/panel/{id} | Gets a ticket board panel
*TicketBoardApi* | [**apiV1TicketBoardPanelIdRegistersGet**](docs/Api/TicketBoardApi.md#apiv1ticketboardpanelidregistersget) | **GET** /api/v1/ticketBoard/panel/{id}/registers | Gets all registers from a ticket board panel
*TicketBoardApi* | [**apiV1TicketBoardPanelPost**](docs/Api/TicketBoardApi.md#apiv1ticketboardpanelpost) | **POST** /api/v1/ticketBoard/panel | Creates a new ticket board panel
*TicketBoardApi* | [**apiV1TicketBoardPanelPut**](docs/Api/TicketBoardApi.md#apiv1ticketboardpanelput) | **PUT** /api/v1/ticketBoard/panel | Updates a ticket board panel
*TicketBoardApi* | [**apiV1TicketBoardProjectGlobalPanelsGet**](docs/Api/TicketBoardApi.md#apiv1ticketboardprojectglobalpanelsget) | **GET** /api/v1/ticketBoard/project/globalPanels | Get global ticket panels
*TicketBoardApi* | [**apiV1TicketBoardProjectIdGet**](docs/Api/TicketBoardApi.md#apiv1ticketboardprojectidget) | **GET** /api/v1/ticketBoard/project/{id} | Gets a ticket board from a project
*TicketBoardApi* | [**apiV1TicketBoardProjectIdRegistersGet**](docs/Api/TicketBoardApi.md#apiv1ticketboardprojectidregistersget) | **GET** /api/v1/ticketBoard/project/{id}/registers | Gets all registers from a ticket board project
*TicketContentApi* | [**apiV1TicketsTicketIdDocumentsDocumentIdGet**](docs/Api/TicketContentApi.md#apiv1ticketsticketiddocumentsdocumentidget) | **GET** /api/v1/tickets/{ticketId}/documents/{documentId} | Gets a ticket document
*TicketContentApi* | [**apiV1TicketsTicketIdDocumentsGet**](docs/Api/TicketContentApi.md#apiv1ticketsticketiddocumentsget) | **GET** /api/v1/tickets/{ticketId}/documents | Gets all ticket documents
*TicketContentApi* | [**apiV1TicketsTicketIdScreenshotsGet**](docs/Api/TicketContentApi.md#apiv1ticketsticketidscreenshotsget) | **GET** /api/v1/tickets/{ticketId}/screenshots | Gets all ticket images
*TicketContentApi* | [**apiV1TicketsTicketIdScreenshotsImageIdGet**](docs/Api/TicketContentApi.md#apiv1ticketsticketidscreenshotsimageidget) | **GET** /api/v1/tickets/{ticketId}/screenshots/{imageId} | Gets a ticket image
*TicketContentApi* | [**apiV1TicketsTicketIdUploadPost**](docs/Api/TicketContentApi.md#apiv1ticketsticketiduploadpost) | **POST** /api/v1/tickets/{ticketId}/upload | upload a document/image
*TicketListsApi* | [**apiV1TicketsCompanyCompanyIdGet**](docs/Api/TicketListsApi.md#apiv1ticketscompanycompanyidget) | **GET** /api/v1/tickets/company/{companyId} | gets a list of company tickets
*TicketListsApi* | [**apiV1TicketsGeneralGet**](docs/Api/TicketListsApi.md#apiv1ticketsgeneralget) | **GET** /api/v1/tickets/general | gets a list of general tickets (assigned to no employee)
*TicketListsApi* | [**apiV1TicketsLocalAdminOverviewGet**](docs/Api/TicketListsApi.md#apiv1ticketslocaladminoverviewget) | **GET** /api/v1/tickets/localAdminOverview | gets a list of all tickets which are assigned to local ticket admins
*TicketListsApi* | [**apiV1TicketsNotIdentifiedGet**](docs/Api/TicketListsApi.md#apiv1ticketsnotidentifiedget) | **GET** /api/v1/tickets/notIdentified | gets a list of not identified tickets
*TicketListsApi* | [**apiV1TicketsOwnGet**](docs/Api/TicketListsApi.md#apiv1ticketsownget) | **GET** /api/v1/tickets/own | gets a list of own tickets (assigned to currently logged in employee)
*TicketListsApi* | [**apiV1TicketsProjectsGet**](docs/Api/TicketListsApi.md#apiv1ticketsprojectsget) | **GET** /api/v1/tickets/projects | gets a list of all projects
*TicketListsApi* | [**apiV1TicketsPut**](docs/Api/TicketListsApi.md#apiv1ticketsput) | **PUT** /api/v1/tickets | Get a (custom) ticket list
*TicketListsApi* | [**apiV1TicketsRepairGet**](docs/Api/TicketListsApi.md#apiv1ticketsrepairget) | **GET** /api/v1/tickets/repair | gets a list of repair tickets
*TicketListsApi* | [**apiV1TicketsTechnicianGet**](docs/Api/TicketListsApi.md#apiv1ticketstechnicianget) | **GET** /api/v1/tickets/technician | gets a list of tickets of all technicians
*TicketListsApi* | [**apiV1TicketsWithRoleGet**](docs/Api/TicketListsApi.md#apiv1ticketswithroleget) | **GET** /api/v1/tickets/withRole | gets a list of all ticket which a technician has a role in
*TicketsApi* | [**apiV1TicketsHistoryTicketIdGet**](docs/Api/TicketsApi.md#apiv1ticketshistoryticketidget) | **GET** /api/v1/tickets/history/{ticketId} | Gets a ticket history
*TicketsApi* | [**apiV1TicketsPost**](docs/Api/TicketsApi.md#apiv1ticketspost) | **POST** /api/v1/tickets | Creates a ticket in the database
*TicketsApi* | [**apiV1TicketsTicketIdCommentsPost**](docs/Api/TicketsApi.md#apiv1ticketsticketidcommentspost) | **POST** /api/v1/tickets/{ticketId}/comments | Creates a comment
*TicketsApi* | [**apiV1TicketsTicketIdDelete**](docs/Api/TicketsApi.md#apiv1ticketsticketiddelete) | **DELETE** /api/v1/tickets/{ticketId} | Deletes a ticket by id
*TicketsApi* | [**apiV1TicketsTicketIdGet**](docs/Api/TicketsApi.md#apiv1ticketsticketidget) | **GET** /api/v1/tickets/{ticketId} | Gets a ticket by id
*TicketsApi* | [**apiV1TicketsTicketIdPut**](docs/Api/TicketsApi.md#apiv1ticketsticketidput) | **PUT** /api/v1/tickets/{ticketId} | Updates a ticket
*TimersApi* | [**apiV1TimersDelete**](docs/Api/TimersApi.md#apiv1timersdelete) | **DELETE** /api/v1/timers | Deletes a timer
*TimersApi* | [**apiV1TimersGet**](docs/Api/TimersApi.md#apiv1timersget) | **GET** /api/v1/timers | Get all timers of current user
*TimersApi* | [**apiV1TimersNotesDelete**](docs/Api/TimersApi.md#apiv1timersnotesdelete) | **DELETE** /api/v1/timers/notes | Deletes a timer fragment
*TimersApi* | [**apiV1TimersNotesPut**](docs/Api/TimersApi.md#apiv1timersnotesput) | **PUT** /api/v1/timers/notes | Updates a timer fragment
*TimersApi* | [**apiV1TimersNotesTimerIdGet**](docs/Api/TimersApi.md#apiv1timersnotestimeridget) | **GET** /api/v1/timers/notes/{timerId} | Get all timer fragments
*TimersApi* | [**apiV1TimersPost**](docs/Api/TimersApi.md#apiv1timerspost) | **POST** /api/v1/timers | Creates a timer
*TimersApi* | [**apiV1TimersTimerIdGet**](docs/Api/TimersApi.md#apiv1timerstimeridget) | **GET** /api/v1/timers/{timerId} | Get a specific timer
*TimersApi* | [**apiV1TimersTimerIdPut**](docs/Api/TimersApi.md#apiv1timerstimeridput) | **PUT** /api/v1/timers/{timerId} | Starts/stops timer
*TimestampApi* | [**apiV1TimestampsDayClosingDelete**](docs/Api/TimestampApi.md#apiv1timestampsdayclosingdelete) | **DELETE** /api/v1/timestamps/dayClosing | remove / undo one or more \&quot;day closings\&quot; for the timestamp module
*TimestampApi* | [**apiV1TimestampsDayClosingPost**](docs/Api/TimestampApi.md#apiv1timestampsdayclosingpost) | **POST** /api/v1/timestamps/dayClosing | does one or more \&quot;day closings\&quot; for the timestamp module
*TimestampApi* | [**apiV1TimestampsDayClosingTillDateGet**](docs/Api/TimestampApi.md#apiv1timestampsdayclosingtilldateget) | **GET** /api/v1/timestamps/dayClosing/tillDate | gets all infos about last dayclosings for employees
*TimestampApi* | [**apiV1TimestampsDayClosingTillDatePost**](docs/Api/TimestampApi.md#apiv1timestampsdayclosingtilldatepost) | **POST** /api/v1/timestamps/dayClosing/tillDate | created dayClosings to a given date
*TimestampApi* | [**apiV1TimestampsEmployeeEmployeeIdInitialBalancePost**](docs/Api/TimestampApi.md#apiv1timestampsemployeeemployeeidinitialbalancepost) | **POST** /api/v1/timestamps/employee/{employeeId}/initialBalance | sets the initial balance for this employee
*TimestampApi* | [**apiV1TimestampsEmployeeIdDayDayPut**](docs/Api/TimestampApi.md#apiv1timestampsemployeeiddaydayput) | **PUT** /api/v1/timestamps/{employeeId}/day/{day} | writes the timstamps of a whole day into the database at once
*TimestampApi* | [**apiV1TimestampsGet**](docs/Api/TimestampApi.md#apiv1timestampsget) | **GET** /api/v1/timestamps | gets a list of timestamps from a given period
*TimestampApi* | [**apiV1TimestampsInfoGet**](docs/Api/TimestampApi.md#apiv1timestampsinfoget) | **GET** /api/v1/timestamps/info | gets the timestamp infos for a given time period
*TimestampApi* | [**apiV1TimestampsPauseConfigsGet**](docs/Api/TimestampApi.md#apiv1timestampspauseconfigsget) | **GET** /api/v1/timestamps/pauseConfigs | gets a list of all pause configs
*TimestampApi* | [**apiV1TimestampsPauseConfigsIdDelete**](docs/Api/TimestampApi.md#apiv1timestampspauseconfigsiddelete) | **DELETE** /api/v1/timestamps/pauseConfigs/{id} | deletes a pause config
*TimestampApi* | [**apiV1TimestampsPauseConfigsIdPut**](docs/Api/TimestampApi.md#apiv1timestampspauseconfigsidput) | **PUT** /api/v1/timestamps/pauseConfigs/{id} | updates a pause config
*TimestampApi* | [**apiV1TimestampsPauseConfigsPost**](docs/Api/TimestampApi.md#apiv1timestampspauseconfigspost) | **POST** /api/v1/timestamps/pauseConfigs | creates a pause config
*TimestampApi* | [**apiV1TimestampsPost**](docs/Api/TimestampApi.md#apiv1timestampspost) | **POST** /api/v1/timestamps | writes a timestamp into the database
*TimestampApi* | [**apiV1TimestampsStatisticsGet**](docs/Api/TimestampApi.md#apiv1timestampsstatisticsget) | **GET** /api/v1/timestamps/statistics | gets the timestamp infos for a given time period (with statistical values)
*TimestampApi* | [**apiV1TimestampsTimestampIdPut**](docs/Api/TimestampApi.md#apiv1timestampstimestampidput) | **PUT** /api/v1/timestamps/{timestampId} | edits a single timestamp
*WebHooksApi* | [**apiV1TanssEventsRulesIdDelete**](docs/Api/WebHooksApi.md#apiv1tansseventsrulesiddelete) | **DELETE** /api/v1/tanssEvents/rules/{id} | deletes a rule
*WebHooksApi* | [**apiV1TanssEventsRulesIdGet**](docs/Api/WebHooksApi.md#apiv1tansseventsrulesidget) | **GET** /api/v1/tanssEvents/rules/{id} | gets a rule
*WebHooksApi* | [**apiV1TanssEventsRulesIdPut**](docs/Api/WebHooksApi.md#apiv1tansseventsrulesidput) | **PUT** /api/v1/tanssEvents/rules/{id} | updates a rule
*WebHooksApi* | [**apiV1TanssEventsRulesPost**](docs/Api/WebHooksApi.md#apiv1tansseventsrulespost) | **POST** /api/v1/tanssEvents/rules | creates a rule
*WebHooksApi* | [**apiV1TanssEventsRulesPut**](docs/Api/WebHooksApi.md#apiv1tansseventsrulesput) | **PUT** /api/v1/tanssEvents/rules | get a list of rules
*WebHooksApi* | [**apiV1TanssEventsRulesTestActionPut**](docs/Api/WebHooksApi.md#apiv1tansseventsrulestestactionput) | **PUT** /api/v1/tanssEvents/rules/test/action | test a rule

## Models

- [ApiCallsV1FromPhoneNrInfos](docs/Model/ApiCallsV1FromPhoneNrInfos.md)
- [ApiCallsV1FromPhoneNrInfosItems](docs/Model/ApiCallsV1FromPhoneNrInfosItems.md)
- [ApiCallsV1PhoneParticipants](docs/Model/ApiCallsV1PhoneParticipants.md)
- [ApiV1IdentifyItems](docs/Model/ApiV1IdentifyItems.md)
- [ApiV1SearchConfigs](docs/Model/ApiV1SearchConfigs.md)
- [ApiV1SearchConfigsCompanyAllOf](docs/Model/ApiV1SearchConfigsCompanyAllOf.md)
- [ApiV1SearchConfigsEmployeeAllOf](docs/Model/ApiV1SearchConfigsEmployeeAllOf.md)
- [ApiV1SearchConfigsTicketAllOf](docs/Model/ApiV1SearchConfigsTicketAllOf.md)
- [ApiV1TagsGroupTag](docs/Model/ApiV1TagsGroupTag.md)
- [ApiV1TagsVisibilities](docs/Model/ApiV1TagsVisibilities.md)
- [ApiV1TanssEventsRulesActions](docs/Model/ApiV1TanssEventsRulesActions.md)
- [ApiV1TanssEventsRulesAssignments](docs/Model/ApiV1TanssEventsRulesAssignments.md)
- [ApiV1TanssEventsRulesParams](docs/Model/ApiV1TanssEventsRulesParams.md)
- [ApiV1TanssEventsRulesTestActionTicketNotificationOptions](docs/Model/ApiV1TanssEventsRulesTestActionTicketNotificationOptions.md)
- [InlineObject](docs/Model/InlineObject.md)
- [InlineObject10](docs/Model/InlineObject10.md)
- [InlineObject11](docs/Model/InlineObject11.md)
- [InlineObject12](docs/Model/InlineObject12.md)
- [InlineObject13](docs/Model/InlineObject13.md)
- [InlineObject14](docs/Model/InlineObject14.md)
- [InlineObject15](docs/Model/InlineObject15.md)
- [InlineObject16](docs/Model/InlineObject16.md)
- [InlineObject17](docs/Model/InlineObject17.md)
- [InlineObject18](docs/Model/InlineObject18.md)
- [InlineObject19](docs/Model/InlineObject19.md)
- [InlineObject2](docs/Model/InlineObject2.md)
- [InlineObject20](docs/Model/InlineObject20.md)
- [InlineObject21](docs/Model/InlineObject21.md)
- [InlineObject22](docs/Model/InlineObject22.md)
- [InlineObject23](docs/Model/InlineObject23.md)
- [InlineObject24](docs/Model/InlineObject24.md)
- [InlineObject25](docs/Model/InlineObject25.md)
- [InlineObject26](docs/Model/InlineObject26.md)
- [InlineObject27](docs/Model/InlineObject27.md)
- [InlineObject28](docs/Model/InlineObject28.md)
- [InlineObject29](docs/Model/InlineObject29.md)
- [InlineObject2Timeframe](docs/Model/InlineObject2Timeframe.md)
- [InlineObject3](docs/Model/InlineObject3.md)
- [InlineObject30](docs/Model/InlineObject30.md)
- [InlineObject31](docs/Model/InlineObject31.md)
- [InlineObject32](docs/Model/InlineObject32.md)
- [InlineObject33](docs/Model/InlineObject33.md)
- [InlineObject34](docs/Model/InlineObject34.md)
- [InlineObject35](docs/Model/InlineObject35.md)
- [InlineObject36](docs/Model/InlineObject36.md)
- [InlineObject4](docs/Model/InlineObject4.md)
- [InlineObject5](docs/Model/InlineObject5.md)
- [InlineObject6](docs/Model/InlineObject6.md)
- [InlineObject7](docs/Model/InlineObject7.md)
- [InlineObject8](docs/Model/InlineObject8.md)
- [InlineObject9](docs/Model/InlineObject9.md)
- [InlineResponse200](docs/Model/InlineResponse200.md)
- [InlineResponse2001](docs/Model/InlineResponse2001.md)
- [InlineResponse200101](docs/Model/InlineResponse200101.md)
- [InlineResponse200101Content](docs/Model/InlineResponse200101Content.md)
- [InlineResponse200101ContentItemsItemsAllOf](docs/Model/InlineResponse200101ContentItemsItemsAllOf.md)
- [InlineResponse200101ContentItemsItemsAllOf1](docs/Model/InlineResponse200101ContentItemsItemsAllOf1.md)
- [InlineResponse200101ContentItemsItemsAllOfResultsInner](docs/Model/InlineResponse200101ContentItemsItemsAllOfResultsInner.md)
- [InlineResponse20011](docs/Model/InlineResponse20011.md)
- [InlineResponse20011Content](docs/Model/InlineResponse20011Content.md)
- [InlineResponse20013](docs/Model/InlineResponse20013.md)
- [InlineResponse20016](docs/Model/InlineResponse20016.md)
- [InlineResponse20016Content](docs/Model/InlineResponse20016Content.md)
- [InlineResponse20017](docs/Model/InlineResponse20017.md)
- [InlineResponse20017Content](docs/Model/InlineResponse20017Content.md)
- [InlineResponse20018](docs/Model/InlineResponse20018.md)
- [InlineResponse20019](docs/Model/InlineResponse20019.md)
- [InlineResponse2001ContentAllOf1](docs/Model/InlineResponse2001ContentAllOf1.md)
- [InlineResponse2001ContentAllOfChatsInner](docs/Model/InlineResponse2001ContentAllOfChatsInner.md)
- [InlineResponse2001ContentAllOfNextSupport](docs/Model/InlineResponse2001ContentAllOfNextSupport.md)
- [InlineResponse2002](docs/Model/InlineResponse2002.md)
- [InlineResponse20022](docs/Model/InlineResponse20022.md)
- [InlineResponse20023](docs/Model/InlineResponse20023.md)
- [InlineResponse20024](docs/Model/InlineResponse20024.md)
- [InlineResponse20024AssignedToCustomers](docs/Model/InlineResponse20024AssignedToCustomers.md)
- [InlineResponse20024Categories](docs/Model/InlineResponse20024Categories.md)
- [InlineResponse20024Customers](docs/Model/InlineResponse20024Customers.md)
- [InlineResponse20024Employees](docs/Model/InlineResponse20024Employees.md)
- [InlineResponse20024PreferredCustomer](docs/Model/InlineResponse20024PreferredCustomer.md)
- [InlineResponse20024Section](docs/Model/InlineResponse20024Section.md)
- [InlineResponse20025](docs/Model/InlineResponse20025.md)
- [InlineResponse20026](docs/Model/InlineResponse20026.md)
- [InlineResponse20026Meta](docs/Model/InlineResponse20026Meta.md)
- [InlineResponse20027](docs/Model/InlineResponse20027.md)
- [InlineResponse20027Content](docs/Model/InlineResponse20027Content.md)
- [InlineResponse20027Meta](docs/Model/InlineResponse20027Meta.md)
- [InlineResponse20028](docs/Model/InlineResponse20028.md)
- [InlineResponse20028Content](docs/Model/InlineResponse20028Content.md)
- [InlineResponse20029](docs/Model/InlineResponse20029.md)
- [InlineResponse20029Content](docs/Model/InlineResponse20029Content.md)
- [InlineResponse2002Content](docs/Model/InlineResponse2002Content.md)
- [InlineResponse2002ContentMails](docs/Model/InlineResponse2002ContentMails.md)
- [InlineResponse2002ContentReceivers](docs/Model/InlineResponse2002ContentReceivers.md)
- [InlineResponse2003](docs/Model/InlineResponse2003.md)
- [InlineResponse20030](docs/Model/InlineResponse20030.md)
- [InlineResponse20030ContentAllOf1](docs/Model/InlineResponse20030ContentAllOf1.md)
- [InlineResponse20031](docs/Model/InlineResponse20031.md)
- [InlineResponse20031ContentItemsAllOf1](docs/Model/InlineResponse20031ContentItemsAllOf1.md)
- [InlineResponse20032](docs/Model/InlineResponse20032.md)
- [InlineResponse20033](docs/Model/InlineResponse20033.md)
- [InlineResponse20033Content](docs/Model/InlineResponse20033Content.md)
- [InlineResponse20034](docs/Model/InlineResponse20034.md)
- [InlineResponse20034Content](docs/Model/InlineResponse20034Content.md)
- [InlineResponse20035](docs/Model/InlineResponse20035.md)
- [InlineResponse20035Content](docs/Model/InlineResponse20035Content.md)
- [InlineResponse20036](docs/Model/InlineResponse20036.md)
- [InlineResponse20036Content](docs/Model/InlineResponse20036Content.md)
- [InlineResponse20038](docs/Model/InlineResponse20038.md)
- [InlineResponse20039](docs/Model/InlineResponse20039.md)
- [InlineResponse20039ContentItemsAllOf](docs/Model/InlineResponse20039ContentItemsAllOf.md)
- [InlineResponse20039ContentItemsAllOf1](docs/Model/InlineResponse20039ContentItemsAllOf1.md)
- [InlineResponse20039ContentItemsAllOfTypesAddlProps](docs/Model/InlineResponse20039ContentItemsAllOfTypesAddlProps.md)
- [InlineResponse2003Content](docs/Model/InlineResponse2003Content.md)
- [InlineResponse2004](docs/Model/InlineResponse2004.md)
- [InlineResponse20040](docs/Model/InlineResponse20040.md)
- [InlineResponse20040ContentItemsAllOf](docs/Model/InlineResponse20040ContentItemsAllOf.md)
- [InlineResponse20040ContentItemsAllOf1](docs/Model/InlineResponse20040ContentItemsAllOf1.md)
- [InlineResponse20040ContentItemsAllOfChangesRequestedInner](docs/Model/InlineResponse20040ContentItemsAllOfChangesRequestedInner.md)
- [InlineResponse20040ContentItemsAllOfHistoryInner](docs/Model/InlineResponse20040ContentItemsAllOfHistoryInner.md)
- [InlineResponse20042](docs/Model/InlineResponse20042.md)
- [InlineResponse20042Content](docs/Model/InlineResponse20042Content.md)
- [InlineResponse20043](docs/Model/InlineResponse20043.md)
- [InlineResponse20045](docs/Model/InlineResponse20045.md)
- [InlineResponse20045Content](docs/Model/InlineResponse20045Content.md)
- [InlineResponse20045ContentCloseRequests](docs/Model/InlineResponse20045ContentCloseRequests.md)
- [InlineResponse20047](docs/Model/InlineResponse20047.md)
- [InlineResponse20047Content](docs/Model/InlineResponse20047Content.md)
- [InlineResponse20048](docs/Model/InlineResponse20048.md)
- [InlineResponse20048ContentItemsAllOf](docs/Model/InlineResponse20048ContentItemsAllOf.md)
- [InlineResponse20048ContentItemsAllOfAllOf1](docs/Model/InlineResponse20048ContentItemsAllOfAllOf1.md)
- [InlineResponse20048ContentItemsAllOfAllOfErpSelectionsInner](docs/Model/InlineResponse20048ContentItemsAllOfAllOfErpSelectionsInner.md)
- [InlineResponse20048ContentItemsAllOfAllOfErpSelectionsItemsMatsItemsAllOf](docs/Model/InlineResponse20048ContentItemsAllOfAllOfErpSelectionsItemsMatsItemsAllOf.md)
- [InlineResponse20048ContentItemsAllOfAllOfErpSelectionsItemsMatsItemsAllOf1](docs/Model/InlineResponse20048ContentItemsAllOfAllOfErpSelectionsItemsMatsItemsAllOf1.md)
- [InlineResponse20048ContentItemsAllOfVarsInner](docs/Model/InlineResponse20048ContentItemsAllOfVarsInner.md)
- [InlineResponse20049](docs/Model/InlineResponse20049.md)
- [InlineResponse20049Content](docs/Model/InlineResponse20049Content.md)
- [InlineResponse2004Content](docs/Model/InlineResponse2004Content.md)
- [InlineResponse2005](docs/Model/InlineResponse2005.md)
- [InlineResponse20050](docs/Model/InlineResponse20050.md)
- [InlineResponse20050ContentItemsAllOf1](docs/Model/InlineResponse20050ContentItemsAllOf1.md)
- [InlineResponse20051](docs/Model/InlineResponse20051.md)
- [InlineResponse20051ContentItemsAllOf](docs/Model/InlineResponse20051ContentItemsAllOf.md)
- [InlineResponse20051ContentItemsAllOf1](docs/Model/InlineResponse20051ContentItemsAllOf1.md)
- [InlineResponse20051ContentItemsAllOfWorkflowsItemsAllOf](docs/Model/InlineResponse20051ContentItemsAllOfWorkflowsItemsAllOf.md)
- [InlineResponse20051ContentItemsAllOfWorkflowsItemsAllOf1](docs/Model/InlineResponse20051ContentItemsAllOfWorkflowsItemsAllOf1.md)
- [InlineResponse20051ContentItemsAllOfWorkflowsItemsAllOfTokensInner](docs/Model/InlineResponse20051ContentItemsAllOfWorkflowsItemsAllOfTokensInner.md)
- [InlineResponse20053](docs/Model/InlineResponse20053.md)
- [InlineResponse20053Absences](docs/Model/InlineResponse20053Absences.md)
- [InlineResponse20053Availability](docs/Model/InlineResponse20053Availability.md)
- [InlineResponse20053Content](docs/Model/InlineResponse20053Content.md)
- [InlineResponse20053EndInfos](docs/Model/InlineResponse20053EndInfos.md)
- [InlineResponse20055](docs/Model/InlineResponse20055.md)
- [InlineResponse20058](docs/Model/InlineResponse20058.md)
- [InlineResponse20058Content](docs/Model/InlineResponse20058Content.md)
- [InlineResponse20059](docs/Model/InlineResponse20059.md)
- [InlineResponse20059ContentItemsAllOf1](docs/Model/InlineResponse20059ContentItemsAllOf1.md)
- [InlineResponse20059ContentItemsAllOfStateLogInner](docs/Model/InlineResponse20059ContentItemsAllOfStateLogInner.md)
- [InlineResponse2005Content](docs/Model/InlineResponse2005Content.md)
- [InlineResponse2006](docs/Model/InlineResponse2006.md)
- [InlineResponse20060](docs/Model/InlineResponse20060.md)
- [InlineResponse20061](docs/Model/InlineResponse20061.md)
- [InlineResponse20061Content](docs/Model/InlineResponse20061Content.md)
- [InlineResponse20061ContentAnticipatedCallbacks](docs/Model/InlineResponse20061ContentAnticipatedCallbacks.md)
- [InlineResponse20061ContentCompanies](docs/Model/InlineResponse20061ContentCompanies.md)
- [InlineResponse20061ContentEmployees](docs/Model/InlineResponse20061ContentEmployees.md)
- [InlineResponse20061ContentTickets](docs/Model/InlineResponse20061ContentTickets.md)
- [InlineResponse20062](docs/Model/InlineResponse20062.md)
- [InlineResponse20062ContentAllOf1](docs/Model/InlineResponse20062ContentAllOf1.md)
- [InlineResponse20062ContentAllOfAssignmentInfos](docs/Model/InlineResponse20062ContentAllOfAssignmentInfos.md)
- [InlineResponse20063](docs/Model/InlineResponse20063.md)
- [InlineResponse20064](docs/Model/InlineResponse20064.md)
- [InlineResponse20065](docs/Model/InlineResponse20065.md)
- [InlineResponse20065ContentAllOf](docs/Model/InlineResponse20065ContentAllOf.md)
- [InlineResponse20065ContentAllOf1](docs/Model/InlineResponse20065ContentAllOf1.md)
- [InlineResponse20065ContentAllOfItemsInner](docs/Model/InlineResponse20065ContentAllOfItemsInner.md)
- [InlineResponse20065ContentAllOfItemsItemsAction](docs/Model/InlineResponse20065ContentAllOfItemsItemsAction.md)
- [InlineResponse20066](docs/Model/InlineResponse20066.md)
- [InlineResponse20067](docs/Model/InlineResponse20067.md)
- [InlineResponse20067ContentItemsAllOf1](docs/Model/InlineResponse20067ContentItemsAllOf1.md)
- [InlineResponse20068](docs/Model/InlineResponse20068.md)
- [InlineResponse20068Content](docs/Model/InlineResponse20068Content.md)
- [InlineResponse20068ContentCompanies](docs/Model/InlineResponse20068ContentCompanies.md)
- [InlineResponse20068ContentDepartments](docs/Model/InlineResponse20068ContentDepartments.md)
- [InlineResponse20068ContentEmployees](docs/Model/InlineResponse20068ContentEmployees.md)
- [InlineResponse20068ContentPanels](docs/Model/InlineResponse20068ContentPanels.md)
- [InlineResponse20068ContentTags](docs/Model/InlineResponse20068ContentTags.md)
- [InlineResponse20068ContentTicketStatus](docs/Model/InlineResponse20068ContentTicketStatus.md)
- [InlineResponse20068ContentTicketTypes](docs/Model/InlineResponse20068ContentTicketTypes.md)
- [InlineResponse20069](docs/Model/InlineResponse20069.md)
- [InlineResponse2007](docs/Model/InlineResponse2007.md)
- [InlineResponse20071](docs/Model/InlineResponse20071.md)
- [InlineResponse20071Content](docs/Model/InlineResponse20071Content.md)
- [InlineResponse20071ContentRegisters](docs/Model/InlineResponse20071ContentRegisters.md)
- [InlineResponse20071ContentTickets](docs/Model/InlineResponse20071ContentTickets.md)
- [InlineResponse20072](docs/Model/InlineResponse20072.md)
- [InlineResponse20073](docs/Model/InlineResponse20073.md)
- [InlineResponse20074](docs/Model/InlineResponse20074.md)
- [InlineResponse20075](docs/Model/InlineResponse20075.md)
- [InlineResponse20075Content](docs/Model/InlineResponse20075Content.md)
- [InlineResponse20078](docs/Model/InlineResponse20078.md)
- [InlineResponse20078Content](docs/Model/InlineResponse20078Content.md)
- [InlineResponse20079](docs/Model/InlineResponse20079.md)
- [InlineResponse20079ContentAllOf](docs/Model/InlineResponse20079ContentAllOf.md)
- [InlineResponse20079ContentAllOfAllOf](docs/Model/InlineResponse20079ContentAllOfAllOf.md)
- [InlineResponse20079ContentAllOfAllOf1](docs/Model/InlineResponse20079ContentAllOfAllOf1.md)
- [InlineResponse20079ContentAllOfAllOfGuarantee](docs/Model/InlineResponse20079ContentAllOfAllOfGuarantee.md)
- [InlineResponse20079ContentAllOfAllOfIpsInner](docs/Model/InlineResponse20079ContentAllOfAllOfIpsInner.md)
- [InlineResponse20079ContentAllOfAllOfIpsItemsServiceAssignmentsInner](docs/Model/InlineResponse20079ContentAllOfAllOfIpsItemsServiceAssignmentsInner.md)
- [InlineResponse20079ContentAllOfDevicesInner](docs/Model/InlineResponse20079ContentAllOfDevicesInner.md)
- [InlineResponse20079ContentAllOfDevicesItemsFieldsInner](docs/Model/InlineResponse20079ContentAllOfDevicesItemsFieldsInner.md)
- [InlineResponse20079ContentAllOfServiceIconsInner](docs/Model/InlineResponse20079ContentAllOfServiceIconsInner.md)
- [InlineResponse20079ContentAllOfSoftwarelicensesInner](docs/Model/InlineResponse20079ContentAllOfSoftwarelicensesInner.md)
- [InlineResponse2007Content](docs/Model/InlineResponse2007Content.md)
- [InlineResponse2008](docs/Model/InlineResponse2008.md)
- [InlineResponse20080](docs/Model/InlineResponse20080.md)
- [InlineResponse20081](docs/Model/InlineResponse20081.md)
- [InlineResponse20081ContentAllOf](docs/Model/InlineResponse20081ContentAllOf.md)
- [InlineResponse20081ContentAllOfAllOf](docs/Model/InlineResponse20081ContentAllOfAllOf.md)
- [InlineResponse20081ContentAllOfAllOf1](docs/Model/InlineResponse20081ContentAllOfAllOf1.md)
- [InlineResponse20082](docs/Model/InlineResponse20082.md)
- [InlineResponse20083](docs/Model/InlineResponse20083.md)
- [InlineResponse20083Content](docs/Model/InlineResponse20083Content.md)
- [InlineResponse20084](docs/Model/InlineResponse20084.md)
- [InlineResponse20084Content](docs/Model/InlineResponse20084Content.md)
- [InlineResponse20085](docs/Model/InlineResponse20085.md)
- [InlineResponse20086](docs/Model/InlineResponse20086.md)
- [InlineResponse20086Content](docs/Model/InlineResponse20086Content.md)
- [InlineResponse20087](docs/Model/InlineResponse20087.md)
- [InlineResponse20089](docs/Model/InlineResponse20089.md)
- [InlineResponse20091](docs/Model/InlineResponse20091.md)
- [InlineResponse20093](docs/Model/InlineResponse20093.md)
- [InlineResponse20095](docs/Model/InlineResponse20095.md)
- [InlineResponse20097](docs/Model/InlineResponse20097.md)
- [InlineResponse20099](docs/Model/InlineResponse20099.md)
- [InlineResponse20099Content](docs/Model/InlineResponse20099Content.md)
- [InlineResponse201](docs/Model/InlineResponse201.md)
- [InlineResponse2011](docs/Model/InlineResponse2011.md)
- [InlineResponse20110](docs/Model/InlineResponse20110.md)
- [InlineResponse20110Content](docs/Model/InlineResponse20110Content.md)
- [InlineResponse20111](docs/Model/InlineResponse20111.md)
- [InlineResponse20111Content](docs/Model/InlineResponse20111Content.md)
- [InlineResponse20111ContentErrors](docs/Model/InlineResponse20111ContentErrors.md)
- [InlineResponse20112](docs/Model/InlineResponse20112.md)
- [InlineResponse20113](docs/Model/InlineResponse20113.md)
- [InlineResponse20114](docs/Model/InlineResponse20114.md)
- [InlineResponse20115](docs/Model/InlineResponse20115.md)
- [InlineResponse20115ContentAllOf1](docs/Model/InlineResponse20115ContentAllOf1.md)
- [InlineResponse20116](docs/Model/InlineResponse20116.md)
- [InlineResponse20117](docs/Model/InlineResponse20117.md)
- [InlineResponse20117Content](docs/Model/InlineResponse20117Content.md)
- [InlineResponse20118](docs/Model/InlineResponse20118.md)
- [InlineResponse20118Content](docs/Model/InlineResponse20118Content.md)
- [InlineResponse20119](docs/Model/InlineResponse20119.md)
- [InlineResponse20119Content](docs/Model/InlineResponse20119Content.md)
- [InlineResponse2012](docs/Model/InlineResponse2012.md)
- [InlineResponse20120](docs/Model/InlineResponse20120.md)
- [InlineResponse20121](docs/Model/InlineResponse20121.md)
- [InlineResponse20122](docs/Model/InlineResponse20122.md)
- [InlineResponse20123](docs/Model/InlineResponse20123.md)
- [InlineResponse20124](docs/Model/InlineResponse20124.md)
- [InlineResponse20125](docs/Model/InlineResponse20125.md)
- [InlineResponse20127](docs/Model/InlineResponse20127.md)
- [InlineResponse20128](docs/Model/InlineResponse20128.md)
- [InlineResponse2013](docs/Model/InlineResponse2013.md)
- [InlineResponse20131](docs/Model/InlineResponse20131.md)
- [InlineResponse20132](docs/Model/InlineResponse20132.md)
- [InlineResponse20134](docs/Model/InlineResponse20134.md)
- [InlineResponse20136](docs/Model/InlineResponse20136.md)
- [InlineResponse20139](docs/Model/InlineResponse20139.md)
- [InlineResponse2014](docs/Model/InlineResponse2014.md)
- [InlineResponse20141](docs/Model/InlineResponse20141.md)
- [InlineResponse20143](docs/Model/InlineResponse20143.md)
- [InlineResponse20144](docs/Model/InlineResponse20144.md)
- [InlineResponse20146](docs/Model/InlineResponse20146.md)
- [InlineResponse20146Content](docs/Model/InlineResponse20146Content.md)
- [InlineResponse20146ContentContent](docs/Model/InlineResponse20146ContentContent.md)
- [InlineResponse20146ContentContentComment](docs/Model/InlineResponse20146ContentContentComment.md)
- [InlineResponse20146ContentContentMail](docs/Model/InlineResponse20146ContentContentMail.md)
- [InlineResponse20146ContentContentSupport](docs/Model/InlineResponse20146ContentContentSupport.md)
- [InlineResponse20146ContentContentTicket](docs/Model/InlineResponse20146ContentContentTicket.md)
- [InlineResponse20147](docs/Model/InlineResponse20147.md)
- [InlineResponse20149](docs/Model/InlineResponse20149.md)
- [InlineResponse2015](docs/Model/InlineResponse2015.md)
- [InlineResponse20150](docs/Model/InlineResponse20150.md)
- [InlineResponse20151](docs/Model/InlineResponse20151.md)
- [InlineResponse20152](docs/Model/InlineResponse20152.md)
- [InlineResponse20153](docs/Model/InlineResponse20153.md)
- [InlineResponse2016](docs/Model/InlineResponse2016.md)
- [InlineResponse2017](docs/Model/InlineResponse2017.md)
- [InlineResponse2018](docs/Model/InlineResponse2018.md)
- [InlineResponse2019](docs/Model/InlineResponse2019.md)
- [InlineResponse201Meta](docs/Model/InlineResponse201Meta.md)
- [InlineResponse201MetaProperties](docs/Model/InlineResponse201MetaProperties.md)
- [InlineResponse202](docs/Model/InlineResponse202.md)
- [InlineResponse2023](docs/Model/InlineResponse2023.md)
- [InlineResponse2026](docs/Model/InlineResponse2026.md)
- [InlineResponse2027](docs/Model/InlineResponse2027.md)
- [InlineResponse204](docs/Model/InlineResponse204.md)
- [InlineResponse403](docs/Model/InlineResponse403.md)
- [InlineResponse4031](docs/Model/InlineResponse4031.md)
- [InlineResponse4031Meta](docs/Model/InlineResponse4031Meta.md)
- [InlineResponse403Error](docs/Model/InlineResponse403Error.md)

## Authorization
All endpoints do not require authorization.
## Tests

To run the tests, use:

```bash
composer install
vendor/bin/phpunit
```

## Author

support@tanss.de

## About this package

This PHP package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: `5.8.22.1`
    - Package version: `5.8.22.1`
- Build package: `org.openapitools.codegen.languages.PhpClientCodegen`

# SLIS\Adapter\Tanss\ManufacturerApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1CpusIdDelete()**](ManufacturerApi.md#apiV1CpusIdDelete) | **DELETE** /api/v1/cpus/{id} | Deletes a cpu
[**apiV1ManufacturersGet()**](ManufacturerApi.md#apiV1ManufacturersGet) | **GET** /api/v1/manufacturers | Get a list of all manufacturers
[**apiV1ManufacturersIdDelete()**](ManufacturerApi.md#apiV1ManufacturersIdDelete) | **DELETE** /api/v1/manufacturers/{id} | Deletes a manufacturer
[**apiV1ManufacturersIdGet()**](ManufacturerApi.md#apiV1ManufacturersIdGet) | **GET** /api/v1/manufacturers/{id} | Get a manufacturer
[**apiV1ManufacturersIdPut()**](ManufacturerApi.md#apiV1ManufacturersIdPut) | **PUT** /api/v1/manufacturers/{id} | Updates a manufacturer
[**apiV1ManufacturersPost()**](ManufacturerApi.md#apiV1ManufacturersPost) | **POST** /api/v1/manufacturers | Creates a new manufacturer


## `apiV1CpusIdDelete()`

```php
apiV1CpusIdDelete($id)
```

Deletes a cpu

Deletes a cpu

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ManufacturerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | Id of the cpu

try {
    $apiInstance->apiV1CpusIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling ManufacturerApi->apiV1CpusIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Id of the cpu |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ManufacturersGet()`

```php
apiV1ManufacturersGet(): \SLIS\Adapter\Tanss\Model\InlineResponse20093
```

Get a list of all manufacturers

Gets a list of all manufacturers

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ManufacturerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiV1ManufacturersGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ManufacturerApi->apiV1ManufacturersGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20093**](../Model/InlineResponse20093.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ManufacturersIdDelete()`

```php
apiV1ManufacturersIdDelete($id)
```

Deletes a manufacturer

Deletes a manufacturer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ManufacturerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | Id of the manufacturer

try {
    $apiInstance->apiV1ManufacturersIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling ManufacturerApi->apiV1ManufacturersIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Id of the manufacturer |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ManufacturersIdGet()`

```php
apiV1ManufacturersIdGet($id): \SLIS\Adapter\Tanss\Model\InlineResponse20150
```

Get a manufacturer

Gets a specific manufacturer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ManufacturerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | Id of the manufacturer

try {
    $result = $apiInstance->apiV1ManufacturersIdGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ManufacturerApi->apiV1ManufacturersIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Id of the manufacturer |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20150**](../Model/InlineResponse20150.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ManufacturersIdPut()`

```php
apiV1ManufacturersIdPut($id, $body): \SLIS\Adapter\Tanss\Model\InlineResponse20150
```

Updates a manufacturer

Updates an existing manufacturer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ManufacturerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | Id of the manufacturer
$body = new \stdClass; // object | manufacturer to be updated

try {
    $result = $apiInstance->apiV1ManufacturersIdPut($id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ManufacturerApi->apiV1ManufacturersIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Id of the manufacturer |
 **body** | **object**| manufacturer to be updated |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20150**](../Model/InlineResponse20150.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ManufacturersPost()`

```php
apiV1ManufacturersPost($inlineObject33): \SLIS\Adapter\Tanss\Model\InlineResponse20150
```

Creates a new manufacturer

Creates a new manufacturer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ManufacturerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject33 = new \SLIS\Adapter\Tanss\Model\InlineObject33(); // \SLIS\Adapter\Tanss\Model\InlineObject33

try {
    $result = $apiInstance->apiV1ManufacturersPost($inlineObject33);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ManufacturerApi->apiV1ManufacturersPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject33** | [**\SLIS\Adapter\Tanss\Model\InlineObject33**](../Model/InlineObject33.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20150**](../Model/InlineResponse20150.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

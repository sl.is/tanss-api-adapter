# # InlineObject8

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groupName** | **string** | name of the monitoring group. * You can use wildcards with a \&quot;*\&quot; char, i.e. \&quot;Server XY -*\&quot; uses all groups starting with \&quot;Server XY -\&quot; | [optional]
**companyId** | **int** | id of the company for this monitoring group | [optional]
**linkTypeId** | **int** | LinkTypeId of the assignment (if known) | [optional]
**linkId** | **int** | LinkId of the assignment (if known) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

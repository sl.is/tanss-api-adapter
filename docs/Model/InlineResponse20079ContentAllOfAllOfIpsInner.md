# # InlineResponse20079ContentAllOfAllOfIpsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of this network interface entry | [optional]
**assignmentType** | **string** | determines, which \&quot;id\&quot; is meant by \&quot;assignmentId\&quot; | [optional]
**assignmentId** | **int** | id of the assignment, which this entry is assigned to (i.e. id of the pc) | [optional]
**ip** | **string** | ip address | [optional]
**mac** | **string** | mac address | [optional]
**remark** | **string** | remarks | [optional]
**dhcp** | **bool** | is this entry a dhcp assigned ip address? | [optional]
**serviceAssignments** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20079ContentAllOfAllOfIpsItemsServiceAssignmentsInner[]**](InlineResponse20079ContentAllOfAllOfIpsItemsServiceAssignmentsInner.md) | if you define ip addresses, you can also give a list of service assignments which shall be assigned to this ip address | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

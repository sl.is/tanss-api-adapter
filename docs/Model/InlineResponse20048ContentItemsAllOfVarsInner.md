# # InlineResponse20048ContentItemsAllOfVarsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | name of the variable | [optional]
**type** | **string** | defines the type of a checklist variable | [optional]
**options** | **string[]** | this fields is only set on MULTISELECT varaibles and contains all selectable options | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

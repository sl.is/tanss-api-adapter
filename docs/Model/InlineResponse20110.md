# # InlineResponse20110

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | **object** |  | [optional]
**content** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20110Content[]**](InlineResponse20110Content.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

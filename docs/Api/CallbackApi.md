# SLIS\Adapter\Tanss\CallbackApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1CallbacksCallbackIdGet()**](CallbackApi.md#apiV1CallbacksCallbackIdGet) | **GET** /api/v1/callbacks/{callbackId} | Gets a callback
[**apiV1CallbacksCallbackIdPut()**](CallbackApi.md#apiV1CallbacksCallbackIdPut) | **PUT** /api/v1/callbacks/{callbackId} | Updates a callback
[**apiV1CallbacksPost()**](CallbackApi.md#apiV1CallbacksPost) | **POST** /api/v1/callbacks | Creates a callback
[**apiV1CallbacksPut()**](CallbackApi.md#apiV1CallbacksPut) | **PUT** /api/v1/callbacks | Get a list of callbacks


## `apiV1CallbacksCallbackIdGet()`

```php
apiV1CallbacksCallbackIdGet($callbackId): \SLIS\Adapter\Tanss\Model\InlineResponse20060
```

Gets a callback

Gets a single callback

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CallbackApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$callbackId = 56; // int | Id of the callback to be fetched

try {
    $result = $apiInstance->apiV1CallbacksCallbackIdGet($callbackId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CallbackApi->apiV1CallbacksCallbackIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **callbackId** | **int**| Id of the callback to be fetched |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20060**](../Model/InlineResponse20060.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1CallbacksCallbackIdPut()`

```php
apiV1CallbacksCallbackIdPut($callbackId, $body): \SLIS\Adapter\Tanss\Model\InlineResponse20060
```

Updates a callback

Updates a callback

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CallbackApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$callbackId = 56; // int | Id of the callback to be updated
$body = new \stdClass; // object | callback object to be updated

try {
    $result = $apiInstance->apiV1CallbacksCallbackIdPut($callbackId, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CallbackApi->apiV1CallbacksCallbackIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **callbackId** | **int**| Id of the callback to be updated |
 **body** | **object**| callback object to be updated |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20060**](../Model/InlineResponse20060.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1CallbacksPost()`

```php
apiV1CallbacksPost($inlineObject24): \SLIS\Adapter\Tanss\Model\InlineResponse20128
```

Creates a callback

Creates a new callback in TANSS

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CallbackApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject24 = new \SLIS\Adapter\Tanss\Model\InlineObject24(); // \SLIS\Adapter\Tanss\Model\InlineObject24

try {
    $result = $apiInstance->apiV1CallbacksPost($inlineObject24);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CallbackApi->apiV1CallbacksPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject24** | [**\SLIS\Adapter\Tanss\Model\InlineObject24**](../Model/InlineObject24.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20128**](../Model/InlineResponse20128.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1CallbacksPut()`

```php
apiV1CallbacksPut($inlineObject23): \SLIS\Adapter\Tanss\Model\InlineResponse20059
```

Get a list of callbacks

Retrieves a list of callback from the database, using misc. filter settings

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CallbackApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject23 = new \SLIS\Adapter\Tanss\Model\InlineObject23(); // \SLIS\Adapter\Tanss\Model\InlineObject23

try {
    $result = $apiInstance->apiV1CallbacksPut($inlineObject23);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CallbackApi->apiV1CallbacksPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject23** | [**\SLIS\Adapter\Tanss\Model\InlineObject23**](../Model/InlineObject23.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20059**](../Model/InlineResponse20059.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

# # ApiV1SearchConfigsCompanyAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**maxResults** | **int** | maximum number of companies to fetch. If value is omitted (null), then 100 max. results are used | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

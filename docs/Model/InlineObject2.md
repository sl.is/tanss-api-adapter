# # InlineObject2

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timeframe** | [**\SLIS\Adapter\Tanss\Model\InlineObject2Timeframe**](InlineObject2Timeframe.md) |  | [optional]
**showTrysAsWell** | **bool** | if true, will show \&quot;tries\&quot; as well, meaning phone calls which didn&#39;t have an established connection | [optional]
**numberFilters** | **string[]** | one or more telephone number filters to be applied to the list | [optional]
**employeeId** | **int** | only shows phone calls of a certain employee | [optional]
**companyId** | **int** | only shows phone calls of these companies | [optional]
**numberInfos** | **bool** | if true, then the fields \&quot;fromPhoneNrInfos\&quot; and \&quot;toPhoneNrInfos\&quot; will be determined for the calls | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# SLIS\Adapter\Tanss\IdentifyApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1IdentifyPost()**](IdentifyApi.md#apiV1IdentifyPost) | **POST** /api/v1/identify | identifies items


## `apiV1IdentifyPost()`

```php
apiV1IdentifyPost($inlineObject36): \SLIS\Adapter\Tanss\Model\InlineResponse200101
```

identifies items

This call lets you \"identify\" misc. items, i.e. get a manufacturer id by name

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\IdentifyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject36 = array(new \SLIS\Adapter\Tanss\Model\InlineObject36()); // \SLIS\Adapter\Tanss\Model\InlineObject36[]

try {
    $result = $apiInstance->apiV1IdentifyPost($inlineObject36);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IdentifyApi->apiV1IdentifyPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject36** | [**\SLIS\Adapter\Tanss\Model\InlineObject36[]**](../Model/InlineObject36.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse200101**](../Model/InlineResponse200101.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

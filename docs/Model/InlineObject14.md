# # InlineObject14

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**balance** | **int** | initial balance for this employee (in minutes) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# SLIS\Adapter\Tanss\TimersApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1TimersDelete()**](TimersApi.md#apiV1TimersDelete) | **DELETE** /api/v1/timers | Deletes a timer
[**apiV1TimersGet()**](TimersApi.md#apiV1TimersGet) | **GET** /api/v1/timers | Get all timers of current user
[**apiV1TimersNotesDelete()**](TimersApi.md#apiV1TimersNotesDelete) | **DELETE** /api/v1/timers/notes | Deletes a timer fragment
[**apiV1TimersNotesPut()**](TimersApi.md#apiV1TimersNotesPut) | **PUT** /api/v1/timers/notes | Updates a timer fragment
[**apiV1TimersNotesTimerIdGet()**](TimersApi.md#apiV1TimersNotesTimerIdGet) | **GET** /api/v1/timers/notes/{timerId} | Get all timer fragments
[**apiV1TimersPost()**](TimersApi.md#apiV1TimersPost) | **POST** /api/v1/timers | Creates a timer
[**apiV1TimersTimerIdGet()**](TimersApi.md#apiV1TimersTimerIdGet) | **GET** /api/v1/timers/{timerId} | Get a specific timer
[**apiV1TimersTimerIdPut()**](TimersApi.md#apiV1TimersTimerIdPut) | **PUT** /api/v1/timers/{timerId} | Starts/stops timer


## `apiV1TimersDelete()`

```php
apiV1TimersDelete($body)
```

Deletes a timer

This route will delete a timer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TimersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \stdClass; // object | timer object to be deleted. The only important field is the \"id\"

try {
    $apiInstance->apiV1TimersDelete($body);
} catch (Exception $e) {
    echo 'Exception when calling TimersApi->apiV1TimersDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **object**| timer object to be deleted. The only important field is the \&quot;id\&quot; |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TimersGet()`

```php
apiV1TimersGet(): \SLIS\Adapter\Tanss\Model\InlineResponse20075
```

Get all timers of current user

This route will get all timers of the currently logged in user

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TimersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiV1TimersGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TimersApi->apiV1TimersGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20075**](../Model/InlineResponse20075.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TimersNotesDelete()`

```php
apiV1TimersNotesDelete($body)
```

Deletes a timer fragment

This route will delete a timer fragment

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TimersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \stdClass; // object | timer fragment to be deleted. The only important field is the \"startTime\" and \"timerId\"

try {
    $apiInstance->apiV1TimersNotesDelete($body);
} catch (Exception $e) {
    echo 'Exception when calling TimersApi->apiV1TimersNotesDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **object**| timer fragment to be deleted. The only important field is the \&quot;startTime\&quot; and \&quot;timerId\&quot; |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TimersNotesPut()`

```php
apiV1TimersNotesPut($body): \SLIS\Adapter\Tanss\Model\InlineResponse2027
```

Updates a timer fragment

This route will update a timer fragment and is used to store / update a \"note\" in the fragment

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TimersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \stdClass; // object | timer fragment to be updated

try {
    $result = $apiInstance->apiV1TimersNotesPut($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TimersApi->apiV1TimersNotesPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **object**| timer fragment to be updated |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2027**](../Model/InlineResponse2027.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TimersNotesTimerIdGet()`

```php
apiV1TimersNotesTimerIdGet($timerId): \SLIS\Adapter\Tanss\Model\InlineResponse20078
```

Get all timer fragments

This route will get all timer fragments of a given timer (start- / stop-dates)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TimersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$timerId = 56; // int | id of the timer

try {
    $result = $apiInstance->apiV1TimersNotesTimerIdGet($timerId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TimersApi->apiV1TimersNotesTimerIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timerId** | **int**| id of the timer |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20078**](../Model/InlineResponse20078.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TimersPost()`

```php
apiV1TimersPost(): \SLIS\Adapter\Tanss\Model\InlineResponse20131
```

Creates a timer

This route will create a timer for the currently logged in user

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TimersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiV1TimersPost();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TimersApi->apiV1TimersPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20131**](../Model/InlineResponse20131.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TimersTimerIdGet()`

```php
apiV1TimersTimerIdGet($timerId): \SLIS\Adapter\Tanss\Model\InlineResponse20131
```

Get a specific timer

This route will get a specific timer, given by an id

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TimersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$timerId = 56; // int

try {
    $result = $apiInstance->apiV1TimersTimerIdGet($timerId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TimersApi->apiV1TimersTimerIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timerId** | **int**|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20131**](../Model/InlineResponse20131.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TimersTimerIdPut()`

```php
apiV1TimersTimerIdPut($timerId): \SLIS\Adapter\Tanss\Model\InlineResponse20131
```

Starts/stops timer

This route will either start or stop a timer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TimersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$timerId = 56; // int

try {
    $result = $apiInstance->apiV1TimersTimerIdPut($timerId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TimersApi->apiV1TimersTimerIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timerId** | **int**|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20131**](../Model/InlineResponse20131.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

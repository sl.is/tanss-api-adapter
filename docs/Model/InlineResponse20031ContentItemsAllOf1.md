# # InlineResponse20031ContentItemsAllOf1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**types** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20099Content[]**](InlineResponse20099Content.md) | all types within this category | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

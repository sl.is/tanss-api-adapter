# SLIS\Adapter\Tanss\MailsApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1MailsTestSmtpPost()**](MailsApi.md#apiV1MailsTestSmtpPost) | **POST** /api/v1/mails/test/smtp | Test email smtp settings


## `apiV1MailsTestSmtpPost()`

```php
apiV1MailsTestSmtpPost($receiver, $inlineObject20): \SLIS\Adapter\Tanss\Model\InlineResponse20124
```

Test email smtp settings

Route for testing smtp server settings. This route sends a mail to the specified receiver.  Need the permission to access the OSK in order to use this route.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\MailsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$receiver = 'receiver_example'; // string | receiver of the test message
$inlineObject20 = new \SLIS\Adapter\Tanss\Model\InlineObject20(); // \SLIS\Adapter\Tanss\Model\InlineObject20

try {
    $result = $apiInstance->apiV1MailsTestSmtpPost($receiver, $inlineObject20);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MailsApi->apiV1MailsTestSmtpPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **receiver** | **string**| receiver of the test message |
 **inlineObject20** | [**\SLIS\Adapter\Tanss\Model\InlineObject20**](../Model/InlineObject20.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20124**](../Model/InlineResponse20124.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

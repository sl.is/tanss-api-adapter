# SLIS\Adapter\Tanss\TicketsApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1TicketsHistoryTicketIdGet()**](TicketsApi.md#apiV1TicketsHistoryTicketIdGet) | **GET** /api/v1/tickets/history/{ticketId} | Gets a ticket history
[**apiV1TicketsPost()**](TicketsApi.md#apiV1TicketsPost) | **POST** /api/v1/tickets | Creates a ticket in the database
[**apiV1TicketsTicketIdCommentsPost()**](TicketsApi.md#apiV1TicketsTicketIdCommentsPost) | **POST** /api/v1/tickets/{ticketId}/comments | Creates a comment
[**apiV1TicketsTicketIdDelete()**](TicketsApi.md#apiV1TicketsTicketIdDelete) | **DELETE** /api/v1/tickets/{ticketId} | Deletes a ticket by id
[**apiV1TicketsTicketIdGet()**](TicketsApi.md#apiV1TicketsTicketIdGet) | **GET** /api/v1/tickets/{ticketId} | Gets a ticket by id
[**apiV1TicketsTicketIdPut()**](TicketsApi.md#apiV1TicketsTicketIdPut) | **PUT** /api/v1/tickets/{ticketId} | Updates a ticket


## `apiV1TicketsHistoryTicketIdGet()`

```php
apiV1TicketsHistoryTicketIdGet($ticketId): \SLIS\Adapter\Tanss\Model\InlineResponse2002
```

Gets a ticket history

This route will fetch a list of comments, supports and mails from a ticket

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ticketId = 56; // int

try {
    $result = $apiInstance->apiV1TicketsHistoryTicketIdGet($ticketId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketsApi->apiV1TicketsHistoryTicketIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketId** | **int**|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketsPost()`

```php
apiV1TicketsPost($uNKNOWNBASETYPE): \SLIS\Adapter\Tanss\Model\InlineResponse201
```

Creates a ticket in the database

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$uNKNOWNBASETYPE = new \SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE(); // \SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE | ticket object to be saved

try {
    $result = $apiInstance->apiV1TicketsPost($uNKNOWNBASETYPE);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketsApi->apiV1TicketsPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uNKNOWNBASETYPE** | [**\SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)| ticket object to be saved |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse201**](../Model/InlineResponse201.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketsTicketIdCommentsPost()`

```php
apiV1TicketsTicketIdCommentsPost($ticketId, $uNKNOWNBASETYPE, $pinned): \SLIS\Adapter\Tanss\Model\InlineResponse2011
```

Creates a comment

This call will create a comment for a given ticket

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ticketId = 56; // int
$uNKNOWNBASETYPE = new \SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE(); // \SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE | comment to be created
$pinned = True; // bool | comment can optionally be \"pinned\" to the ticket

try {
    $result = $apiInstance->apiV1TicketsTicketIdCommentsPost($ticketId, $uNKNOWNBASETYPE, $pinned);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketsApi->apiV1TicketsTicketIdCommentsPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketId** | **int**|  |
 **uNKNOWNBASETYPE** | [**\SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)| comment to be created |
 **pinned** | **bool**| comment can optionally be \&quot;pinned\&quot; to the ticket | [optional]

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2011**](../Model/InlineResponse2011.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketsTicketIdDelete()`

```php
apiV1TicketsTicketIdDelete($ticketId, $targetTicketId)
```

Deletes a ticket by id

This call will delete a ticket.  If the parameter \"targetTicketId\" is given, will also move entities such as chats or callbacks to a target ticket

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ticketId = 56; // int
$targetTicketId = 56; // int | if given, will also move entities such as chats or callbacks to a target ticket

try {
    $apiInstance->apiV1TicketsTicketIdDelete($ticketId, $targetTicketId);
} catch (Exception $e) {
    echo 'Exception when calling TicketsApi->apiV1TicketsTicketIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketId** | **int**|  |
 **targetTicketId** | **int**| if given, will also move entities such as chats or callbacks to a target ticket | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketsTicketIdGet()`

```php
apiV1TicketsTicketIdGet($ticketId): \SLIS\Adapter\Tanss\Model\InlineResponse2001
```

Gets a ticket by id

Fetches a ticket by id and returns all aspects of the ticket

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ticketId = 56; // int

try {
    $result = $apiInstance->apiV1TicketsTicketIdGet($ticketId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketsApi->apiV1TicketsTicketIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketId** | **int**|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketsTicketIdPut()`

```php
apiV1TicketsTicketIdPut($ticketId, $body): \SLIS\Adapter\Tanss\Model\InlineResponse202
```

Updates a ticket

Updated a ticket

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ticketId = 56; // int
$body = new \stdClass; // object | updated ticket object to be saved

try {
    $result = $apiInstance->apiV1TicketsTicketIdPut($ticketId, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketsApi->apiV1TicketsTicketIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketId** | **int**|  |
 **body** | **object**| updated ticket object to be saved |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse202**](../Model/InlineResponse202.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

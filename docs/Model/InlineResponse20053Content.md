# # InlineResponse20053Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**employeeId** | **int** | id of the employee | [optional]
**availability** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20053Availability**](InlineResponse20053Availability.md) |  | [optional]
**absences** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20053Absences[]**](InlineResponse20053Absences.md) | list of absenced the employee has right now | [optional]
**appointments** | **object[]** | list of appointments the employee has right now | [optional]
**endInfos** | [**array<string,\SLIS\Adapter\Tanss\Model\InlineResponse20053EndInfos>**](InlineResponse20053EndInfos.md) | specific informations about absences, when they will end.  They key ist the id of the absence, the value is an info object regarding infos | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

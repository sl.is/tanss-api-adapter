# # InlineResponse20051ContentItemsAllOf1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**workflows** | [**InlineResponse20051ContentItemsAllOfWorkflowsItemsAllOf[]**](InlineResponse20051ContentItemsAllOfWorkflowsItemsAllOf.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

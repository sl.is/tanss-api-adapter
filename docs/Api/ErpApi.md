# SLIS\Adapter\Tanss\ErpApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiErpV1CompaniesDepartmentsGet()**](ErpApi.md#apiErpV1CompaniesDepartmentsGet) | **GET** /api/erp/v1/companies/departments | gets all departments
[**apiErpV1CompaniesEmployeesDepartmentsGet()**](ErpApi.md#apiErpV1CompaniesEmployeesDepartmentsGet) | **GET** /api/erp/v1/companies/employees/departments/ | gets all users with the associated departments
[**apiErpV1CompaniesEmployeesGet()**](ErpApi.md#apiErpV1CompaniesEmployeesGet) | **GET** /api/erp/v1/companies/employees | get all employees from the own company
[**apiErpV1CompaniesSearchIdDisplayIdGet()**](ErpApi.md#apiErpV1CompaniesSearchIdDisplayIdGet) | **GET** /api/erp/v1/companies/searchId/{displayId} | search for a company
[**apiErpV1CompanyCategoriesGet()**](ErpApi.md#apiErpV1CompanyCategoriesGet) | **GET** /api/erp/v1/companyCategories | list of company categories
[**apiErpV1CustomersGet()**](ErpApi.md#apiErpV1CustomersGet) | **GET** /api/erp/v1/customers | Get a list of customers and employees
[**apiErpV1CustomersPost()**](ErpApi.md#apiErpV1CustomersPost) | **POST** /api/erp/v1/customers | Insert new customers
[**apiErpV1DepartmentsDepartmentIdEmployeesGet()**](ErpApi.md#apiErpV1DepartmentsDepartmentIdEmployeesGet) | **GET** /api/erp/v1/departments/{departmentId}/employees | gets all employees of a department
[**apiErpV1EmployeesEmployeeIdDepartmentsGet()**](ErpApi.md#apiErpV1EmployeesEmployeeIdDepartmentsGet) | **GET** /api/erp/v1/employees/{employeeId}/departments | gets all departments of a employee
[**apiErpV1InvoicesGet()**](ErpApi.md#apiErpV1InvoicesGet) | **GET** /api/erp/v1/invoices | Gets a list of billable supports
[**apiErpV1InvoicesPost()**](ErpApi.md#apiErpV1InvoicesPost) | **POST** /api/erp/v1/invoices | Insert new invoices
[**apiErpV1TicketsPost()**](ErpApi.md#apiErpV1TicketsPost) | **POST** /api/erp/v1/tickets | create a new ticket
[**apiErpV1TicketsStatusGet()**](ErpApi.md#apiErpV1TicketsStatusGet) | **GET** /api/erp/v1/tickets/status | gets a list of tickets states
[**apiErpV1TicketsTypesGet()**](ErpApi.md#apiErpV1TicketsTypesGet) | **GET** /api/erp/v1/tickets/types | gets a list of tickets types


## `apiErpV1CompaniesDepartmentsGet()`

```php
apiErpV1CompaniesDepartmentsGet(): \SLIS\Adapter\Tanss\Model\InlineResponse20030
```

gets all departments

This call gets a list of all the departments for the own company (= assignable departments which can be used to assign tickets)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ErpApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiErpV1CompaniesDepartmentsGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ErpApi->apiErpV1CompaniesDepartmentsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20030**](../Model/InlineResponse20030.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiErpV1CompaniesEmployeesDepartmentsGet()`

```php
apiErpV1CompaniesEmployeesDepartmentsGet(): \SLIS\Adapter\Tanss\Model\InlineResponse20035
```

gets all users with the associated departments

This call retrieves a list of all technicians. The associated departments are fetched and returned as well (as a HashMap). The key represent the id of the employee, the value is a list of departments

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ErpApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiErpV1CompaniesEmployeesDepartmentsGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ErpApi->apiErpV1CompaniesEmployeesDepartmentsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20035**](../Model/InlineResponse20035.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiErpV1CompaniesEmployeesGet()`

```php
apiErpV1CompaniesEmployeesGet(): \SLIS\Adapter\Tanss\Model\InlineResponse20029
```

get all employees from the own company

This call gets a list of all the employees from the own company (= technicians)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ErpApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiErpV1CompaniesEmployeesGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ErpApi->apiErpV1CompaniesEmployeesGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20029**](../Model/InlineResponse20029.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiErpV1CompaniesSearchIdDisplayIdGet()`

```php
apiErpV1CompaniesSearchIdDisplayIdGet($displayId): \SLIS\Adapter\Tanss\Model\InlineResponse20033
```

search for a company

This call searches for a customers based on the ERP customer number

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ErpApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$displayId = 'displayId_example'; // string | external customer number, which is used by the ERP system

try {
    $result = $apiInstance->apiErpV1CompaniesSearchIdDisplayIdGet($displayId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ErpApi->apiErpV1CompaniesSearchIdDisplayIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **displayId** | **string**| external customer number, which is used by the ERP system |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20033**](../Model/InlineResponse20033.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiErpV1CompanyCategoriesGet()`

```php
apiErpV1CompanyCategoriesGet(): \SLIS\Adapter\Tanss\Model\InlineResponse20031
```

list of company categories

Gets a list of all company categories

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ErpApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiErpV1CompanyCategoriesGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ErpApi->apiErpV1CompanyCategoriesGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20031**](../Model/InlineResponse20031.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiErpV1CustomersGet()`

```php
apiErpV1CustomersGet($apiToken, $modified): \SLIS\Adapter\Tanss\Model\InlineResponse20024
```

Get a list of customers and employees

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ErpApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$apiToken = 'apiToken_example'; // string | ERP Api-Token
$modified = 3.4; // float | Timestamp in seconds from which created or modified customers and employees will be send. If you set the value to -1 then all customers and employees will be send

try {
    $result = $apiInstance->apiErpV1CustomersGet($apiToken, $modified);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ErpApi->apiErpV1CustomersGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiToken** | **string**| ERP Api-Token |
 **modified** | **float**| Timestamp in seconds from which created or modified customers and employees will be send. If you set the value to -1 then all customers and employees will be send |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20024**](../Model/InlineResponse20024.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiErpV1CustomersPost()`

```php
apiErpV1CustomersPost($apiToken, $inlineObject11): \SLIS\Adapter\Tanss\Model\InlineResponse20025[]
```

Insert new customers

```xml <?xml version=\"1.0\" encoding=\"UTF-8\" ?> <MENTION_EXPORT type=\"kunden\">     <KUNDEN>         <ROW>             <!-- *Pflichtfeld* -->             <kdnummer>KUNDENNUMMER</kdnummer>             <!-- *Pflichtfeld* -->             <kdarchiv>IN-/AKTIV | 0 = aktiv , 1 = inaktiv</kdarchiv>             <!--     *Kein Pflichtfeld*      !!! Nur beachtet wenn Mention als Schnittstelle gewaehlt ist !!!      Setzt den Wawi-Konfigwert \"xml-wawi.import.interne_info.bemerkung\" voraus    -->             <kdbem>INTERNE INFO IN DEN FIRMENSTAMMDATEN</kdbem>             <!--     *Kein Pflichtfeld*      !!! Nur beachtet wenn Mention als Schnittstelle gewaehlt ist !!!      Diese Information wird bei den wichtigen Firmeninformationen in den Firmen-Stammdaten,     im Info-Symbol bei den Firmen-Icons, sowie im Telefonie-Popup mit ausgegeben.    -->             <kdverknr>VERKAUFER</kdverknr>         </ROW>     </KUNDEN>     <!--   *Alle Felder in dem Block TANSS sind optional*    Wird ein Element nicht angegeben, ignoriert TANSS dies beim Import.   Wird ein Element angegeben, jedoch mit einem leeren Inhalt, wird TANSS diesen Inhalt uebernehmen.  -->     <TANSS>         <ROW>             <matchcode>MATCHCODE</matchcode>             <fahrtkm>FAHRT-KM</fahrtkm>             <fahrtdauer>FAHRT-DAUER IN MINUTEN</fahrtdauer>             <fahrtzone>NAME DER ZONE</fahrtzone>             <fahrttyp>STANDARDTYP | z = Zone , k = Kilometer</fahrttyp>             <!--     Alle bereits zugewiesenen Kategorien werden entfernt und die neuen Kategorien werden zugewiesen.     Ist eine Kategorie in TANSS noch nicht vorhanden, wird diese neu angelegt.     Bestehende Kategorien werden in TANSS nicht geloescht.    -->             <kategorien>                 <kategorie>FIRMENKATEGORIE 1</kategorie>                 <kategorie>FIRMENKATEGORIE 2</kategorie>                 <kategorie>FIRMENKATEGORIE 3</kategorie>             </kategorien>         </ROW>     </TANSS>     <ADRESSEN>         <ROW>             <!-- *Pflichtfeld* -->             <adname2>KUNDENNAME</adname2>             <!-- *Pflichtfeld* -->             <adname3>KUNDENNAME ZEILE ZWEI</adname3>             <!-- *Kein Pflichtfeld* -->             <adstrasse>STRASSE</adstrasse>             <!-- *Kein Pflichtfeld* -->             <adplz>PLZ</adplz>             <!-- *Kein Pflichtfeld* -->             <adort>ORT</adort>             <!-- *Kein Pflichtfeld* -->             <adland>LAND</adland>             <!-- *Kein Pflichtfeld* -->             <adurl>WEBSEITE URL</adurl>         </ROW>     </ADRESSEN>     <ADRESSPA>         <ROW>             <!-- *Pflichtfeld* -->             <panummer>KUNDENNUMMER</panummer>             <!--     *Pflichtfeld*      Eindeutige Nummer pro Mitarbeiter.     Muss bei jedem Import pro Mitarbeiter die gleiche Nummer sein.    -->             <palfdnr>MITARBEITERNUMMER</palfdnr>             <!-- *Kein Pflichtfeld* -->             <pafunk>FUNKTION</pafunk>             <!-- *Kein Pflichtfeld* -->             <patelefon1>TELEFON 1</patelefon1>             <!-- *Kein Pflichtfeld* -->             <patelefon2>TELEFON 2</patelefon2>             <!-- *Kein Pflichtfeld* -->             <pafax>FAX</pafax>             <!-- *Kein Pflichtfeld* -->             <paemail>EMAIL</paemail>             <!-- *Kein Pflichtfeld* -->             <paanummer>ANREDE | Standard = 1 Herr , 2 Frau</paanummer>             <!-- *Pflichtfeld* -->             <paname1>VORNAME</paname1>             <!-- *Pflichtfeld* -->             <paname2>NACHNAME</paname2>             <!-- *Pflichtfeld* -->             <panoaktiv>IN-/AKTIV | 0 = aktiv , 1 = inaktiv</panoaktiv>             <!-- *Kein Pflichtfeld* -->             <patelefon3>MOBIL</patelefon3>             <!-- *Kein Pflichtfeld* -->             <patelefon4>PRIVAT TEL</patelefon4>             <!-- *Kein Pflichtfeld* -->             <pageburt>GEBURTSTAG | 0000-00-00T00:00:00</pageburt>             <!-- *Kein Pflichtfeld* (Muss in der Konfiguration mit dem Parameter 'xml-wawi.import.mitarbeiter_bemerkung_uebernehmen' aktiviert werden) -->             <pabem>BEMERKUNG</pabem>         </ROW>     </ADRESSPA> </MENTION_EXPORT> ```

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ErpApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$apiToken = 'apiToken_example'; // string | ERP Api-Token
$inlineObject11 = array(new \SLIS\Adapter\Tanss\Model\InlineObject11()); // \SLIS\Adapter\Tanss\Model\InlineObject11[]

try {
    $result = $apiInstance->apiErpV1CustomersPost($apiToken, $inlineObject11);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ErpApi->apiErpV1CustomersPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiToken** | **string**| ERP Api-Token |
 **inlineObject11** | [**\SLIS\Adapter\Tanss\Model\InlineObject11[]**](../Model/InlineObject11.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20025[]**](../Model/InlineResponse20025.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiErpV1DepartmentsDepartmentIdEmployeesGet()`

```php
apiErpV1DepartmentsDepartmentIdEmployeesGet($departmentId): \SLIS\Adapter\Tanss\Model\InlineResponse20032
```

gets all employees of a department

This call lists all employees of a given department

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ErpApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$departmentId = 56; // int | Id of the department

try {
    $result = $apiInstance->apiErpV1DepartmentsDepartmentIdEmployeesGet($departmentId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ErpApi->apiErpV1DepartmentsDepartmentIdEmployeesGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **departmentId** | **int**| Id of the department |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20032**](../Model/InlineResponse20032.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiErpV1EmployeesEmployeeIdDepartmentsGet()`

```php
apiErpV1EmployeesEmployeeIdDepartmentsGet($employeeId): \SLIS\Adapter\Tanss\Model\InlineResponse20034
```

gets all departments of a employee

This call gets all the associated departments of an employee

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ErpApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$employeeId = 56; // int | Id of the employee

try {
    $result = $apiInstance->apiErpV1EmployeesEmployeeIdDepartmentsGet($employeeId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ErpApi->apiErpV1EmployeesEmployeeIdDepartmentsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **employeeId** | **int**| Id of the employee |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20034**](../Model/InlineResponse20034.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiErpV1InvoicesGet()`

```php
apiErpV1InvoicesGet($apiToken, $pdf): \SLIS\Adapter\Tanss\Model\InlineResponse20022[]
```

Gets a list of billable supports

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ErpApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$apiToken = 'apiToken_example'; // string | ERP Api-Token
$pdf = True; // bool | When set to true the response includes the support evaluation PDF without an invoice number in Base64 format

try {
    $result = $apiInstance->apiErpV1InvoicesGet($apiToken, $pdf);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ErpApi->apiErpV1InvoicesGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiToken** | **string**| ERP Api-Token |
 **pdf** | **bool**| When set to true the response includes the support evaluation PDF without an invoice number in Base64 format | [optional]

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20022[]**](../Model/InlineResponse20022.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiErpV1InvoicesPost()`

```php
apiErpV1InvoicesPost($apiToken, $inlineObject10): \SLIS\Adapter\Tanss\Model\InlineResponse20023[]
```

Insert new invoices

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ErpApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$apiToken = 'apiToken_example'; // string | ERP Api-Token
$inlineObject10 = array(new \SLIS\Adapter\Tanss\Model\InlineObject10()); // \SLIS\Adapter\Tanss\Model\InlineObject10[]

try {
    $result = $apiInstance->apiErpV1InvoicesPost($apiToken, $inlineObject10);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ErpApi->apiErpV1InvoicesPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiToken** | **string**| ERP Api-Token |
 **inlineObject10** | [**\SLIS\Adapter\Tanss\Model\InlineObject10[]**](../Model/InlineObject10.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20023[]**](../Model/InlineResponse20023.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiErpV1TicketsPost()`

```php
apiErpV1TicketsPost($body): \SLIS\Adapter\Tanss\Model\InlineResponse20026
```

create a new ticket

This call creates a new ticket in the database

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ErpApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \stdClass; // object

try {
    $result = $apiInstance->apiErpV1TicketsPost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ErpApi->apiErpV1TicketsPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **object**|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20026**](../Model/InlineResponse20026.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiErpV1TicketsStatusGet()`

```php
apiErpV1TicketsStatusGet(): \SLIS\Adapter\Tanss\Model\InlineResponse20027
```

gets a list of tickets states

This call retrieves a list of all ticket states in the system

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ErpApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiErpV1TicketsStatusGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ErpApi->apiErpV1TicketsStatusGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20027**](../Model/InlineResponse20027.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiErpV1TicketsTypesGet()`

```php
apiErpV1TicketsTypesGet(): \SLIS\Adapter\Tanss\Model\InlineResponse20028
```

gets a list of tickets types

This call gets a list of all the ticket types in the system

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ErpApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiErpV1TicketsTypesGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ErpApi->apiErpV1TicketsTypesGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20028**](../Model/InlineResponse20028.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

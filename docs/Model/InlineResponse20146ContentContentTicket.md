# # InlineResponse20146ContentContentTicket

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the ticket | [optional]
**companyId** | **int** | id of the company of the ticket | [optional]
**companyName** | **string** | name of the company | [optional]
**remitterId** | **int** | id of the remitter (employee id) | [optional]
**remitterName** | **string** | name of the remitter | [optional]
**title** | **string** | title of the ticket | [optional]
**content** | **string** | content of the ticket | [optional]
**assignedToEmployeeId** | **int** | id of the assigned employee | [optional]
**assignedToEmployeeName** | **string** | name of the assigned employee | [optional]
**assignedToDepartmentId** | **int** | id of the assigned department | [optional]
**assignedToDepartmentName** | **string** | name of the assigned department | [optional]
**statusId** | **int** | id of the ticket status | [optional]
**statusName** | **string** | name of the ticket status | [optional]
**typeId** | **int** | id of the ticket type | [optional]
**typeName** | **string** | name of the ticket type | [optional]
**dueDate** | **int** | due date as timestamp | [optional]
**dueDateFormatted** | **string** | formatted due date | [optional]
**deadlineDate** | **int** | deadline date as timestamp | [optional]
**deadlineDateFormatted** | **string** | formatted deadline date | [optional]
**serviceCapAmount** | **float** | service cap of the ticket | [optional]
**serviceCapAmountFormatted** | **string** | formatted service cap | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

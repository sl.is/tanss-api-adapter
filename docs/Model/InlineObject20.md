# # InlineObject20

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**smtpAddress** | **string** | e-mail address for sending e-mails | [optional]
**smtpHost** | **string** | host (address) of the smtp server | [optional]
**smtpUser** | **string** | username of the smtp server | [optional]
**smtpPassword** | **string** | password of the smtp server | [optional]
**smtpAuth** | **bool** | true if the smtp server requires authentication | [optional]
**smtpEncryptionType** | **string** | enum representing the security setting of the mail server | [optional]
**smtpSenderName** | **string** | send mails with this displayed name (optional) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineResponse20017Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | **string** | UserId which is transferred in a remote support object | [optional]
**employeeId** | **int** | Employee id of the TANSS user | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

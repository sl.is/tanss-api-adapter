# # InlineResponse20065ContentAllOf1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20065ContentAllOfItemsInner[]**](InlineResponse20065ContentAllOfItemsInner.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

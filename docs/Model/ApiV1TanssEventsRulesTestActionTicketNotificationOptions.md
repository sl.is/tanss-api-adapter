# # ApiV1TanssEventsRulesTestActionTicketNotificationOptions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reason** | **string** | reason of the ticket notification | [optional]
**mailId** | **int** | if reason is \&quot;EMAIL_RECEIVED\&quot;, the id of the mail is given here | [optional]
**commentId** | **int** | if reason is \&quot;NEW_COMMENT\&quot;, the id of the comment is given here | [optional]
**supportId** | **int** | if reason is \&quot;NEW_SUPPORT\&quot;, the id of the support is given here | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

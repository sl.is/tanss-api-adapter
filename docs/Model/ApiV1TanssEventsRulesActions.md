# # ApiV1TanssEventsRulesActions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**actionType** | **string** | defines which action shall be executed | [optional]
**params** | [**\SLIS\Adapter\Tanss\Model\ApiV1TanssEventsRulesParams**](ApiV1TanssEventsRulesParams.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

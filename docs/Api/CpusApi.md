# SLIS\Adapter\Tanss\CpusApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1CpusGet()**](CpusApi.md#apiV1CpusGet) | **GET** /api/v1/cpus | Get a list of all cpus
[**apiV1CpusIdGet()**](CpusApi.md#apiV1CpusIdGet) | **GET** /api/v1/cpus/{id} | Get a cpu
[**apiV1CpusIdPut()**](CpusApi.md#apiV1CpusIdPut) | **PUT** /api/v1/cpus/{id} | Updates a cpu
[**apiV1CpusPost()**](CpusApi.md#apiV1CpusPost) | **POST** /api/v1/cpus | Creates a new cpu


## `apiV1CpusGet()`

```php
apiV1CpusGet(): \SLIS\Adapter\Tanss\Model\InlineResponse20095
```

Get a list of all cpus

Gets a list of all cpus

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CpusApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiV1CpusGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CpusApi->apiV1CpusGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20095**](../Model/InlineResponse20095.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1CpusIdGet()`

```php
apiV1CpusIdGet($id): \SLIS\Adapter\Tanss\Model\InlineResponse20151
```

Get a cpu

Gets a specific cpu

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CpusApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | Id of the cpu

try {
    $result = $apiInstance->apiV1CpusIdGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CpusApi->apiV1CpusIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Id of the cpu |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20151**](../Model/InlineResponse20151.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1CpusIdPut()`

```php
apiV1CpusIdPut($id, $body): \SLIS\Adapter\Tanss\Model\InlineResponse20151
```

Updates a cpu

Updates an existing cpu

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CpusApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | Id of the cpu
$body = new \stdClass; // object | cpu to be updated

try {
    $result = $apiInstance->apiV1CpusIdPut($id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CpusApi->apiV1CpusIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Id of the cpu |
 **body** | **object**| cpu to be updated |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20151**](../Model/InlineResponse20151.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1CpusPost()`

```php
apiV1CpusPost($inlineObject34): \SLIS\Adapter\Tanss\Model\InlineResponse20151
```

Creates a new cpu

Creates a new cpu

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CpusApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject34 = new \SLIS\Adapter\Tanss\Model\InlineObject34(); // \SLIS\Adapter\Tanss\Model\InlineObject34

try {
    $result = $apiInstance->apiV1CpusPost($inlineObject34);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CpusApi->apiV1CpusPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject34** | [**\SLIS\Adapter\Tanss\Model\InlineObject34**](../Model/InlineObject34.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20151**](../Model/InlineResponse20151.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

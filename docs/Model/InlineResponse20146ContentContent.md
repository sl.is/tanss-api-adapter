# # InlineResponse20146ContentContent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ticket** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20146ContentContentTicket**](InlineResponse20146ContentContentTicket.md) |  | [optional]
**oldValues** | **object** |  | [optional]
**comment** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20146ContentContentComment**](InlineResponse20146ContentContentComment.md) |  | [optional]
**mail** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20146ContentContentMail**](InlineResponse20146ContentContentMail.md) |  | [optional]
**support** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20146ContentContentSupport**](InlineResponse20146ContentContentSupport.md) |  | [optional]
**addedTags** | **object[]** | if tags were added, here the list of added tags is given | [optional]
**removedTags** | **object[]** | if tags were removed, here the list of removed tags is given | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

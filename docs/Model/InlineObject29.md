# # InlineObject29

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**name** | **string** | defines a \&quot;name\&quot; for the rule | [optional]
**active** | **bool** | true if the rule is active. If \&quot;false\&quot;, rule won&#39;t trigger notifications | [optional]
**assignments** | [**\SLIS\Adapter\Tanss\Model\ApiV1TanssEventsRulesAssignments[]**](ApiV1TanssEventsRulesAssignments.md) | defines for which \&quot;assignments\&quot; a rule shall be triggered (currently only used for ticket board panels) | [optional]
**actions** | [**\SLIS\Adapter\Tanss\Model\ApiV1TanssEventsRulesActions[]**](ApiV1TanssEventsRulesActions.md) | defines which actions will be triggered when executing the rule | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

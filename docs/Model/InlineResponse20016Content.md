# # InlineResponse20016Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deviceId** | **string** | DeviceId which is transferred in a remote support object | [optional]
**companyId** | **int** | Company which this deviceId refers to | [optional]
**linkTypeId** | **int** | LinkTypeId of the assignment which this deviceId refers to | [optional]
**linkId** | **int** | LinkId of the assignment which this deviceId refers to | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

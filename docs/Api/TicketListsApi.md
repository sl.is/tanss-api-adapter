# SLIS\Adapter\Tanss\TicketListsApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1TicketsCompanyCompanyIdGet()**](TicketListsApi.md#apiV1TicketsCompanyCompanyIdGet) | **GET** /api/v1/tickets/company/{companyId} | gets a list of company tickets
[**apiV1TicketsGeneralGet()**](TicketListsApi.md#apiV1TicketsGeneralGet) | **GET** /api/v1/tickets/general | gets a list of general tickets (assigned to no employee)
[**apiV1TicketsLocalAdminOverviewGet()**](TicketListsApi.md#apiV1TicketsLocalAdminOverviewGet) | **GET** /api/v1/tickets/localAdminOverview | gets a list of all tickets which are assigned to local ticket admins
[**apiV1TicketsNotIdentifiedGet()**](TicketListsApi.md#apiV1TicketsNotIdentifiedGet) | **GET** /api/v1/tickets/notIdentified | gets a list of not identified tickets
[**apiV1TicketsOwnGet()**](TicketListsApi.md#apiV1TicketsOwnGet) | **GET** /api/v1/tickets/own | gets a list of own tickets (assigned to currently logged in employee)
[**apiV1TicketsProjectsGet()**](TicketListsApi.md#apiV1TicketsProjectsGet) | **GET** /api/v1/tickets/projects | gets a list of all projects
[**apiV1TicketsPut()**](TicketListsApi.md#apiV1TicketsPut) | **PUT** /api/v1/tickets | Get a (custom) ticket list
[**apiV1TicketsRepairGet()**](TicketListsApi.md#apiV1TicketsRepairGet) | **GET** /api/v1/tickets/repair | gets a list of repair tickets
[**apiV1TicketsTechnicianGet()**](TicketListsApi.md#apiV1TicketsTechnicianGet) | **GET** /api/v1/tickets/technician | gets a list of tickets of all technicians
[**apiV1TicketsWithRoleGet()**](TicketListsApi.md#apiV1TicketsWithRoleGet) | **GET** /api/v1/tickets/withRole | gets a list of all ticket which a technician has a role in


## `apiV1TicketsCompanyCompanyIdGet()`

```php
apiV1TicketsCompanyCompanyIdGet($companyId)
```

gets a list of company tickets

This route gets a list of all tickets for a given company

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketListsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$companyId = 56; // int | id of the company for the tickets

try {
    $apiInstance->apiV1TicketsCompanyCompanyIdGet($companyId);
} catch (Exception $e) {
    echo 'Exception when calling TicketListsApi->apiV1TicketsCompanyCompanyIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **companyId** | **int**| id of the company for the tickets |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketsGeneralGet()`

```php
apiV1TicketsGeneralGet()
```

gets a list of general tickets (assigned to no employee)

This route gets a list of all open tickets which are not assigned to any employee

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketListsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV1TicketsGeneralGet();
} catch (Exception $e) {
    echo 'Exception when calling TicketListsApi->apiV1TicketsGeneralGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketsLocalAdminOverviewGet()`

```php
apiV1TicketsLocalAdminOverviewGet()
```

gets a list of all tickets which are assigned to local ticket admins

This route gets a list of all tickets which are \"under control\" of local ticket admins

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketListsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV1TicketsLocalAdminOverviewGet();
} catch (Exception $e) {
    echo 'Exception when calling TicketListsApi->apiV1TicketsLocalAdminOverviewGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketsNotIdentifiedGet()`

```php
apiV1TicketsNotIdentifiedGet()
```

gets a list of not identified tickets

This route gets a list of all tickets, which couldn't be assigned to a company and therefore are \"unidentified\"

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketListsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV1TicketsNotIdentifiedGet();
} catch (Exception $e) {
    echo 'Exception when calling TicketListsApi->apiV1TicketsNotIdentifiedGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketsOwnGet()`

```php
apiV1TicketsOwnGet(): \SLIS\Adapter\Tanss\Model\InlineResponse200
```

gets a list of own tickets (assigned to currently logged in employee)

This route gets a list of all open tickets which are assigned to the currently logged in employee

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketListsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiV1TicketsOwnGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketListsApi->apiV1TicketsOwnGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse200**](../Model/InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketsProjectsGet()`

```php
apiV1TicketsProjectsGet()
```

gets a list of all projects

This route gets a list of all projects

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketListsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV1TicketsProjectsGet();
} catch (Exception $e) {
    echo 'Exception when calling TicketListsApi->apiV1TicketsProjectsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketsPut()`

```php
apiV1TicketsPut($inlineObject)
```

Get a (custom) ticket list

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketListsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject = new \SLIS\Adapter\Tanss\Model\InlineObject(); // \SLIS\Adapter\Tanss\Model\InlineObject

try {
    $apiInstance->apiV1TicketsPut($inlineObject);
} catch (Exception $e) {
    echo 'Exception when calling TicketListsApi->apiV1TicketsPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject** | [**\SLIS\Adapter\Tanss\Model\InlineObject**](../Model/InlineObject.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketsRepairGet()`

```php
apiV1TicketsRepairGet()
```

gets a list of repair tickets

This route gets a list of all repair tickets

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketListsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV1TicketsRepairGet();
} catch (Exception $e) {
    echo 'Exception when calling TicketListsApi->apiV1TicketsRepairGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketsTechnicianGet()`

```php
apiV1TicketsTechnicianGet()
```

gets a list of tickets of all technicians

This route gets a list of tickets of all technicians (excluding currently logged in employee)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketListsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV1TicketsTechnicianGet();
} catch (Exception $e) {
    echo 'Exception when calling TicketListsApi->apiV1TicketsTechnicianGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketsWithRoleGet()`

```php
apiV1TicketsWithRoleGet()
```

gets a list of all ticket which a technician has a role in

This route gets a list of all tickets which the currently logged in technician has a role in

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketListsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV1TicketsWithRoleGet();
} catch (Exception $e) {
    echo 'Exception when calling TicketListsApi->apiV1TicketsWithRoleGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

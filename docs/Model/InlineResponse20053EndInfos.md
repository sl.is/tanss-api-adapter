# # InlineResponse20053EndInfos

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timeType** | **string** | specifies, when an absence will end (in word TODAY, TOMORROW or TIME). The frontend can then render the result in a better way | [optional]
**time** | **int** | timestamp, when the vacation/absence will end | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# SLIS\Adapter\Tanss\ComponentsApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1ComponentsComponentIdDelete()**](ComponentsApi.md#apiV1ComponentsComponentIdDelete) | **DELETE** /api/v1/components/{componentId} | Deletes a component
[**apiV1ComponentsComponentIdGet()**](ComponentsApi.md#apiV1ComponentsComponentIdGet) | **GET** /api/v1/components/{componentId} | Gets a component by id
[**apiV1ComponentsComponentIdPut()**](ComponentsApi.md#apiV1ComponentsComponentIdPut) | **PUT** /api/v1/components/{componentId} | Updates a component
[**apiV1ComponentsPost()**](ComponentsApi.md#apiV1ComponentsPost) | **POST** /api/v1/components | Creates a component
[**apiV1ComponentsPut()**](ComponentsApi.md#apiV1ComponentsPut) | **PUT** /api/v1/components | Gets a list of components
[**apiV1ComponentsTypesGet()**](ComponentsApi.md#apiV1ComponentsTypesGet) | **GET** /api/v1/components/types | Gets a list of component types
[**apiV1ComponentsTypesPost()**](ComponentsApi.md#apiV1ComponentsTypesPost) | **POST** /api/v1/components/types | Create component type
[**apiV1ComponentsTypesTypeIdDelete()**](ComponentsApi.md#apiV1ComponentsTypesTypeIdDelete) | **DELETE** /api/v1/components/types/{typeId} | Delete component type
[**apiV1ComponentsTypesTypeIdPut()**](ComponentsApi.md#apiV1ComponentsTypesTypeIdPut) | **PUT** /api/v1/components/types/{typeId} | Update component type


## `apiV1ComponentsComponentIdDelete()`

```php
apiV1ComponentsComponentIdDelete($componentId)
```

Deletes a component

Deletes a conponent

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ComponentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$componentId = 56; // int

try {
    $apiInstance->apiV1ComponentsComponentIdDelete($componentId);
} catch (Exception $e) {
    echo 'Exception when calling ComponentsApi->apiV1ComponentsComponentIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **componentId** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ComponentsComponentIdGet()`

```php
apiV1ComponentsComponentIdGet($componentId): \SLIS\Adapter\Tanss\Model\InlineResponse20084
```

Gets a component by id

Fetches a component by a given id

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ComponentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$componentId = 56; // int

try {
    $result = $apiInstance->apiV1ComponentsComponentIdGet($componentId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ComponentsApi->apiV1ComponentsComponentIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **componentId** | **int**|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20084**](../Model/InlineResponse20084.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ComponentsComponentIdPut()`

```php
apiV1ComponentsComponentIdPut($componentId, $body): \SLIS\Adapter\Tanss\Model\InlineResponse20139
```

Updates a component

Updates a component

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ComponentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$componentId = 56; // int
$body = new \stdClass; // object | component object to be saved

try {
    $result = $apiInstance->apiV1ComponentsComponentIdPut($componentId, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ComponentsApi->apiV1ComponentsComponentIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **componentId** | **int**|  |
 **body** | **object**| component object to be saved |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20139**](../Model/InlineResponse20139.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ComponentsPost()`

```php
apiV1ComponentsPost($body): \SLIS\Adapter\Tanss\Model\InlineResponse20139
```

Creates a component

Creates a component

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ComponentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \stdClass; // object | component object to be saved

try {
    $result = $apiInstance->apiV1ComponentsPost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ComponentsApi->apiV1ComponentsPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **object**| component object to be saved |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20139**](../Model/InlineResponse20139.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ComponentsPut()`

```php
apiV1ComponentsPut($uNKNOWNBASETYPE): \SLIS\Adapter\Tanss\Model\InlineResponse20085
```

Gets a list of components

Gets a list of components

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ComponentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$uNKNOWNBASETYPE = new \SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE(); // \SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE | query parameters for the request

try {
    $result = $apiInstance->apiV1ComponentsPut($uNKNOWNBASETYPE);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ComponentsApi->apiV1ComponentsPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uNKNOWNBASETYPE** | [**\SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)| query parameters for the request |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20085**](../Model/InlineResponse20085.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ComponentsTypesGet()`

```php
apiV1ComponentsTypesGet(): \SLIS\Adapter\Tanss\Model\InlineResponse20086
```

Gets a list of component types

Gets a list of component types

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ComponentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiV1ComponentsTypesGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ComponentsApi->apiV1ComponentsTypesGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20086**](../Model/InlineResponse20086.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ComponentsTypesPost()`

```php
apiV1ComponentsTypesPost($body): \SLIS\Adapter\Tanss\Model\InlineResponse20141
```

Create component type

Creates a new component type

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ComponentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \stdClass; // object | component type to be saved

try {
    $result = $apiInstance->apiV1ComponentsTypesPost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ComponentsApi->apiV1ComponentsTypesPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **object**| component type to be saved |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20141**](../Model/InlineResponse20141.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ComponentsTypesTypeIdDelete()`

```php
apiV1ComponentsTypesTypeIdDelete($typeId)
```

Delete component type

Delete a component type

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ComponentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$typeId = 56; // int | type id to be deleted

try {
    $apiInstance->apiV1ComponentsTypesTypeIdDelete($typeId);
} catch (Exception $e) {
    echo 'Exception when calling ComponentsApi->apiV1ComponentsTypesTypeIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **typeId** | **int**| type id to be deleted |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ComponentsTypesTypeIdPut()`

```php
apiV1ComponentsTypesTypeIdPut($typeId, $body): \SLIS\Adapter\Tanss\Model\InlineResponse20141
```

Update component type

Updates a component type

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ComponentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$typeId = 56; // int | type id to be updated
$body = new \stdClass; // object | component type to be saved

try {
    $result = $apiInstance->apiV1ComponentsTypesTypeIdPut($typeId, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ComponentsApi->apiV1ComponentsTypesTypeIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **typeId** | **int**| type id to be updated |
 **body** | **object**| component type to be saved |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20141**](../Model/InlineResponse20141.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

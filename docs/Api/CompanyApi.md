# SLIS\Adapter\Tanss\CompanyApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1CompaniesPost()**](CompanyApi.md#apiV1CompaniesPost) | **POST** /api/v1/companies | Creates a new company


## `apiV1CompaniesPost()`

```php
apiV1CompaniesPost($uNKNOWNBASETYPE): \SLIS\Adapter\Tanss\Model\InlineResponse20143
```

Creates a new company

Creates a new company in TANSS.  If the company shall be a \"personal customer\", you must also specify an employee object on the field \"personalCustomerEmployee\".

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CompanyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$uNKNOWNBASETYPE = new \SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE(); // \SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE | company object to be saved

try {
    $result = $apiInstance->apiV1CompaniesPost($uNKNOWNBASETYPE);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyApi->apiV1CompaniesPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uNKNOWNBASETYPE** | [**\SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)| company object to be saved |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20143**](../Model/InlineResponse20143.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

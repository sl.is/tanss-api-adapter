# SLIS\Adapter\Tanss\ChatsApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1ChatsChatIdGet()**](ChatsApi.md#apiV1ChatsChatIdGet) | **GET** /api/v1/chats/{chatId} | Gets a chat
[**apiV1ChatsCloseChatIdPost()**](ChatsApi.md#apiV1ChatsCloseChatIdPost) | **POST** /api/v1/chats/close/{chatId} | Closes a chat
[**apiV1ChatsCloseChatIdPut()**](ChatsApi.md#apiV1ChatsCloseChatIdPut) | **PUT** /api/v1/chats/close/{chatId} | Accept/decline close request
[**apiV1ChatsCloseRequestsGet()**](ChatsApi.md#apiV1ChatsCloseRequestsGet) | **GET** /api/v1/chats/closeRequests | Gets chat close requests
[**apiV1ChatsMessagesPost()**](ChatsApi.md#apiV1ChatsMessagesPost) | **POST** /api/v1/chats/messages | Creates a new chat message
[**apiV1ChatsParticipantsDelete()**](ChatsApi.md#apiV1ChatsParticipantsDelete) | **DELETE** /api/v1/chats/participants | Deletes a participant
[**apiV1ChatsParticipantsPost()**](ChatsApi.md#apiV1ChatsParticipantsPost) | **POST** /api/v1/chats/participants | Adds a participant
[**apiV1ChatsPost()**](ChatsApi.md#apiV1ChatsPost) | **POST** /api/v1/chats | Creates a new chat
[**apiV1ChatsPut()**](ChatsApi.md#apiV1ChatsPut) | **PUT** /api/v1/chats | Get a list of chats
[**apiV1ChatsReOpenChatIdPost()**](ChatsApi.md#apiV1ChatsReOpenChatIdPost) | **POST** /api/v1/chats/reOpen/{chatId} | re-opens a chat


## `apiV1ChatsChatIdGet()`

```php
apiV1ChatsChatIdGet($chatId, $withMessages): \SLIS\Adapter\Tanss\Model\InlineResponse20114
```

Gets a chat

Gets infos for a given chat (inlcuding messages, logs and participants)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ChatsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$chatId = 56; // int | Id of the chat to be fetched
$withMessages = True; // bool | If \"false\" is given here, no messages will be loaded

try {
    $result = $apiInstance->apiV1ChatsChatIdGet($chatId, $withMessages);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChatsApi->apiV1ChatsChatIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **chatId** | **int**| Id of the chat to be fetched |
 **withMessages** | **bool**| If \&quot;false\&quot; is given here, no messages will be loaded | [optional]

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20114**](../Model/InlineResponse20114.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ChatsCloseChatIdPost()`

```php
apiV1ChatsCloseChatIdPost($chatId): \SLIS\Adapter\Tanss\Model\InlineResponse20118
```

Closes a chat

Closes a chat. If the chat is not created by yourself, a close requests is create at first

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ChatsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$chatId = 56; // int | Id of the chat

try {
    $result = $apiInstance->apiV1ChatsCloseChatIdPost($chatId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChatsApi->apiV1ChatsCloseChatIdPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **chatId** | **int**| Id of the chat |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20118**](../Model/InlineResponse20118.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ChatsCloseChatIdPut()`

```php
apiV1ChatsCloseChatIdPut($chatId, $accept): \SLIS\Adapter\Tanss\Model\InlineResponse20117
```

Accept/decline close request

This routes accepts or declines a chat closing request.  This can only be done for own chats (created by yourself)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ChatsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$chatId = 56; // int | Id of the chat
$accept = True; // bool | true = accept cloe request / false = decline close request

try {
    $result = $apiInstance->apiV1ChatsCloseChatIdPut($chatId, $accept);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChatsApi->apiV1ChatsCloseChatIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **chatId** | **int**| Id of the chat |
 **accept** | **bool**| true &#x3D; accept cloe request / false &#x3D; decline close request |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20117**](../Model/InlineResponse20117.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ChatsCloseRequestsGet()`

```php
apiV1ChatsCloseRequestsGet(): \SLIS\Adapter\Tanss\Model\InlineResponse20045
```

Gets chat close requests

Return the following inforations: * Which chats were closed by another employee * Which chats were closed and had unread chat messages

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ChatsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiV1ChatsCloseRequestsGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChatsApi->apiV1ChatsCloseRequestsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20045**](../Model/InlineResponse20045.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ChatsMessagesPost()`

```php
apiV1ChatsMessagesPost($inlineObject16): \SLIS\Adapter\Tanss\Model\InlineResponse20115
```

Creates a new chat message

Creates a new chat message. The message must at least contain the chat id and a content.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ChatsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject16 = new \SLIS\Adapter\Tanss\Model\InlineObject16(); // \SLIS\Adapter\Tanss\Model\InlineObject16

try {
    $result = $apiInstance->apiV1ChatsMessagesPost($inlineObject16);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChatsApi->apiV1ChatsMessagesPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject16** | [**\SLIS\Adapter\Tanss\Model\InlineObject16**](../Model/InlineObject16.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20115**](../Model/InlineResponse20115.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ChatsParticipantsDelete()`

```php
apiV1ChatsParticipantsDelete($chatId, $employeeId, $departmentId)
```

Deletes a participant

Deletes a participant (employee or department) from a chat

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ChatsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$chatId = 56; // int | Id of the chat
$employeeId = 56; // int | Either the id of the employee ...
$departmentId = 56; // int | ... or the id of the department

try {
    $apiInstance->apiV1ChatsParticipantsDelete($chatId, $employeeId, $departmentId);
} catch (Exception $e) {
    echo 'Exception when calling ChatsApi->apiV1ChatsParticipantsDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **chatId** | **int**| Id of the chat |
 **employeeId** | **int**| Either the id of the employee ... |
 **departmentId** | **int**| ... or the id of the department | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ChatsParticipantsPost()`

```php
apiV1ChatsParticipantsPost($inlineObject17): \SLIS\Adapter\Tanss\Model\InlineResponse20116
```

Adds a participant

Adds a participant (employee or department) to a chat

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ChatsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject17 = new \SLIS\Adapter\Tanss\Model\InlineObject17(); // \SLIS\Adapter\Tanss\Model\InlineObject17

try {
    $result = $apiInstance->apiV1ChatsParticipantsPost($inlineObject17);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChatsApi->apiV1ChatsParticipantsPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject17** | [**\SLIS\Adapter\Tanss\Model\InlineObject17**](../Model/InlineObject17.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20116**](../Model/InlineResponse20116.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ChatsPost()`

```php
apiV1ChatsPost($uNKNOWNBASETYPE): \SLIS\Adapter\Tanss\Model\InlineResponse20114
```

Creates a new chat

Creates a new chat in TANSS. The chat must contain at least one participant and one message

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ChatsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$uNKNOWNBASETYPE = new \SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE(); // \SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE | chat object to be saved

try {
    $result = $apiInstance->apiV1ChatsPost($uNKNOWNBASETYPE);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChatsApi->apiV1ChatsPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uNKNOWNBASETYPE** | [**\SLIS\Adapter\Tanss\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)| chat object to be saved |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20114**](../Model/InlineResponse20114.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ChatsPut()`

```php
apiV1ChatsPut($inlineObject15): \SLIS\Adapter\Tanss\Model\InlineResponse20043
```

Get a list of chats

Retrieves a list of chats from the database, using misc. filter settings

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ChatsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject15 = new \SLIS\Adapter\Tanss\Model\InlineObject15(); // \SLIS\Adapter\Tanss\Model\InlineObject15

try {
    $result = $apiInstance->apiV1ChatsPut($inlineObject15);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChatsApi->apiV1ChatsPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject15** | [**\SLIS\Adapter\Tanss\Model\InlineObject15**](../Model/InlineObject15.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20043**](../Model/InlineResponse20043.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1ChatsReOpenChatIdPost()`

```php
apiV1ChatsReOpenChatIdPost($chatId): \SLIS\Adapter\Tanss\Model\InlineResponse20119
```

re-opens a chat

Re-opens a chat. Can only be done for chats with access and closed chats

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\ChatsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$chatId = 56; // int | Id of the chat

try {
    $result = $apiInstance->apiV1ChatsReOpenChatIdPost($chatId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChatsApi->apiV1ChatsReOpenChatIdPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **chatId** | **int**| Id of the chat |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20119**](../Model/InlineResponse20119.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

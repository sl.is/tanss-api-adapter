# # InlineResponse20062ContentAllOfAssignmentInfos

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rank** | **int** | rank (position) of the checklist | [optional]
**completedByEmployeeId** | **int** | if checklist was completed by a process, id of the employee who has completed the checklist is given here | [optional]
**completedOnDate** | **int** | if checklist was completed by a process, defines the date (timestamp) | [optional]
**completedReason** | **string** | if checklist was completed by a process, a reason (text) could be given in the process event | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

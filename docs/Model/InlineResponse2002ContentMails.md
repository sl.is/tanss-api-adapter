# # InlineResponse2002ContentMails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the mail | [optional]
**senderId** | **int** | id of the mail sender (if a tanss employee could be matches) | [optional]
**senderName** | **string** | name of the sender | [optional]
**senderEmail** | **string** | e-Mail of the sender | [optional]
**subject** | **string** | subject of the mail | [optional]
**bodyHtml** | **string** | html body | [optional]
**bodyPlain** | **string** | plain body | [optional]
**plainTextOnly** | **bool** | true, if mail was sent \&quot;only plain\&quot; | [optional]
**inbound** | **bool** | true, if the mail was incoming | [optional]
**internal** | **bool** | true, if the mail is \&quot;internal\&quot; only | [optional]
**receivers** | [**\SLIS\Adapter\Tanss\Model\InlineResponse2002ContentReceivers[]**](InlineResponse2002ContentReceivers.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

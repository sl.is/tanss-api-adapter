# # InlineResponse20011Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**employeeId** | **int** | id of the TANSS employee | [optional]
**username** | **string** | identifier string of the TANSS employee | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

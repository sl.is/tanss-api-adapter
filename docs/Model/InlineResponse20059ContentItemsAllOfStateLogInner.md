# # InlineResponse20059ContentItemsAllOfStateLogInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | **int** | Timestamp of this log entry | [optional]
**state** | **object** |  | [optional]
**infoText** | **string** | If a info was given, it will be shown here | [optional]
**employeeId** | **int** | This employee has done the state change | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

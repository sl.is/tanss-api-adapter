# # ApiV1TanssEventsRulesAssignments

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**linkType** | **string** | Enum representing the \&quot;link type\&quot; of an assignment. The id is given in the field \&quot;linkId\&quot; | [optional]
**linkId** | **int** | id of the assignment | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineObject18

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the erp selection | [optional]
**variableName** | **string** | variable name of the erp selection.  This name is used in the offer template within the {ERP:...} syntax | [optional]
**prompt** | **string** | This prompt will be shown when a user shall specify some material within an offer. | [optional]
**mats** | [**InlineResponse20048ContentItemsAllOfAllOfErpSelectionsItemsMatsItemsAllOf[]**](InlineResponse20048ContentItemsAllOfAllOfErpSelectionsItemsMatsItemsAllOf.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

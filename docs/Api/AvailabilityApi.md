# SLIS\Adapter\Tanss\AvailabilityApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1AvailabilityPost()**](AvailabilityApi.md#apiV1AvailabilityPost) | **POST** /api/v1/availability | Fetches availability infos


## `apiV1AvailabilityPost()`

```php
apiV1AvailabilityPost($employeeIds): \SLIS\Adapter\Tanss\Model\InlineResponse20053
```

Fetches availability infos

This requests fetches information about employee availability

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\AvailabilityApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$employeeIds = 'employeeIds_example'; // string | Ids of the employees (comma separated)

try {
    $result = $apiInstance->apiV1AvailabilityPost($employeeIds);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AvailabilityApi->apiV1AvailabilityPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **employeeIds** | **string**| Ids of the employees (comma separated) |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20053**](../Model/InlineResponse20053.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

# # InlineResponse20079ContentAllOfDevicesInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the periphery | [optional]
**companyId** | **int** | id of the assigned company | [optional]
**date** | **int** | date when the device was purchased (or entered into the system) | [optional]
**peripheryTypeId** | **int** | id of the periphery type | [optional]
**manufacturerId** | **int** | id of the device manufacturer | [optional]
**type** | **string** | type of the periphery. This is mostly used to \&quot;describe\&quot; the periphery in lists | [optional]
**location** | **string** | location / room of this device | [optional]
**remark** | **string** | remarks for this component | [optional]
**internalRemark** | **string** | internal remarks of the device (can contain passwords), which only is seen by technicians or freelancers with the given permission | [optional]
**serialNumber** | **string** | serial number of the device | [optional]
**inventoryNumber** | **string** | inventory number of this device | [optional]
**version** | **string** | infos of the version of this device | [optional]
**active** | **bool** | true, if the device is active | [optional]
**employeeId** | **int** | id of the assigned employee | [optional]
**pcId** | **int** | if the device is built into this pc, a pc id is given here | [optional]
**billingNumber** | **string** | infos about the billing number which this component was purchased | [optional]
**articleNumber** | **string** | article number of the component | [optional]
**storageId** | **int** | id of the storage, the component is contained in | [optional]
**purchasePrice** | **float** | purchase price of this component | [optional]
**sellingPrice** | **float** | selling price of this device | [optional]
**ownageType** | **object** |  | [optional]
**name** | **string** | hostname | [optional]
**description** | **string** | misc. description infos for this device | [optional]
**manufacturerNumber** | **string** | manufacturer number of this device | [optional]
**fields** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20079ContentAllOfDevicesItemsFieldsInner[]**](InlineResponse20079ContentAllOfDevicesItemsFieldsInner.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineObject26

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**itemId** | **int** | id of the checlist item that will be checked | [optional]
**checklistId** | **int** | id of the checklist which this item is in | [optional]
**mainChecklistId** | **int** | if the item is part of an \&quot;included\&quot; checklist, you must specify the \&quot;main\&quot; checklist as well | [optional]
**linkTypeId** | **int** | linktype of the assignment (i.e. 11 for ticket) | [optional]
**linkId** | **int** | link id of the assignment (if assigned to a ticket then the ticket id goes here) | [optional]
**value** | **int** | 1 &#x3D; check / 0 &#x3D; uncheck | [optional]
**multiSelectId** | **int** | if an multiselect option is checked, give the multiselect option id here | [optional]
**vars** | **array<string,string>** | if vars are needed for checking this field, then give these here | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineResponse20111ContentErrors

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**employeeId** | **int** | id of the employee | [optional]
**date** | **\DateTime** | day when the error occured | [optional]
**text** | **string** | key/identifier of the error message | [optional]
**localizedText** | **string** | localized text of the error message | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

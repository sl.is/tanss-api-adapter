# # InlineResponse20068ContentTicketTypes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**panelId** | **int** | Panel id | [optional]
**typeId** | **int** | Type id | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

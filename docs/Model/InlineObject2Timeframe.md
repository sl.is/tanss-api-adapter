# # InlineObject2Timeframe

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from** | **int** | Unix timestamp of the \&quot;from\&quot; date | [optional]
**to** | **int** | Unix timestamp of the \&quot;to\&quot; date | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

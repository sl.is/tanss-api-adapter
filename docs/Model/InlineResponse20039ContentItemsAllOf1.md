# # InlineResponse20039ContentItemsAllOf1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**changesRequested** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20036Content[]**](InlineResponse20036Content.md) | if any change requests are present for this user / day, they are given here | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

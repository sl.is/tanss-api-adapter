# # InlineResponse20061ContentAnticipatedCallbacks

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fromEmployeeId** | **int** | id of the employee who waits for this callback | [optional]
**phoneNumber** | **string** | phone number of this callback | [optional]
**date** | **int** | date when the anticipated callback was entered | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# SLIS\Adapter\Tanss\CallsApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiCallsV1EmployeeAssignmentDelete()**](CallsApi.md#apiCallsV1EmployeeAssignmentDelete) | **DELETE** /api/calls/v1/employeeAssignment | Deletes an employee assignment
[**apiCallsV1EmployeeAssignmentGet()**](CallsApi.md#apiCallsV1EmployeeAssignmentGet) | **GET** /api/calls/v1/employeeAssignment | Get all employee assignments
[**apiCallsV1EmployeeAssignmentPost()**](CallsApi.md#apiCallsV1EmployeeAssignmentPost) | **POST** /api/calls/v1/employeeAssignment | Creates a new employee assignment
[**apiCallsV1IdGet()**](CallsApi.md#apiCallsV1IdGet) | **GET** /api/calls/v1/{id} | Get phone call by id
[**apiCallsV1IdPut()**](CallsApi.md#apiCallsV1IdPut) | **PUT** /api/calls/v1/{id} | Update phone call
[**apiCallsV1NotificationPost()**](CallsApi.md#apiCallsV1NotificationPost) | **POST** /api/calls/v1/notification | Creates a call notification
[**apiCallsV1Post()**](CallsApi.md#apiCallsV1Post) | **POST** /api/calls/v1 | Creates/imports a phone call into the database
[**apiCallsV1Put()**](CallsApi.md#apiCallsV1Put) | **PUT** /api/calls/v1 | Get a list of phone calls


## `apiCallsV1EmployeeAssignmentDelete()`

```php
apiCallsV1EmployeeAssignmentDelete($body)
```

Deletes an employee assignment

Deletes an assignment between an \"idString\" and a TANSS employee id from the database

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CallsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \stdClass; // object | Phone employee assignment to be deleted

try {
    $apiInstance->apiCallsV1EmployeeAssignmentDelete($body);
} catch (Exception $e) {
    echo 'Exception when calling CallsApi->apiCallsV1EmployeeAssignmentDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **object**| Phone employee assignment to be deleted |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiCallsV1EmployeeAssignmentGet()`

```php
apiCallsV1EmployeeAssignmentGet(): \SLIS\Adapter\Tanss\Model\InlineResponse20011
```

Get all employee assignments

Get all assignments between idStrings and TANSS employees

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CallsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiCallsV1EmployeeAssignmentGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CallsApi->apiCallsV1EmployeeAssignmentGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20011**](../Model/InlineResponse20011.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiCallsV1EmployeeAssignmentPost()`

```php
apiCallsV1EmployeeAssignmentPost($body): \SLIS\Adapter\Tanss\Model\InlineResponse2013
```

Creates a new employee assignment

Create a new assignment between an \"idString\" and a TANSS employee id

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CallsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \stdClass; // object | Phone employee assignment

try {
    $result = $apiInstance->apiCallsV1EmployeeAssignmentPost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CallsApi->apiCallsV1EmployeeAssignmentPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **object**| Phone employee assignment |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2013**](../Model/InlineResponse2013.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiCallsV1IdGet()`

```php
apiCallsV1IdGet($id): \SLIS\Adapter\Tanss\Model\InlineResponse2012
```

Get phone call by id

Gets a specific phone call from the database based on the phone call id

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CallsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | Id of the phone call to be fetched

try {
    $result = $apiInstance->apiCallsV1IdGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CallsApi->apiCallsV1IdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Id of the phone call to be fetched |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2012**](../Model/InlineResponse2012.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiCallsV1IdPut()`

```php
apiCallsV1IdPut($id, $body): \SLIS\Adapter\Tanss\Model\InlineResponse2012
```

Update phone call

This api call updates an existing phone call

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CallsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | Id of the phone call to be updated
$body = new \stdClass; // object | Updated phone call object

try {
    $result = $apiInstance->apiCallsV1IdPut($id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CallsApi->apiCallsV1IdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Id of the phone call to be updated |
 **body** | **object**| Updated phone call object |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2012**](../Model/InlineResponse2012.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiCallsV1NotificationPost()`

```php
apiCallsV1NotificationPost($body): \SLIS\Adapter\Tanss\Model\InlineResponse2012
```

Creates a call notification

This api call is used to generate a notification for an incoming or outgoing call.  This is used to display a pop-up to the given participants.  Important: The node.js socket server and a proper configuration is required (TANSS OSK values \"socket.*\")

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CallsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \stdClass; // object | call object for this notification

try {
    $result = $apiInstance->apiCallsV1NotificationPost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CallsApi->apiCallsV1NotificationPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **object**| call object for this notification |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2012**](../Model/InlineResponse2012.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiCallsV1Post()`

```php
apiCallsV1Post($inlineObject3): \SLIS\Adapter\Tanss\Model\InlineResponse2012
```

Creates/imports a phone call into the database

This api call is used to import a phone call into the database.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CallsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject3 = new \SLIS\Adapter\Tanss\Model\InlineObject3(); // \SLIS\Adapter\Tanss\Model\InlineObject3

try {
    $result = $apiInstance->apiCallsV1Post($inlineObject3);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CallsApi->apiCallsV1Post: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject3** | [**\SLIS\Adapter\Tanss\Model\InlineObject3**](../Model/InlineObject3.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2012**](../Model/InlineResponse2012.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiCallsV1Put()`

```php
apiCallsV1Put($inlineObject2): \SLIS\Adapter\Tanss\Model\InlineResponse2008
```

Get a list of phone calls

Retrieves a list of phone calls from the database, using misc. filter settings

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CallsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject2 = new \SLIS\Adapter\Tanss\Model\InlineObject2(); // \SLIS\Adapter\Tanss\Model\InlineObject2

try {
    $result = $apiInstance->apiCallsV1Put($inlineObject2);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CallsApi->apiCallsV1Put: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject2** | [**\SLIS\Adapter\Tanss\Model\InlineObject2**](../Model/InlineObject2.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2008**](../Model/InlineResponse2008.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

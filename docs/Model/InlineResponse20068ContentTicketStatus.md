# # InlineResponse20068ContentTicketStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**panelId** | **int** | Panel id | [optional]
**statusId** | **int** | Status id | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineResponse20146ContentContentSupport

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the support | [optional]
**companyId** | **int** | id of the company | [optional]
**companyName** | **string** | name of the company | [optional]
**employeeId** | **int** | id of the technician | [optional]
**employeeName** | **string** | name of the technician | [optional]
**location** | **string** | Defines, where a support takes place | [optional]
**date** | **int** | date of the support as timestamp | [optional]
**dateFormatted** | **string** | formatted date of the support | [optional]
**duration** | **int** | duration in minutes | [optional]
**typeId** | **int** | id of the support type | [optional]
**typeName** | **string** | name of the support type | [optional]
**text** | **string** | text(description of the support | [optional]
**internal** | **bool** | true if it&#39;s an internal support | [optional]
**planningType** | **object** |  | [optional]
**planningTypeName** | **string** | name of the planning typ (i.e. \&quot;Getätigte Leistung\&quot;, \&quot;Termin\&quot;) | [optional]
**outlook** | **bool** | true if it&#39;s an outlook appointment | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineResponse403

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error** | [**\SLIS\Adapter\Tanss\Model\InlineResponse403Error**](InlineResponse403Error.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

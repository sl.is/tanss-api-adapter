# # InlineResponse20084Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the pc / server | [optional]
**inventoryNumber** | **string** | inventory number of this component | [optional]
**componentTypeId** | **int** | id of the component type | [optional]
**pcId** | **int** | the component is built into this pc (0 if the component ist not attached to a pc) | [optional]
**peripheryId** | **int** | if the component is built into a periphery, here the id is given | [optional]
**manufacturerId** | **int** | id of the components manufacturer | [optional]
**type** | **string** | type of the component. This is mostly used to \&quot;describe\&quot; the component in lists | [optional]
**serialNumber** | **string** | serial number of the component | [optional]
**megabytes** | **int** | capcity (in megbytes), which is used to determine the size of ram or HDDs | [optional]
**hddTypeId** | **int** | id of the HDD type | [optional]
**scsiId** | **string** | SCSI id | [optional]
**onBoard** | **bool** | if true, the component is \&quot;onboard\&quot; | [optional]
**companyId** | **int** | id of the company, which this component is assigned to. Only relevant if the component is not built into a pc! | [optional]
**remark** | **string** | remarks for this component | [optional]
**active** | **bool** | true, if the component is active | [optional]
**date** | **int** | date when the component was purchased (or entered into the system) | [optional]
**billingNumber** | **string** | infos about the billing number which this component was purchased | [optional]
**articleNumber** | **string** | article number of the component | [optional]
**storageId** | **int** | id of the storage, the component is contained in | [optional]
**purchasePrice** | **float** | purchase price of this component | [optional]
**sellingPrice** | **float** | selling price of this component | [optional]
**description** | **string** | misc. description infos for this component | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

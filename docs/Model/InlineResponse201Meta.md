# # InlineResponse201Meta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**linkedEntities** | **object** |  | [optional]
**properties** | [**\SLIS\Adapter\Tanss\Model\InlineResponse201MetaProperties**](InlineResponse201MetaProperties.md) |  | [optional]
**listProperties** | **object** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

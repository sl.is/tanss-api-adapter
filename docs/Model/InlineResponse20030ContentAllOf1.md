# # InlineResponse20030ContentAllOf1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**internal** | **bool** | if true, represents an \&quot;internal\&quot; department (&#x3D; used for ticket or technician assignment) | [optional]
**customerLock** | **bool** | if true, department is \&quot;locked\&quot; for customers by default | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineResponse20068Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**panels** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20068ContentPanels[]**](InlineResponse20068ContentPanels.md) | Array of panels | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineObject5

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**remoteMaintenanceId** | **string** | \&quot;Identifier\&quot; used by the remote systems to identify this remote support | [optional]
**typeId** | **int** | \&quot;Id\&quot; of the external remote support system, which is defined in the TANSS administration (\&quot;Externe Fernwartungs-Anbindungen verwalten\&quot;). This field can&#39;t be set, as the Api token was created for a specific \&quot;type\&quot; and this value is fix. | [optional]
**userId** | **string** | \&quot;Identifier string\&quot; of the technician who has done this remote support. This can be used to \&quot;translate\&quot; the identifier of the technician to a TANSS employee id | [optional]
**userName** | **string** | name of the technician who has done this remote support, this is mainly used for displaying issues | [optional]
**employeeId** | **int** | Id of the technician who has done this remote support (TANSS employee id). This can be omitted if the id is not known or the \&quot;userId\&quot; can be \&quot;translated\&quot; | [optional]
**deviceId** | **string** | \&quot;Identifier string\&quot; or unique name of the device the remote support was executed for. This can be used via a \&quot;translation table\&quot; to get the company and/or assignment. | [optional]
**deviceName** | **string** | Simply the name of the device the remote support was executed for. This is mainly used for displying issues. | [optional]
**companyId** | **int** | id of the company for this remote support | [optional]
**linkTypeId** | **int** | LinkTypeId of the assignment (if known) | [optional]
**linkId** | **int** | LinkId of the assignment (if known) | [optional]
**startTime** | **int** | Unix timestamp of the begin for this remote support | [optional]
**endTime** | **int** | Unix timestamp of the end for this remote support | [optional]
**comment** | **string** | (optional) comment of the remote support | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # ApiCallsV1FromPhoneNrInfos

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**foundType** | **string** | Determines how a phone number could be matched | [optional]
**items** | [**\SLIS\Adapter\Tanss\Model\ApiCallsV1FromPhoneNrInfosItems[]**](ApiCallsV1FromPhoneNrInfosItems.md) | all possible matches | [optional]
**result** | **object** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# SLIS\Adapter\Tanss\RemoteSupportsApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiRemoteSupportsV1AssignDeviceDelete()**](RemoteSupportsApi.md#apiRemoteSupportsV1AssignDeviceDelete) | **DELETE** /api/remoteSupports/v1/assignDevice | Deletes a device assignment
[**apiRemoteSupportsV1AssignDeviceGet()**](RemoteSupportsApi.md#apiRemoteSupportsV1AssignDeviceGet) | **GET** /api/remoteSupports/v1/assignDevice | Gets all device assignments
[**apiRemoteSupportsV1AssignDevicePost()**](RemoteSupportsApi.md#apiRemoteSupportsV1AssignDevicePost) | **POST** /api/remoteSupports/v1/assignDevice | Creates a device assignment
[**apiRemoteSupportsV1AssignEmployeeDelete()**](RemoteSupportsApi.md#apiRemoteSupportsV1AssignEmployeeDelete) | **DELETE** /api/remoteSupports/v1/assignEmployee | Delets a technician assignment
[**apiRemoteSupportsV1AssignEmployeeGet()**](RemoteSupportsApi.md#apiRemoteSupportsV1AssignEmployeeGet) | **GET** /api/remoteSupports/v1/assignEmployee | Gets all technician assignments
[**apiRemoteSupportsV1AssignEmployeePost()**](RemoteSupportsApi.md#apiRemoteSupportsV1AssignEmployeePost) | **POST** /api/remoteSupports/v1/assignEmployee | Creates a technician assignment
[**apiRemoteSupportsV1Post()**](RemoteSupportsApi.md#apiRemoteSupportsV1Post) | **POST** /api/remoteSupports/v1 | Creates/imports a remote support into the database
[**apiRemoteSupportsV1Put()**](RemoteSupportsApi.md#apiRemoteSupportsV1Put) | **PUT** /api/remoteSupports/v1 | Get list of remote supports
[**apiRemoteSupportsV1RemoteSupportIdDelete()**](RemoteSupportsApi.md#apiRemoteSupportsV1RemoteSupportIdDelete) | **DELETE** /api/remoteSupports/v1/{remoteSupportId} | Delete remote support
[**apiRemoteSupportsV1RemoteSupportIdGet()**](RemoteSupportsApi.md#apiRemoteSupportsV1RemoteSupportIdGet) | **GET** /api/remoteSupports/v1/{remoteSupportId} | Get remote support by id
[**apiRemoteSupportsV1RemoteSupportIdPut()**](RemoteSupportsApi.md#apiRemoteSupportsV1RemoteSupportIdPut) | **PUT** /api/remoteSupports/v1/{remoteSupportId} | Updates a remote support


## `apiRemoteSupportsV1AssignDeviceDelete()`

```php
apiRemoteSupportsV1AssignDeviceDelete($inlineObject6)
```

Deletes a device assignment

This call deletes an entry in a \"translation table\" that allows identification of a remote support to a company / assignment based of the `deviceId`

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\RemoteSupportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject6 = new \SLIS\Adapter\Tanss\Model\InlineObject6(); // \SLIS\Adapter\Tanss\Model\InlineObject6

try {
    $apiInstance->apiRemoteSupportsV1AssignDeviceDelete($inlineObject6);
} catch (Exception $e) {
    echo 'Exception when calling RemoteSupportsApi->apiRemoteSupportsV1AssignDeviceDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject6** | [**\SLIS\Adapter\Tanss\Model\InlineObject6**](../Model/InlineObject6.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiRemoteSupportsV1AssignDeviceGet()`

```php
apiRemoteSupportsV1AssignDeviceGet(): \SLIS\Adapter\Tanss\Model\InlineResponse20016
```

Gets all device assignments

This call gets all device assignments of the given remote support \"type\". A list is returned containing all information how to match a `deviceId` for the given remote support type to a TANSS company / assignment. The \"type\" is fix and is stored in the used Api token.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\RemoteSupportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiRemoteSupportsV1AssignDeviceGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RemoteSupportsApi->apiRemoteSupportsV1AssignDeviceGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20016**](../Model/InlineResponse20016.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiRemoteSupportsV1AssignDevicePost()`

```php
apiRemoteSupportsV1AssignDevicePost($body): \SLIS\Adapter\Tanss\Model\InlineResponse2015
```

Creates a device assignment

This call creates an entry in a \"translation table\" that allows identification of a remote support to a company / assignment based of the `deviceId`

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\RemoteSupportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \stdClass; // object | object containing the assignment of \"`deviceId` -> `company` / `linkTypeId` / `linkId`\"

try {
    $result = $apiInstance->apiRemoteSupportsV1AssignDevicePost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RemoteSupportsApi->apiRemoteSupportsV1AssignDevicePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **object**| object containing the assignment of \&quot;&#x60;deviceId&#x60; -&gt; &#x60;company&#x60; / &#x60;linkTypeId&#x60; / &#x60;linkId&#x60;\&quot; |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2015**](../Model/InlineResponse2015.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiRemoteSupportsV1AssignEmployeeDelete()`

```php
apiRemoteSupportsV1AssignEmployeeDelete($inlineObject7)
```

Delets a technician assignment

This call deletes an entry in a \"translation table\" that allows identification of a TANSS employee based on a `userId` of a remote support

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\RemoteSupportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject7 = new \SLIS\Adapter\Tanss\Model\InlineObject7(); // \SLIS\Adapter\Tanss\Model\InlineObject7

try {
    $apiInstance->apiRemoteSupportsV1AssignEmployeeDelete($inlineObject7);
} catch (Exception $e) {
    echo 'Exception when calling RemoteSupportsApi->apiRemoteSupportsV1AssignEmployeeDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject7** | [**\SLIS\Adapter\Tanss\Model\InlineObject7**](../Model/InlineObject7.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiRemoteSupportsV1AssignEmployeeGet()`

```php
apiRemoteSupportsV1AssignEmployeeGet(): \SLIS\Adapter\Tanss\Model\InlineResponse20017
```

Gets all technician assignments

This call gets all employee assignments for the current remote support \"type\". The \"type\" is fix and is stored in the used Api token.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\RemoteSupportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiRemoteSupportsV1AssignEmployeeGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RemoteSupportsApi->apiRemoteSupportsV1AssignEmployeeGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20017**](../Model/InlineResponse20017.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiRemoteSupportsV1AssignEmployeePost()`

```php
apiRemoteSupportsV1AssignEmployeePost($body): \SLIS\Adapter\Tanss\Model\InlineResponse2016
```

Creates a technician assignment

This call creates an entry in a \"translation table\" that allows identification of a TANSS employee based on a `userId` of a remote support

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\RemoteSupportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \stdClass; // object | object containing the assignment of \"`type` + `userId` -> `employeeId`\"

try {
    $result = $apiInstance->apiRemoteSupportsV1AssignEmployeePost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RemoteSupportsApi->apiRemoteSupportsV1AssignEmployeePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **object**| object containing the assignment of \&quot;&#x60;type&#x60; + &#x60;userId&#x60; -&gt; &#x60;employeeId&#x60;\&quot; |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2016**](../Model/InlineResponse2016.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiRemoteSupportsV1Post()`

```php
apiRemoteSupportsV1Post($inlineObject5): \SLIS\Adapter\Tanss\Model\InlineResponse2014
```

Creates/imports a remote support into the database

Stores/imports a remote support of an external system into the TANSS database. * if `userId` is given, the api can try to \"translate\" it to a TANSS employee (use route `/api/remoteSupports/v1/assignEmployee` to set the matchings) * if `deviceId` is given, the api can try to \"translate\" it into a TANSS company and assignment (use route `/api/remoteSupports/v1/assignDevice` to set the matchings)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\RemoteSupportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject5 = new \SLIS\Adapter\Tanss\Model\InlineObject5(); // \SLIS\Adapter\Tanss\Model\InlineObject5

try {
    $result = $apiInstance->apiRemoteSupportsV1Post($inlineObject5);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RemoteSupportsApi->apiRemoteSupportsV1Post: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject5** | [**\SLIS\Adapter\Tanss\Model\InlineObject5**](../Model/InlineObject5.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2014**](../Model/InlineResponse2014.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiRemoteSupportsV1Put()`

```php
apiRemoteSupportsV1Put($inlineObject4): \SLIS\Adapter\Tanss\Model\InlineResponse20013
```

Get list of remote supports

Gets a list of remote supports calls based on misc. filter settings

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\RemoteSupportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject4 = new \SLIS\Adapter\Tanss\Model\InlineObject4(); // \SLIS\Adapter\Tanss\Model\InlineObject4

try {
    $result = $apiInstance->apiRemoteSupportsV1Put($inlineObject4);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RemoteSupportsApi->apiRemoteSupportsV1Put: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject4** | [**\SLIS\Adapter\Tanss\Model\InlineObject4**](../Model/InlineObject4.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20013**](../Model/InlineResponse20013.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiRemoteSupportsV1RemoteSupportIdDelete()`

```php
apiRemoteSupportsV1RemoteSupportIdDelete($remoteSupportId)
```

Delete remote support

Deletes a remote support by id. This can only be done for \"external\" remote support (type >= 1000)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\RemoteSupportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$remoteSupportId = 56; // int | Id of the remote support to be deleted

try {
    $apiInstance->apiRemoteSupportsV1RemoteSupportIdDelete($remoteSupportId);
} catch (Exception $e) {
    echo 'Exception when calling RemoteSupportsApi->apiRemoteSupportsV1RemoteSupportIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **remoteSupportId** | **int**| Id of the remote support to be deleted |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiRemoteSupportsV1RemoteSupportIdGet()`

```php
apiRemoteSupportsV1RemoteSupportIdGet($remoteSupportId): \SLIS\Adapter\Tanss\Model\InlineResponse2014
```

Get remote support by id

Gets a remote support from the database (based on the id)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\RemoteSupportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$remoteSupportId = 56; // int | Id of the remote support to be fetched

try {
    $result = $apiInstance->apiRemoteSupportsV1RemoteSupportIdGet($remoteSupportId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RemoteSupportsApi->apiRemoteSupportsV1RemoteSupportIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **remoteSupportId** | **int**| Id of the remote support to be fetched |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2014**](../Model/InlineResponse2014.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiRemoteSupportsV1RemoteSupportIdPut()`

```php
apiRemoteSupportsV1RemoteSupportIdPut($remoteSupportId, $body): \SLIS\Adapter\Tanss\Model\InlineResponse2014
```

Updates a remote support

Updates a remote support by id and sets the new values

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\RemoteSupportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$remoteSupportId = 56; // int | Id of the remote support to be updated
$body = new \stdClass; // object | new values for the remote support object

try {
    $result = $apiInstance->apiRemoteSupportsV1RemoteSupportIdPut($remoteSupportId, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RemoteSupportsApi->apiRemoteSupportsV1RemoteSupportIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **remoteSupportId** | **int**| Id of the remote support to be updated |
 **body** | **object**| new values for the remote support object |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2014**](../Model/InlineResponse2014.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

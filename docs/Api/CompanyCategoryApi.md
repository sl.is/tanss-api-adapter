# SLIS\Adapter\Tanss\CompanyCategoryApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1CompanyCategoriesGet()**](CompanyCategoryApi.md#apiV1CompanyCategoriesGet) | **GET** /api/v1/companyCategories | list of categories
[**apiV1CompanyCategoriesIdDelete()**](CompanyCategoryApi.md#apiV1CompanyCategoriesIdDelete) | **DELETE** /api/v1/companyCategories/{id} | Deletes a company category
[**apiV1CompanyCategoriesIdGet()**](CompanyCategoryApi.md#apiV1CompanyCategoriesIdGet) | **GET** /api/v1/companyCategories/{id} | gets a category
[**apiV1CompanyCategoriesIdPut()**](CompanyCategoryApi.md#apiV1CompanyCategoriesIdPut) | **PUT** /api/v1/companyCategories/{id} | updates a category
[**apiV1CompanyCategoriesPost()**](CompanyCategoryApi.md#apiV1CompanyCategoriesPost) | **POST** /api/v1/companyCategories | Creates a new company category
[**apiV1CompanyCategoriesTypesGet()**](CompanyCategoryApi.md#apiV1CompanyCategoriesTypesGet) | **GET** /api/v1/companyCategories/types | list of company types
[**apiV1CompanyCategoriesTypesIdDelete()**](CompanyCategoryApi.md#apiV1CompanyCategoriesTypesIdDelete) | **DELETE** /api/v1/companyCategories/types/{id} | Deletes a company type
[**apiV1CompanyCategoriesTypesIdGet()**](CompanyCategoryApi.md#apiV1CompanyCategoriesTypesIdGet) | **GET** /api/v1/companyCategories/types/{id} | gets a company type
[**apiV1CompanyCategoriesTypesIdPut()**](CompanyCategoryApi.md#apiV1CompanyCategoriesTypesIdPut) | **PUT** /api/v1/companyCategories/types/{id} | updates a company type
[**apiV1CompanyCategoriesTypesPost()**](CompanyCategoryApi.md#apiV1CompanyCategoriesTypesPost) | **POST** /api/v1/companyCategories/types | Creates a new company type


## `apiV1CompanyCategoriesGet()`

```php
apiV1CompanyCategoriesGet(): \SLIS\Adapter\Tanss\Model\InlineResponse20097
```

list of categories

Gets a list of all categories

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CompanyCategoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiV1CompanyCategoriesGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyCategoryApi->apiV1CompanyCategoriesGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20097**](../Model/InlineResponse20097.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1CompanyCategoriesIdDelete()`

```php
apiV1CompanyCategoriesIdDelete($id)
```

Deletes a company category

Deletes a company category

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CompanyCategoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | Id of the company category

try {
    $apiInstance->apiV1CompanyCategoriesIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling CompanyCategoryApi->apiV1CompanyCategoriesIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Id of the company category |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1CompanyCategoriesIdGet()`

```php
apiV1CompanyCategoriesIdGet($id): \SLIS\Adapter\Tanss\Model\InlineResponse20152
```

gets a category

gets a single company category

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CompanyCategoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | Id of the company category

try {
    $result = $apiInstance->apiV1CompanyCategoriesIdGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyCategoryApi->apiV1CompanyCategoriesIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Id of the company category |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20152**](../Model/InlineResponse20152.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1CompanyCategoriesIdPut()`

```php
apiV1CompanyCategoriesIdPut($id, $body): \SLIS\Adapter\Tanss\Model\InlineResponse20152
```

updates a category

Updates a company category

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CompanyCategoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | Id of the company category
$body = new \stdClass; // object | company category object to be updated

try {
    $result = $apiInstance->apiV1CompanyCategoriesIdPut($id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyCategoryApi->apiV1CompanyCategoriesIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Id of the company category |
 **body** | **object**| company category object to be updated |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20152**](../Model/InlineResponse20152.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1CompanyCategoriesPost()`

```php
apiV1CompanyCategoriesPost($inlineObject35): \SLIS\Adapter\Tanss\Model\InlineResponse20152
```

Creates a new company category

Creates a company category in TANSS.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CompanyCategoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject35 = new \SLIS\Adapter\Tanss\Model\InlineObject35(); // \SLIS\Adapter\Tanss\Model\InlineObject35

try {
    $result = $apiInstance->apiV1CompanyCategoriesPost($inlineObject35);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyCategoryApi->apiV1CompanyCategoriesPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject35** | [**\SLIS\Adapter\Tanss\Model\InlineObject35**](../Model/InlineObject35.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20152**](../Model/InlineResponse20152.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1CompanyCategoriesTypesGet()`

```php
apiV1CompanyCategoriesTypesGet(): \SLIS\Adapter\Tanss\Model\InlineResponse20099
```

list of company types

Gets a list of all company types

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CompanyCategoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiV1CompanyCategoriesTypesGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyCategoryApi->apiV1CompanyCategoriesTypesGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20099**](../Model/InlineResponse20099.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1CompanyCategoriesTypesIdDelete()`

```php
apiV1CompanyCategoriesTypesIdDelete($id)
```

Deletes a company type

Deletes a company type

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CompanyCategoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | Id of the company type

try {
    $apiInstance->apiV1CompanyCategoriesTypesIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling CompanyCategoryApi->apiV1CompanyCategoriesTypesIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Id of the company type |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1CompanyCategoriesTypesIdGet()`

```php
apiV1CompanyCategoriesTypesIdGet($id): \SLIS\Adapter\Tanss\Model\InlineResponse20153
```

gets a company type

gets a single company type

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CompanyCategoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | Id of the company type

try {
    $result = $apiInstance->apiV1CompanyCategoriesTypesIdGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyCategoryApi->apiV1CompanyCategoriesTypesIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Id of the company type |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20153**](../Model/InlineResponse20153.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1CompanyCategoriesTypesIdPut()`

```php
apiV1CompanyCategoriesTypesIdPut($id, $body): \SLIS\Adapter\Tanss\Model\InlineResponse20153
```

updates a company type

Updates a company type

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CompanyCategoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | Id of the company type
$body = new \stdClass; // object | company type object to be updated

try {
    $result = $apiInstance->apiV1CompanyCategoriesTypesIdPut($id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyCategoryApi->apiV1CompanyCategoriesTypesIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Id of the company type |
 **body** | **object**| company type object to be updated |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20153**](../Model/InlineResponse20153.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1CompanyCategoriesTypesPost()`

```php
apiV1CompanyCategoriesTypesPost($body): \SLIS\Adapter\Tanss\Model\InlineResponse20153
```

Creates a new company type

Creates a company type in TANSS.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\CompanyCategoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \stdClass; // object | company ztype object to be saved

try {
    $result = $apiInstance->apiV1CompanyCategoriesTypesPost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyCategoryApi->apiV1CompanyCategoriesTypesPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **object**| company ztype object to be saved |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20153**](../Model/InlineResponse20153.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

# SLIS\Adapter\Tanss\TicketContentApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1TicketsTicketIdDocumentsDocumentIdGet()**](TicketContentApi.md#apiV1TicketsTicketIdDocumentsDocumentIdGet) | **GET** /api/v1/tickets/{ticketId}/documents/{documentId} | Gets a ticket document
[**apiV1TicketsTicketIdDocumentsGet()**](TicketContentApi.md#apiV1TicketsTicketIdDocumentsGet) | **GET** /api/v1/tickets/{ticketId}/documents | Gets all ticket documents
[**apiV1TicketsTicketIdScreenshotsGet()**](TicketContentApi.md#apiV1TicketsTicketIdScreenshotsGet) | **GET** /api/v1/tickets/{ticketId}/screenshots | Gets all ticket images
[**apiV1TicketsTicketIdScreenshotsImageIdGet()**](TicketContentApi.md#apiV1TicketsTicketIdScreenshotsImageIdGet) | **GET** /api/v1/tickets/{ticketId}/screenshots/{imageId} | Gets a ticket image
[**apiV1TicketsTicketIdUploadPost()**](TicketContentApi.md#apiV1TicketsTicketIdUploadPost) | **POST** /api/v1/tickets/{ticketId}/upload | upload a document/image


## `apiV1TicketsTicketIdDocumentsDocumentIdGet()`

```php
apiV1TicketsTicketIdDocumentsDocumentIdGet($ticketId, $documentId): \SLIS\Adapter\Tanss\Model\InlineResponse2004
```

Gets a ticket document

This call will produce a direct download link for downloading this document.  The generated url can only be used once withing 15 minutes.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketContentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ticketId = 56; // int
$documentId = 56; // int | id of the document

try {
    $result = $apiInstance->apiV1TicketsTicketIdDocumentsDocumentIdGet($ticketId, $documentId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketContentApi->apiV1TicketsTicketIdDocumentsDocumentIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketId** | **int**|  |
 **documentId** | **int**| id of the document |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2004**](../Model/InlineResponse2004.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketsTicketIdDocumentsGet()`

```php
apiV1TicketsTicketIdDocumentsGet($ticketId): \SLIS\Adapter\Tanss\Model\InlineResponse2003
```

Gets all ticket documents

Gets all the documents attached to a ticket

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketContentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ticketId = 56; // int

try {
    $result = $apiInstance->apiV1TicketsTicketIdDocumentsGet($ticketId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketContentApi->apiV1TicketsTicketIdDocumentsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketId** | **int**|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketsTicketIdScreenshotsGet()`

```php
apiV1TicketsTicketIdScreenshotsGet($ticketId): \SLIS\Adapter\Tanss\Model\InlineResponse2005
```

Gets all ticket images

Gets all the images attached to a ticket

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketContentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ticketId = 56; // int

try {
    $result = $apiInstance->apiV1TicketsTicketIdScreenshotsGet($ticketId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketContentApi->apiV1TicketsTicketIdScreenshotsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketId** | **int**|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2005**](../Model/InlineResponse2005.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketsTicketIdScreenshotsImageIdGet()`

```php
apiV1TicketsTicketIdScreenshotsImageIdGet($ticketId, $imageId): \SLIS\Adapter\Tanss\Model\InlineResponse2006
```

Gets a ticket image

This call will produce a direct download link for downloading this image.  The generated url can only be used once withing 15 minutes.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketContentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ticketId = 56; // int
$imageId = 56; // int | id of the image

try {
    $result = $apiInstance->apiV1TicketsTicketIdScreenshotsImageIdGet($ticketId, $imageId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketContentApi->apiV1TicketsTicketIdScreenshotsImageIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketId** | **int**|  |
 **imageId** | **int**| id of the image |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2006**](../Model/InlineResponse2006.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TicketsTicketIdUploadPost()`

```php
apiV1TicketsTicketIdUploadPost($ticketId, $files, $description): \SLIS\Adapter\Tanss\Model\InlineResponse2007
```

upload a document/image

This route lets you upload a document or image into a ticket.  E-mails can be uploaded as well, the system will try to parse them and store them as e-mails.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TicketContentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ticketId = 56; // int
$files = "/path/to/file.txt"; // \SplFileObject | binary data of the uploaded file(s)
$description = "/path/to/file.txt"; // \SplFileObject | description of the uploaded file(s)

try {
    $result = $apiInstance->apiV1TicketsTicketIdUploadPost($ticketId, $files, $description);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketContentApi->apiV1TicketsTicketIdUploadPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketId** | **int**|  |
 **files** | **\SplFileObject****\SplFileObject**| binary data of the uploaded file(s) | [optional]
 **description** | **\SplFileObject****\SplFileObject**| description of the uploaded file(s) | [optional]

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2007**](../Model/InlineResponse2007.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `multipart/form-data`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

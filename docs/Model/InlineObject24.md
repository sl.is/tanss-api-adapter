# # InlineObject24

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fromEmployeeId** | **int** | Defines the employee who entered the callback | [optional]
**toEmployeeId** | **int** | The callback is assigned to this employee (This person has to do the call) | [optional]
**toDepartmentId** | **int** | Callbacks can be assigned to a special department. Every employee of this department sees the callback. | [optional]
**date** | **int** | The callback was created on this date | [optional]
**companyId** | **int** | Here, the id of the company is given which has to be called back | [optional]
**companyName** | **string** | same as \&quot;companyId\&quot;, but here a string can be given (i.e. if the company wasn&#39;t created yet in TANSS) | [optional]
**employeeId** | **int** | Here, the id of the employee is given (who has to be called back) | [optional]
**employeeName** | **string** | same as \&quot;employeeId\&quot;, but here a string can be given (i.e. if the employee wasn&#39;t created yet in TANSS) | [optional]
**phoneNumber** | **string** | You must define the phone number here | [optional]
**info** | **string** | (Optional) Further information regarding the callback | [optional]
**state** | **string** | Describes a current \&quot;state\&quot; of a callback | [optional]
**priority** | **int** | priority of the callback from 1 (lowest) to 9 (highest) | [optional]
**callbackAfterTime** | **int** | If the callback has to be \&quot;later\&quot; than a specific time, give here the minimum date for the callback | [optional]
**callbackUntilTime** | **int** | If the callback has to be processed before a given date, specify this \&quot;latest\&quot; date here | [optional]
**linkTypeId** | **int** | If the callback is assigned to a ticket (or other assignment), linkType is given here | [optional]
**linkId** | **int** | If the callback is assigned to a ticket (or other assignment), the id is given here | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

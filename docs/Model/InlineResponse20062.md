# # InlineResponse20062

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | **object** |  | [optional]
**content** | [**InlineResponse20065ContentAllOf**](InlineResponse20065ContentAllOf.md) | model used to render tickets in lists | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

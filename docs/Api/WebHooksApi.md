# SLIS\Adapter\Tanss\WebHooksApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1TanssEventsRulesIdDelete()**](WebHooksApi.md#apiV1TanssEventsRulesIdDelete) | **DELETE** /api/v1/tanssEvents/rules/{id} | deletes a rule
[**apiV1TanssEventsRulesIdGet()**](WebHooksApi.md#apiV1TanssEventsRulesIdGet) | **GET** /api/v1/tanssEvents/rules/{id} | gets a rule
[**apiV1TanssEventsRulesIdPut()**](WebHooksApi.md#apiV1TanssEventsRulesIdPut) | **PUT** /api/v1/tanssEvents/rules/{id} | updates a rule
[**apiV1TanssEventsRulesPost()**](WebHooksApi.md#apiV1TanssEventsRulesPost) | **POST** /api/v1/tanssEvents/rules | creates a rule
[**apiV1TanssEventsRulesPut()**](WebHooksApi.md#apiV1TanssEventsRulesPut) | **PUT** /api/v1/tanssEvents/rules | get a list of rules
[**apiV1TanssEventsRulesTestActionPut()**](WebHooksApi.md#apiV1TanssEventsRulesTestActionPut) | **PUT** /api/v1/tanssEvents/rules/test/action | test a rule


## `apiV1TanssEventsRulesIdDelete()`

```php
apiV1TanssEventsRulesIdDelete($id)
```

deletes a rule

This route delete a \"TANSS event rule\"

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\WebHooksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | id of the rule

try {
    $apiInstance->apiV1TanssEventsRulesIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling WebHooksApi->apiV1TanssEventsRulesIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id of the rule |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TanssEventsRulesIdGet()`

```php
apiV1TanssEventsRulesIdGet($id): \SLIS\Adapter\Tanss\Model\InlineResponse20144
```

gets a rule

This route retrieve a \"TANSS event rule\"

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\WebHooksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | id of the rule

try {
    $result = $apiInstance->apiV1TanssEventsRulesIdGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebHooksApi->apiV1TanssEventsRulesIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id of the rule |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20144**](../Model/InlineResponse20144.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TanssEventsRulesIdPut()`

```php
apiV1TanssEventsRulesIdPut($id, $body): \SLIS\Adapter\Tanss\Model\InlineResponse20144
```

updates a rule

This route will update a \"TANSS event rule\"

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\WebHooksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | id of the rule
$body = new \stdClass; // object | tanss event rule object

try {
    $result = $apiInstance->apiV1TanssEventsRulesIdPut($id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebHooksApi->apiV1TanssEventsRulesIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id of the rule |
 **body** | **object**| tanss event rule object |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20144**](../Model/InlineResponse20144.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TanssEventsRulesPost()`

```php
apiV1TanssEventsRulesPost($inlineObject29): \SLIS\Adapter\Tanss\Model\InlineResponse20144
```

creates a rule

This route will create a \"TANSS event rule\", which is primarily used for webhhok notifications.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\WebHooksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject29 = new \SLIS\Adapter\Tanss\Model\InlineObject29(); // \SLIS\Adapter\Tanss\Model\InlineObject29

try {
    $result = $apiInstance->apiV1TanssEventsRulesPost($inlineObject29);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebHooksApi->apiV1TanssEventsRulesPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject29** | [**\SLIS\Adapter\Tanss\Model\InlineObject29**](../Model/InlineObject29.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20144**](../Model/InlineResponse20144.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TanssEventsRulesPut()`

```php
apiV1TanssEventsRulesPut($inlineObject28): \SLIS\Adapter\Tanss\Model\InlineResponse20087
```

get a list of rules

This route will return a list of rules, matching a filter, which is sent in the body

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\WebHooksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject28 = new \SLIS\Adapter\Tanss\Model\InlineObject28(); // \SLIS\Adapter\Tanss\Model\InlineObject28

try {
    $result = $apiInstance->apiV1TanssEventsRulesPut($inlineObject28);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebHooksApi->apiV1TanssEventsRulesPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject28** | [**\SLIS\Adapter\Tanss\Model\InlineObject28**](../Model/InlineObject28.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20087**](../Model/InlineResponse20087.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TanssEventsRulesTestActionPut()`

```php
apiV1TanssEventsRulesTestActionPut($inlineObject30): \SLIS\Adapter\Tanss\Model\InlineResponse20146
```

test a rule

This route will let you trigger an action, so you can view the result of the action (i.e. WebHook notification)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\WebHooksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject30 = new \SLIS\Adapter\Tanss\Model\InlineObject30(); // \SLIS\Adapter\Tanss\Model\InlineObject30

try {
    $result = $apiInstance->apiV1TanssEventsRulesTestActionPut($inlineObject30);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebHooksApi->apiV1TanssEventsRulesTestActionPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject30** | [**\SLIS\Adapter\Tanss\Model\InlineObject30**](../Model/InlineObject30.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20146**](../Model/InlineResponse20146.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

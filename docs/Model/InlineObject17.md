# # InlineObject17

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chatId** | **int** | id of the corresponding chat id | [optional]
**employeeId** | **int** | if the participant is an employee, the id is given here | [optional]
**departmentId** | **int** | if the participant is an department, the id is given here | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

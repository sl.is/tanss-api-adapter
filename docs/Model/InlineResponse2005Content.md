# # InlineResponse2005Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the image | [optional]
**ticketId** | **int** | id of the assigned ticket | [optional]
**fileName** | **string** | original filename of the image | [optional]
**employeeId** | **int** | id of the employee who uploaded this image | [optional]
**date** | **int** | date when the image was uploaded | [optional]
**internal** | **bool** | true if the image is internal (customers won&#39;t see it) | [optional]
**description** | **string** | (optional) description of the image | [optional]
**image** | **string** | base64 encoded string, representing the image data | [optional]
**type** | **string** | type / extension of the image (i.e. png, jpg) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

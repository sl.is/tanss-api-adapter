# # InlineResponse20047Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the offer template | [optional]
**name** | **string** | name of the offer template | [optional]
**creationDate** | **int** | timestamp of the creation of this offer template | [optional]
**createdByEmployeeId** | **int** | id of the employee who has created this offer template | [optional]
**previousId** | **int** | if the offer template is a \&quot;newer\&quot; version of another template, the old template (pervious id) goes here | [optional]
**validInDays** | **int** | you can define here how long (in days) a generated offer will be valid | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

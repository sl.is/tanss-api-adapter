# # InlineResponse20051ContentItemsAllOfWorkflowsItemsAllOfTokensInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of this token | [optional]
**date** | **int** | date, when the token was generated | [optional]
**expires** | **int** | date, when the token will expire | [optional]
**type** | **string** | describes the type of a token | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

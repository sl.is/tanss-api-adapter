# # InlineResponse20146ContentContentMail

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the mail | [optional]
**senderName** | **string** | name of the sender | [optional]
**senderEMail** | **string** | email of the sender | [optional]
**subject** | **string** | mail subject | [optional]
**bodyPlain** | **string** | plaintext body of the mail | [optional]
**inbound** | **bool** | true if it&#39;s an inbound E-Mail | [optional]
**internal** | **bool** | true if it&#39;s an internal E-Mail | [optional]
**receiverEMails** | **string[]** | list of E-Mail receivers | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

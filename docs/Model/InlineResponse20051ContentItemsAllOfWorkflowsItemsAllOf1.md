# # InlineResponse20051ContentItemsAllOfWorkflowsItemsAllOf1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tokens** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20051ContentItemsAllOfWorkflowsItemsAllOfTokensInner[]**](InlineResponse20051ContentItemsAllOfWorkflowsItemsAllOfTokensInner.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

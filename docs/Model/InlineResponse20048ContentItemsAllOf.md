# # InlineResponse20048ContentItemsAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vars** | [**\SLIS\Adapter\Tanss\Model\InlineResponse20048ContentItemsAllOfVarsInner[]**](InlineResponse20048ContentItemsAllOfVarsInner.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineResponse20033Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the company | [optional]
**displayId** | **int** | This is the \&quot;displayed id\&quot; (&#x3D;Kundennummer) of the company. The id must be unique, a customer nr. could be used multiple times | [optional]
**name** | **string** | name of the company | [optional]
**matchcode** | **string** | matchcode (used for searching) | [optional]
**street** | **string** | street | [optional]
**postcode** | **string** | postal code | [optional]
**city** | **string** | city | [optional]
**country** | **string** | country | [optional]
**note** | **string** | a note for this company | [optional]
**headquarterId** | **int** | if this company is a subsidiary, then here the id of the \&quot;central\&quot; / \&quot;headquerter\&quot; company is given | [optional]
**email** | **string** | e-Mail | [optional]
**website** | **string** | website | [optional]
**supportInfo** | **string** | a support info (internal remark) can be used here | [optional]
**lockout** | **bool** | if true, this company is locked | [optional]
**lockoutReason** | **string** | if company is locked, a reason can be given here | [optional]
**inactive** | **bool** | If true, the company is inactive | [optional]
**telephone** | **string** | phone number | [optional]
**telefax** | **string** | telefax number | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

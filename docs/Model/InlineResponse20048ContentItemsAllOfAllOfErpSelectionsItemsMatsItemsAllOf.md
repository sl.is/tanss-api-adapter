# # InlineResponse20048ContentItemsAllOfAllOfErpSelectionsItemsMatsItemsAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the erp selection material | [optional]
**erpSelectionId** | **int** | id of the associated erp selection | [optional]
**source** | **string** | determines the \&quot;source\&quot; for this material selection (either TANSS or MENTION).  \&quot;articleId\&quot; determines the id of the material | [optional]
**articleId** | **string** | determines the \&quot;id\&quot; of the material. * if the source TANSS is used, this represents the id of the TANSS material * if the source MENTION is used, this represents the id of the MENTION material | [optional]
**pos** | **int** | position of this material. | [optional]
**type** | **string** | (not implemented yet, but a type can be used to determined the \&quot;type\&quot; of a material) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

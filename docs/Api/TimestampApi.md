# SLIS\Adapter\Tanss\TimestampApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1TimestampsDayClosingDelete()**](TimestampApi.md#apiV1TimestampsDayClosingDelete) | **DELETE** /api/v1/timestamps/dayClosing | remove / undo one or more \&quot;day closings\&quot; for the timestamp module
[**apiV1TimestampsDayClosingPost()**](TimestampApi.md#apiV1TimestampsDayClosingPost) | **POST** /api/v1/timestamps/dayClosing | does one or more \&quot;day closings\&quot; for the timestamp module
[**apiV1TimestampsDayClosingTillDateGet()**](TimestampApi.md#apiV1TimestampsDayClosingTillDateGet) | **GET** /api/v1/timestamps/dayClosing/tillDate | gets all infos about last dayclosings for employees
[**apiV1TimestampsDayClosingTillDatePost()**](TimestampApi.md#apiV1TimestampsDayClosingTillDatePost) | **POST** /api/v1/timestamps/dayClosing/tillDate | created dayClosings to a given date
[**apiV1TimestampsEmployeeEmployeeIdInitialBalancePost()**](TimestampApi.md#apiV1TimestampsEmployeeEmployeeIdInitialBalancePost) | **POST** /api/v1/timestamps/employee/{employeeId}/initialBalance | sets the initial balance for this employee
[**apiV1TimestampsEmployeeIdDayDayPut()**](TimestampApi.md#apiV1TimestampsEmployeeIdDayDayPut) | **PUT** /api/v1/timestamps/{employeeId}/day/{day} | writes the timstamps of a whole day into the database at once
[**apiV1TimestampsGet()**](TimestampApi.md#apiV1TimestampsGet) | **GET** /api/v1/timestamps | gets a list of timestamps from a given period
[**apiV1TimestampsInfoGet()**](TimestampApi.md#apiV1TimestampsInfoGet) | **GET** /api/v1/timestamps/info | gets the timestamp infos for a given time period
[**apiV1TimestampsPauseConfigsGet()**](TimestampApi.md#apiV1TimestampsPauseConfigsGet) | **GET** /api/v1/timestamps/pauseConfigs | gets a list of all pause configs
[**apiV1TimestampsPauseConfigsIdDelete()**](TimestampApi.md#apiV1TimestampsPauseConfigsIdDelete) | **DELETE** /api/v1/timestamps/pauseConfigs/{id} | deletes a pause config
[**apiV1TimestampsPauseConfigsIdPut()**](TimestampApi.md#apiV1TimestampsPauseConfigsIdPut) | **PUT** /api/v1/timestamps/pauseConfigs/{id} | updates a pause config
[**apiV1TimestampsPauseConfigsPost()**](TimestampApi.md#apiV1TimestampsPauseConfigsPost) | **POST** /api/v1/timestamps/pauseConfigs | creates a pause config
[**apiV1TimestampsPost()**](TimestampApi.md#apiV1TimestampsPost) | **POST** /api/v1/timestamps | writes a timestamp into the database
[**apiV1TimestampsStatisticsGet()**](TimestampApi.md#apiV1TimestampsStatisticsGet) | **GET** /api/v1/timestamps/statistics | gets the timestamp infos for a given time period (with statistical values)
[**apiV1TimestampsTimestampIdPut()**](TimestampApi.md#apiV1TimestampsTimestampIdPut) | **PUT** /api/v1/timestamps/{timestampId} | edits a single timestamp


## `apiV1TimestampsDayClosingDelete()`

```php
apiV1TimestampsDayClosingDelete($requestBody): \SLIS\Adapter\Tanss\Model\InlineResponse204
```

remove / undo one or more \"day closings\" for the timestamp module

if calculations have to be re-done (i.e. values have changed), then the \"day closing\" objects have to be re-reated. They have to be deleted first, which can be done via this route. After this, they can be created again (via the POST route)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TimestampApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$requestBody = array(new \stdClass); // object[] | Array of day closing identifiers (each representing the employee and date)

try {
    $result = $apiInstance->apiV1TimestampsDayClosingDelete($requestBody);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TimestampApi->apiV1TimestampsDayClosingDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestBody** | [**object[]**](../Model/object.md)| Array of day closing identifiers (each representing the employee and date) |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse204**](../Model/InlineResponse204.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TimestampsDayClosingPost()`

```php
apiV1TimestampsDayClosingPost($inlineObject12): \SLIS\Adapter\Tanss\Model\InlineResponse20110
```

does one or more \"day closings\" for the timestamp module

\"closes\" a day by storing the calculated values into the database. This can only be done by employees who have access to the selected employee and therefore can execute a \"day closing\" for the timestamp module

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TimestampApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject12 = array(new \SLIS\Adapter\Tanss\Model\InlineObject12()); // \SLIS\Adapter\Tanss\Model\InlineObject12[] | Array of day closing identifiers (each representing the employee and date)

try {
    $result = $apiInstance->apiV1TimestampsDayClosingPost($inlineObject12);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TimestampApi->apiV1TimestampsDayClosingPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject12** | [**\SLIS\Adapter\Tanss\Model\InlineObject12[]**](../Model/InlineObject12.md)| Array of day closing identifiers (each representing the employee and date) |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20110**](../Model/InlineResponse20110.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TimestampsDayClosingTillDateGet()`

```php
apiV1TimestampsDayClosingTillDateGet(): \SLIS\Adapter\Tanss\Model\InlineResponse204
```

gets all infos about last dayclosings for employees

This route gets a list of all employees which you have (statistic) access to and return the last dayClosing for this employee.  If the employee doesn't have a dayClosing yet, the date is 0.  Name of the employees are give in the linked entities.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TimestampApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiV1TimestampsDayClosingTillDateGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TimestampApi->apiV1TimestampsDayClosingTillDateGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse204**](../Model/InlineResponse204.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TimestampsDayClosingTillDatePost()`

```php
apiV1TimestampsDayClosingTillDatePost($inlineObject13): \SLIS\Adapter\Tanss\Model\InlineResponse20111
```

created dayClosings to a given date

This route will create all \"missing\" dayClosings for a given list of employees till a given date.  The list of created dayClosing objects will be returned.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TimestampApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inlineObject13 = new \SLIS\Adapter\Tanss\Model\InlineObject13(); // \SLIS\Adapter\Tanss\Model\InlineObject13

try {
    $result = $apiInstance->apiV1TimestampsDayClosingTillDatePost($inlineObject13);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TimestampApi->apiV1TimestampsDayClosingTillDatePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject13** | [**\SLIS\Adapter\Tanss\Model\InlineObject13**](../Model/InlineObject13.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20111**](../Model/InlineResponse20111.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TimestampsEmployeeEmployeeIdInitialBalancePost()`

```php
apiV1TimestampsEmployeeEmployeeIdInitialBalancePost($employeeId, $inlineObject14): \SLIS\Adapter\Tanss\Model\InlineResponse20112
```

sets the initial balance for this employee

When transferring balances from a foreign system, the balance could have an initial value. This can be set by using this call.  Requires the permission to change other employees timestamps directly

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TimestampApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$employeeId = 56; // int | id of the employee
$inlineObject14 = new \SLIS\Adapter\Tanss\Model\InlineObject14(); // \SLIS\Adapter\Tanss\Model\InlineObject14

try {
    $result = $apiInstance->apiV1TimestampsEmployeeEmployeeIdInitialBalancePost($employeeId, $inlineObject14);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TimestampApi->apiV1TimestampsEmployeeEmployeeIdInitialBalancePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **employeeId** | **int**| id of the employee |
 **inlineObject14** | [**\SLIS\Adapter\Tanss\Model\InlineObject14**](../Model/InlineObject14.md)|  |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20112**](../Model/InlineResponse20112.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TimestampsEmployeeIdDayDayPut()`

```php
apiV1TimestampsEmployeeIdDayDayPut($employeeId, $day, $requestBody): \SLIS\Adapter\Tanss\Model\InlineResponse20038
```

writes the timstamps of a whole day into the database at once

This call automatically saves all timestamps of a single day into the databse (automatically determines which of those shall be inserted, updated or deleted)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TimestampApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$employeeId = 56; // int
$day = 'day_example'; // string
$requestBody = array(new \stdClass); // object[] | list of timestamps of the user for this day

try {
    $result = $apiInstance->apiV1TimestampsEmployeeIdDayDayPut($employeeId, $day, $requestBody);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TimestampApi->apiV1TimestampsEmployeeIdDayDayPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **employeeId** | **int**|  |
 **day** | **string**|  |
 **requestBody** | [**object[]**](../Model/object.md)| list of timestamps of the user for this day |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20038**](../Model/InlineResponse20038.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TimestampsGet()`

```php
apiV1TimestampsGet($from, $till): \SLIS\Adapter\Tanss\Model\InlineResponse20036
```

gets a list of timestamps from a given period

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TimestampApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$from = 56; // int | timestamp of start of period. if omitted,the beginning of the current day will be used
$till = 56; // int | timestamp of end of period. if omitted,the end of the current day will be used

try {
    $result = $apiInstance->apiV1TimestampsGet($from, $till);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TimestampApi->apiV1TimestampsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **from** | **int**| timestamp of start of period. if omitted,the beginning of the current day will be used | [optional]
 **till** | **int**| timestamp of end of period. if omitted,the end of the current day will be used | [optional]

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20036**](../Model/InlineResponse20036.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TimestampsInfoGet()`

```php
apiV1TimestampsInfoGet($from, $till): \SLIS\Adapter\Tanss\Model\InlineResponse20039
```

gets the timestamp infos for a given time period

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TimestampApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$from = 56; // int | timestamp of start of period. if omitted,the beginning of the current day will be used
$till = 56; // int | timestamp of end of period. if omitted,the end of the current day will be used

try {
    $result = $apiInstance->apiV1TimestampsInfoGet($from, $till);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TimestampApi->apiV1TimestampsInfoGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **from** | **int**| timestamp of start of period. if omitted,the beginning of the current day will be used | [optional]
 **till** | **int**| timestamp of end of period. if omitted,the end of the current day will be used | [optional]

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20039**](../Model/InlineResponse20039.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TimestampsPauseConfigsGet()`

```php
apiV1TimestampsPauseConfigsGet(): \SLIS\Adapter\Tanss\Model\InlineResponse20042
```

gets a list of all pause configs

Defines a configuration for automatical pause subtraction.  This is needed because longer working periods must contain certain pauses.  In this section, those pause config objects are maintained.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TimestampApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->apiV1TimestampsPauseConfigsGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TimestampApi->apiV1TimestampsPauseConfigsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20042**](../Model/InlineResponse20042.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TimestampsPauseConfigsIdDelete()`

```php
apiV1TimestampsPauseConfigsIdDelete($id)
```

deletes a pause config

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TimestampApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | id of the pause config

try {
    $apiInstance->apiV1TimestampsPauseConfigsIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling TimestampApi->apiV1TimestampsPauseConfigsIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id of the pause config |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TimestampsPauseConfigsIdPut()`

```php
apiV1TimestampsPauseConfigsIdPut($id, $body): \SLIS\Adapter\Tanss\Model\InlineResponse20113
```

updates a pause config

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TimestampApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | id of the pause config
$body = new \stdClass; // object | pause config object to be saved

try {
    $result = $apiInstance->apiV1TimestampsPauseConfigsIdPut($id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TimestampApi->apiV1TimestampsPauseConfigsIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id of the pause config |
 **body** | **object**| pause config object to be saved |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20113**](../Model/InlineResponse20113.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TimestampsPauseConfigsPost()`

```php
apiV1TimestampsPauseConfigsPost($body): \SLIS\Adapter\Tanss\Model\InlineResponse20113
```

creates a pause config

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TimestampApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \stdClass; // object | pause config object to be saved

try {
    $result = $apiInstance->apiV1TimestampsPauseConfigsPost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TimestampApi->apiV1TimestampsPauseConfigsPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **object**| pause config object to be saved |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20113**](../Model/InlineResponse20113.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TimestampsPost()`

```php
apiV1TimestampsPost($body, $autoPause): \SLIS\Adapter\Tanss\Model\InlineResponse2019
```

writes a timestamp into the database

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TimestampApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = {"state":"PAUSE_START","type":"WORK"}; // object | timestamp object to be saved
$autoPause = True; // bool | if true, a pause will automatically be inserted, if the minimum pause is not met for this day (see \"pause config\" routes)

try {
    $result = $apiInstance->apiV1TimestampsPost($body, $autoPause);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TimestampApi->apiV1TimestampsPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **object**| timestamp object to be saved |
 **autoPause** | **bool**| if true, a pause will automatically be inserted, if the minimum pause is not met for this day (see \&quot;pause config\&quot; routes) | [optional]

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2019**](../Model/InlineResponse2019.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TimestampsStatisticsGet()`

```php
apiV1TimestampsStatisticsGet($from, $till, $employeeIds): \SLIS\Adapter\Tanss\Model\InlineResponse20040
```

gets the timestamp infos for a given time period (with statistical values)

gets the timestamp infos for a given time period. Also extends misc. statistical values to the response, for example infos about documented support, vacation, illness or absences.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TimestampApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$from = 56; // int | timestamp of start of period. if omitted,the beginning of the current day will be used
$till = 56; // int | timestamp of end of period. if omitted,the end of the current day will be used
$employeeIds = 'employeeIds_example'; // string | id of all employees of whom the statistics shall be generated (comma sperated). Important you must have the permission to view the statistics for this employee

try {
    $result = $apiInstance->apiV1TimestampsStatisticsGet($from, $till, $employeeIds);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TimestampApi->apiV1TimestampsStatisticsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **from** | **int**| timestamp of start of period. if omitted,the beginning of the current day will be used | [optional]
 **till** | **int**| timestamp of end of period. if omitted,the end of the current day will be used | [optional]
 **employeeIds** | **string**| id of all employees of whom the statistics shall be generated (comma sperated). Important you must have the permission to view the statistics for this employee | [optional]

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse20040**](../Model/InlineResponse20040.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1TimestampsTimestampIdPut()`

```php
apiV1TimestampsTimestampIdPut($timestampId, $body): \SLIS\Adapter\Tanss\Model\InlineResponse2019
```

edits a single timestamp

with this call, you can edit a single timestamp

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new SLIS\Adapter\Tanss\Api\TimestampApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$timestampId = 56; // int | id of the timestamp
$body = new \stdClass; // object | timestamp object to be saved

try {
    $result = $apiInstance->apiV1TimestampsTimestampIdPut($timestampId, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TimestampApi->apiV1TimestampsTimestampIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timestampId** | **int**| id of the timestamp |
 **body** | **object**| timestamp object to be saved |

### Return type

[**\SLIS\Adapter\Tanss\Model\InlineResponse2019**](../Model/InlineResponse2019.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

# # InlineResponse20086Content

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the component type | [optional]
**type** | **string** | name of the component type | [optional]
**shortName** | **string** | short name of the component type | [optional]
**shown** | **bool** | if false, this type won&#39;t be shown in the select field (meaning its an inactive type) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

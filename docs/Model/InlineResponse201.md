# # InlineResponse201

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**\SLIS\Adapter\Tanss\Model\InlineResponse201Meta**](InlineResponse201Meta.md) |  | [optional]
**content** | [**InlineResponse20071ContentTickets**](InlineResponse20071ContentTickets.md) | ticket model with all fields | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
